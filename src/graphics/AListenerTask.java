package graphics;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import javax.swing.SwingWorker;

/**
 * Wrapper for the GUI listener.
 *
 * <T> return type
 * <S> intermediary type (the "shout out" to listen for)
 */
public abstract class AListenerTask<T, S> extends SwingWorker<T, S> 
        implements PropertyChangeListener {

    private ALoudCall<T, S> aMethod;

    public AListenerTask(ALoudCall<T, S> aMethod) {
        this.aMethod = aMethod;
    }

    @Override
    protected T doInBackground() throws Exception {
        aMethod.addListener(this);
        return aMethod.call();
    }

    @SuppressWarnings("unchecked")
	public void propertyChange(PropertyChangeEvent evt) {
        if ("shoutOut".equals(evt.getPropertyName())) {
            publish((S)evt.getNewValue());
        }
    }

//    @Override
//    protected abstract void done();
    
    @Override
    protected abstract void process(List<S> chunks);
}