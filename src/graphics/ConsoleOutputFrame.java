package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

import myGrid.Main;

/**
 * The text area which is used for displaying logging information.
 */
@SuppressWarnings("serial")
public class ConsoleOutputFrame extends JFrame{
    private JTextArea textArea;
    private JButton buttonClear;     
     
    public ConsoleOutputFrame() {
        super("Console Outputs");
         
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(680, 520);
        setLocationRelativeTo(null);    // centers on screen
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        
        textArea = new JTextArea(50, 10);
        textArea.setEditable(false);
        buttonClear = new JButton("Clear");
        buttonClear.setAlignmentX(LEFT_ALIGNMENT); //TODO
        
        // adds event handler for button Clear
        buttonClear.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent evt) {
        		// clears the text area
        		try {
        			textArea.getDocument().remove(0,
        					textArea.getDocument().getLength());
        		} catch (BadLocationException ex) {
        			ex.printStackTrace();
        		}
        	}
        });

        add(buttonClear);
        add(new JScrollPane(textArea));

        if(Main.useGUI){
        	PrintStream printStream = new PrintStream(new MyOutputStream(textArea));
        	System.setOut(printStream);
        }
        //        standardOut = System.out;
    }
}
