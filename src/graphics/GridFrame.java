package graphics;

import java.awt.Cursor;
import java.awt.Frame;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

@SuppressWarnings("serial")
public class GridFrame extends JFrame {
	private GridGraphics gridGraphic;

	public GridGraphics getGridGraphic() {
		return gridGraphic;
	}

	public GridFrame() {
		setTitle("Grids of data and the potential candidates");
		setExtendedState(Frame.MAXIMIZED_BOTH);
		pack();

		gridGraphic = new GridGraphics();
		JScrollPane gridScrollPane = new JScrollPane(gridGraphic);
		gridScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		gridScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
		
		JPanel gridPanel = new JPanel();
		gridPanel.setLayout(new BoxLayout(gridPanel, BoxLayout.Y_AXIS));
		gridPanel.add(new InputPanel(gridPanel));
		gridPanel.add(gridScrollPane);
	    
		add(gridPanel);
		setVisible(true);
		setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
