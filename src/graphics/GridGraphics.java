package graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import myGrid.Cell;
import myGrid.Grid;
import myGrid.Main;
import myGrid.MinMaxPair;
import myGrid.MyPoint;
import myGrid.Node;
import myGrid.TNode;
import mySweepline.Interval1D;

@SuppressWarnings("serial")
public class GridGraphics extends JPanel {
	private AlsXYMouseLabelComponent alsXYMouseLabel;
	private JLayeredPane layeredPane;

	private int gridGraphicWidth = 1000;
	private int gridGraphicHeight = 1000;
	private int zoomLimit = 10000;

	private Interval1D intervalResult;
	private HashSet<Interval1D> maxWindowsForEachCandidate;
	private LinkedHashSet<Node> candidateNodeList;
	private ArrayList<MyPoint> pointsInProcessing;
	private ArrayList<Node> nodesInProcessing;
	private Double y1_nodesEdge;
	private Double y2_nodesEdge;
	private Double x1_nodesEdge;
	private Double x2_nodesEdge;
	private int detailLevel;
	private LinkedHashSet<String> keywordsForLevelDetail;

	private ArrayList<MyPoint> keywordPoints;
	
	public void clearGraph(){
		intervalResult = null;
		maxWindowsForEachCandidate = null;
		candidateNodeList = null;
		pointsInProcessing = null;
		nodesInProcessing = null;
		y1_nodesEdge = null;
		y2_nodesEdge = null;
		x1_nodesEdge = null;
		x2_nodesEdge = null;
		keywordPoints = null;
		detailLevel = 0;
		keywordsForLevelDetail = null;
		repaint();
	}
	
	public void setRectResult(Interval1D intervalResult) {
		this.intervalResult = intervalResult;
		repaint();
	}

	public void setCandidatesResults(HashSet<Interval1D> maxWindowsForEachCandidate) {
		this.maxWindowsForEachCandidate = maxWindowsForEachCandidate;
		repaint();
	}

	public void setCandidateNodeList() {
		this.candidateNodeList = Main.myQuery.getCandidateNodeList();
		repaint();
	}

	public void setPointsInProcessing(ArrayList<MyPoint> pointsInProcessing) {
		this.pointsInProcessing = pointsInProcessing;
		repaint();
	}

	public void setNodesInProcessing(ArrayList<Node> nodesInProcessing) {
		this.nodesInProcessing = nodesInProcessing;
		repaint();
	}

	public void setKeywordPoints() {
		keywordPoints = new ArrayList<MyPoint>();
		for (MyPoint twitterPt : Grid.getInstance().getDatasetAsPoints()) {
			if (twitterPt.containCertainKeywords(Main.myQuery.getKeywords()))
				keywordPoints.add(twitterPt); // p with queryKeywords
		}
		repaint();
	}
	public void setLevelForDetail(int detailLevel, LinkedHashSet<String> keywordsForLevelDetail){
		this.detailLevel = detailLevel;
		this.keywordsForLevelDetail = keywordsForLevelDetail;
		repaint();
	}
	
	public void setGridGraphicWidth(int gridGraphicWidth) {
		this.gridGraphicWidth = gridGraphicWidth;
	}
	
	public void setGridGraphicHeight(int gridGraphicHeight) {
		this.gridGraphicHeight = gridGraphicHeight;
	}


	public int getGridGraphicWidth() {
		return gridGraphicWidth;
	}
	
	public int getGridGraphicHeight() {
		return gridGraphicHeight;
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(gridGraphicWidth, gridGraphicHeight);
	}


	public GridGraphics() {
		alsXYMouseLabel = new AlsXYMouseLabelComponent();

		// repaint when zoom
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int direction = e.getWheelRotation();
				double zoom = direction * 1.08;
				zoom = (zoom > 0) ? 1 / zoom : -zoom;
				if (gridGraphicWidth * zoom < zoomLimit && gridGraphicHeight * zoom < zoomLimit) {
					gridGraphicWidth = (int) (gridGraphicWidth * zoom);
					gridGraphicHeight = (int) (gridGraphicHeight * zoom);
				}
				GridGraphics.this.repaint();
				alsXYMouseLabel.repaint();
			}
		});

		// add coordinates of mouse position
		layeredPane = new JLayeredPane();
		layeredPane.setPreferredSize(new Dimension(gridGraphicWidth, gridGraphicHeight));
		layeredPane.add(alsXYMouseLabel, JLayeredPane.DRAG_LAYER);
		alsXYMouseLabel.setBounds(0, 0, gridGraphicWidth, gridGraphicHeight);
		this.add(layeredPane);
		
//		addMouseMotionListener(new MouseMotionAdapter() {  TODO try update
//			public void mouseMoved(MouseEvent me)
//			{
//				alsXYMouseLabel.x = me.getX();
//				alsXYMouseLabel.y = me.getY();
//				layeredPane.revalidate();
//				layeredPane.repaint();
//			}
//		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;
		drawQuerySize(g2d);
		drawGridAndPoints(g2d);
		drawKeyWordPoints(g2d);
		drawQueryCandidates(g2d);
		// drawQuerySize(g2d); //rect left top to show the query size
		processCandidateNodes(g2d);
		processCandidatePoints(g2d);

		drawMaxWindowAllCandidates(g2d);
		drawMaxWindow(g2d);
		if(Main.useCandidates)
			drawDetailCurrentLevelNew(g2d);
		else
			drawDetailCurrentLevelOld(g2d);
	}

	private void drawQuerySize(Graphics2D g2d) {
		g2d.drawRect(0, 0, (int) (Main.myQuery.getQueryWidth() * getGridGraphicWidth()), (int) (Main.myQuery.getQueryHeight() * getGridGraphicHeight()));
	}

	private void drawGridAndPoints(Graphics2D g2d) {
		if(Grid.getInstance()==null){
			return;
		}
		Map<Integer, Node[][]> printTree = Grid.getInstance().getTree();
		for (Map.Entry<Integer, Node[][]> entry : printTree.entrySet()) {
			Node[][] nodes = entry.getValue();
			int nodesLengthX = nodes.length;
			int nodesLengthY = nodes[nodesLengthX - 1].length;
			for (int i = 0; i < nodesLengthX; i++) {
				for (int j = 0; j < nodesLengthY; j++) {
					if (nodes[i][j] instanceof TNode) {

					} else {
						Cell currentCell = (Cell) nodes[i][j];
						int x1 = (int) (currentCell.getX1() * getGridGraphicWidth());
						int x2 = (int) (currentCell.getX2() * getGridGraphicWidth());
						int y1 = (int) (currentCell.getY1() * getGridGraphicHeight());
						int y2 = (int) (currentCell.getY2() * getGridGraphicHeight());
						
						if(Main.myQuery.getKeywords()!=null && currentCell.calPoint(Main.myQuery.getKeywords())!=null){
							int relevantPCnts = currentCell.calPoint(Main.myQuery.getKeywords()).size();
							if (relevantPCnts != 0) {
								g2d.setColor(new Color(0, 0, 0, 100));
								g2d.drawString(String.valueOf(relevantPCnts), x1, y2);
							}
						}
						g2d.setColor(Color.black);
						g2d.drawRect(x1, y1, x2 - x1, y2 - y1);
					}
				}
			}
		}
		for (MyPoint point : Grid.getInstance().getDatasetAsPoints()) {
			g2d.setColor(Color.black);
			g2d.fillOval((int) (point.getX() * getGridGraphicWidth()-2),
					(int) (point.getY() * getGridGraphicHeight()-2), 4, 4);
		}
	}

	private void drawKeyWordPoints(Graphics2D g2d){
		if(keywordPoints != null && keywordPoints.size()>0){
			for (MyPoint point : keywordPoints) {
				g2d.setColor(Color.ORANGE);
				g2d.fillOval((int) (point.getX() * getGridGraphicWidth()-2),
						(int) (point.getY() * getGridGraphicHeight()-2), 4, 4);
			}
		}
	}
	
	private void drawQueryCandidates(Graphics2D g2d) {
		if (candidateNodeList != null && candidateNodeList.size() > 0) {
			for (Node node : candidateNodeList) {
				g2d.setColor(Color.GREEN);
				int x1 = (int) (node.getX1() * gridGraphicWidth);
				int y1 = (int) (node.getY1() * gridGraphicHeight);
				int x2 = (int) (node.getX2() * gridGraphicWidth);
				int y2 = (int) (node.getY2() * gridGraphicHeight);
				Stroke oldStroke = g2d.getStroke();
				g2d.setStroke(new BasicStroke(3));
				g2d.drawRect(x1, y1, x2 - x1, y2 - y1);
				g2d.setStroke(oldStroke);
			}
		}
	}

	private void processCandidateNodes(Graphics2D g2d) {
		x1_nodesEdge = null;
		x2_nodesEdge = null;
		y2_nodesEdge = null;
		y1_nodesEdge = null;
		if (nodesInProcessing != null && nodesInProcessing.size() > 0) {
			for (Node node : nodesInProcessing) {
				int alpha = 30; // 50% transparent
				Color myColour = new Color(255, 0, 0, alpha);
				g2d.setColor(myColour);
				int x = (int) (node.getX1() * getGridGraphicWidth());
				int y = (int) (node.getY1() * getGridGraphicHeight());
				int width = (int) ((node.getX2() - node.getX1()) * getGridGraphicWidth());
				int height = (int) ((node.getY2() - node.getY1()) * getGridGraphicHeight());
				g2d.fillRect(x, y, width, height);
				g2d.setColor(Color.BLACK);
				x1_nodesEdge = (x1_nodesEdge == null) ? node.getX1() : Math.min(x1_nodesEdge, node.getX1());
				x2_nodesEdge = (x2_nodesEdge == null) ? node.getX2() : Math.max(x2_nodesEdge, node.getX2());
				y1_nodesEdge = (y1_nodesEdge == null) ? node.getY1() : Math.min(y1_nodesEdge, node.getY1());
				y2_nodesEdge = (y2_nodesEdge == null) ? node.getY2() : Math.max(y2_nodesEdge, node.getY2());
			}
		}
	}

	private void processCandidatePoints(Graphics2D g2d) {
		if (pointsInProcessing != null && pointsInProcessing.size() > 0) {
			for (MyPoint point : pointsInProcessing) {
				g2d.setColor(Color.red);
				g2d.fillOval((int) (point.getX() * getGridGraphicWidth()-2),
						(int) (point.getY() * getGridGraphicHeight()-2), 4, 4);
				g2d.setColor(Color.BLACK);
			}
		}
	}

	private void drawMaxWindowAllCandidates(Graphics2D g2d) {
		// draw maxWindow in all candidates
		if (maxWindowsForEachCandidate != null && maxWindowsForEachCandidate.size() > 0) {
			Iterator<Interval1D> it = maxWindowsForEachCandidate.iterator();
			while(it.hasNext()){
				Interval1D interval1d = it.next();
				Stroke oldStroke = g2d.getStroke();
				g2d.setStroke(new BasicStroke(1));
				g2d.setColor(Color.ORANGE);
				
				int width = (int) (Main.myQuery.getQueryWidth() * getGridGraphicWidth());
				int height = (int) (Main.myQuery.getQueryHeight() * getGridGraphicHeight());
				int x1 = (int) (interval1d.getHigh() * getGridGraphicWidth()) - width;
				int y1 = (int) (interval1d.getHeight() * getGridGraphicHeight()) - height;
				g2d.drawRect(x1, y1, width, height);
				g2d.setStroke(oldStroke);
				g2d.drawString(String.valueOf(interval1d.getCount()-1), x1, y1);
			}
		}
	}

	private void drawMaxWindow(Graphics2D g2d) {
		// draw result
		if (intervalResult != null) {
			Stroke oldStroke = g2d.getStroke();
			g2d.setStroke(new BasicStroke(1));
			g2d.setColor(Color.RED);
			int width = (int) (Main.myQuery.getQueryWidth() * getGridGraphicWidth());
			int height = (int) (Main.myQuery.getQueryHeight() * getGridGraphicHeight());
			int x1 = (int) (intervalResult.getHigh() * getGridGraphicWidth()) - width;
			int y1 = (int) (intervalResult.getHeight() * getGridGraphicHeight()) - height;
			g2d.drawRect(x1, y1, width, height);
			g2d.setStroke(oldStroke);
			g2d.drawString(String.valueOf(intervalResult.getCount()-1), x1, y1);
		}
	}

	private void drawDetailCurrentLevelOld(Graphics2D g2d){
		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(2));
		g2d.setColor(Color.RED);
		if(detailLevel != 0){
			Node[][] currentLevel = Grid.getInstance().getTree().get(detailLevel);
			for (Node[] nodes : currentLevel) {
				for (Node node : nodes) { 
					if(((TNode)node).getNodeMinMaxOld()== null)
						continue;
					int count = 0;
					for (String keyword : keywordsForLevelDetail) {
						HashMap<Integer, MinMaxPair> keywordMinMaxBound = ((TNode)node).getNodeMinMaxOld().get(keyword);
						if(keywordMinMaxBound == null)
							continue;
						MinMaxPair minMax = keywordMinMaxBound.get(detailLevel);
						int min = minMax.getMin();	
						int max = minMax.getMax();
						//TODO setting minmaxKeyword for a certain search: 
						int x1 =(int)(node.getX1()*getGridGraphicWidth());
						int y1 =(int)(node.getY1()*getGridGraphicHeight());
						int x2 =(int)(node.getX2()*getGridGraphicWidth());
						int y2 =(int)(node.getY2()*getGridGraphicHeight());
						g2d.drawString(keyword+" "+min+"/"+max, x1, y2- count*15);
						g2d.drawRect(x1 ,y1 , x2-x1, y2-y1);
						count ++;
					}
				}
			}
		}
		g2d.setStroke(oldStroke);
	}
	
	private void drawDetailCurrentLevelNew(Graphics2D g2d) {
		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(2));
		g2d.setColor(Color.RED);
		if (detailLevel != 0) {
			Node[][] currentLevel = Grid.getInstance().getTree().get(detailLevel);
			for (Node[] nodes : currentLevel) {
				for (Node node : nodes) {
					if (((TNode) node).getNodeMinMaxNew() == null)
						continue;

					int count = 0;
					for (String keyword : keywordsForLevelDetail) {
						MinMaxPair minMax = ((TNode) node).getNodeMinMaxNew().get(keyword);
						if (minMax != null) {
							int min = minMax.getMin();
							int max = minMax.getMax();
							// TODO setting minmaxKeyword for a certain search:
							int x1 = (int) (node.getX1() * getGridGraphicWidth());
							int y1 = (int) (node.getY1() * getGridGraphicHeight());
							int x2 = (int) (node.getX2() * getGridGraphicWidth());
							int y2 = (int) (node.getY2() * getGridGraphicHeight());
							g2d.drawString(keyword + " " + min + "/" + max, x1, y2 - count * 15);
							if ((Grid.getInstance().getLevelCandidates().get(detailLevel).get(keyword)).contains(node)){
								g2d.setStroke(new BasicStroke(4.0F));
								g2d.drawRect(x1, y1, x2 - x1, y2 - y1);
								g2d.setStroke(new BasicStroke(2.0F));
							}
							else{
								System.out.println("this should not happen: draw the result rect with thin red frame");
								g2d.drawRect(x1, y1, x2 - x1, y2 - y1);
							}
							count++;
						}
					}
				}
			}
		}
		g2d.setStroke(oldStroke);
	}

	class AlsXYMouseLabelComponent extends JComponent {
		public int x;
		public int y;

		public AlsXYMouseLabelComponent() {
			this.setBackground(Color.blue);
		}

		// use the xy coordinates to update the mouse cursor text/label
		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			String s = 1.0 * x / gridGraphicWidth + ", " + 1.0 * y / gridGraphicHeight;
			g.setColor(Color.red);
			g.drawString(s, x + 10, y + 10);
		}
	}
}
