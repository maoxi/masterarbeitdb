package graphics;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import myGrid.TwitterDataLoader;
import myGrid.Grid;
import myGrid.Main;
import myGrid.TextQuery;
import utilities.SortFunctions;

@SuppressWarnings("serial")
public class InputPanel extends JPanel{
	ConsoleOutputFrame consoleOutputFrame = new ConsoleOutputFrame();
	
	File filteredFile;
	JPanel parentPanel;
	JTextField input;
	JButton updateQueryButton;
	JTextField inputQueryWidth;
	JTextField inputQueryHeight; 
	JButton updateQueryParamButton;
	JTextField inputTreeHeight;
	JButton updateTreeHeightBtn;
	JComboBox<String> showLevelDetail; 
	
	public InputPanel(JPanel parentPanel) {
		this.parentPanel = parentPanel;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JPanel basicComp = new JPanel();
		basicComp.setBorder(BorderFactory.createTitledBorder("Basic parameters"));
		basicComp.setLayout(new BoxLayout(basicComp, BoxLayout.X_AXIS));
		JPanel compareComp = new JPanel();
		compareComp.setBorder(BorderFactory.createTitledBorder("Comparison parameters"));
		compareComp.setLayout(new BoxLayout(compareComp, BoxLayout.X_AXIS));
		
		JButton openCSVFileButton = new JButton("Open a .csv File...");
		openCSVFileButton.addActionListener(new createTreeFromFileActionListener());

		input = new JTextField(30);
		updateQueryButton = new JButton("Update Query Keywords");
		updateQueryButton.addActionListener(new AddQueryListener());
		
		inputQueryWidth = new JTextField(5);
		inputQueryHeight = new JTextField(5);
		updateQueryParamButton = new JButton("Update Query Width/Height");
		updateQueryParamButton.addActionListener(new ChangeQueryParameterListener());
		
		inputTreeHeight = new JTextField(3);
		updateTreeHeightBtn = new JButton("Update tree height");
		updateTreeHeightBtn.addActionListener(new ChangeQueryParameterListener());
		
		JRadioButton isWithAnimation = new JRadioButton("Show animation");
		isWithAnimation.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				 if (e.getStateChange() == ItemEvent.SELECTED)
					 Main.delayTime = 500;
				 else
					 Main.delayTime = 0;
			}
		});
		
		JRadioButton useKeywordFromData = new JRadioButton("Keywords original(Y)/ filtered(N)", false);
		useKeywordFromData.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Main.useOriginalKeywordFrequency = (e.getStateChange() == ItemEvent.SELECTED);
			}
		});

		JRadioButton useAccurateMAX = new JRadioButton("Accurate neighbour", false);
		useAccurateMAX.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
					Main.useAccurateMAX = (e.getStateChange() == ItemEvent.SELECTED);
			}
		});
		
		JRadioButton useGroupingKeywords = new JRadioButton("Group keywords", false);
		useGroupingKeywords.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Main.useLocalKWGroup = (e.getStateChange()== ItemEvent.SELECTED);
			}
		});
		useGroupingKeywords.setEnabled(false);

		JRadioButton useCandidates = new JRadioButton("Save candidates instead of Min/Max", false);
		useCandidates.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
					Main.useCandidates = (e.getStateChange() == ItemEvent.SELECTED);
					if(Main.useCandidates)
						useGroupingKeywords.setEnabled(true);
					else{
						useGroupingKeywords.setEnabled(false);
						Main.useLocalKWGroup = false;
					}
			}
		});

		JRadioButton usePruneRedudant = new JRadioButton("Prune redudant between levels", false);
		usePruneRedudant.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Main.pruneRedundantCandidate = (e.getStateChange()== ItemEvent.SELECTED);
			}
		});
		
		JRadioButton useSort = new JRadioButton("Sort Min,Max,Freq.", false);
		useSort.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Main.useSort = (e.getStateChange()== ItemEvent.SELECTED);
			}
		});		
		
		JSlider zoomGraphic = new JSlider(SwingConstants.HORIZONTAL,500, 5000, 1000);
		zoomGraphic.addChangeListener(new ZoomGraphicListener());
		zoomGraphic.setMajorTickSpacing(100);
		zoomGraphic.setMinorTickSpacing(10);
		zoomGraphic.setPaintTicks(false);
		zoomGraphic.setPaintLabels(false);
		
		showLevelDetail = new JComboBox<>();
		showLevelDetail.addItem("clear all level info.");
		//info of last level not necessary
		if(Grid.getInstance()!=null){
			for(int treeHeight =1; treeHeight<= Grid.getInstance().getTreeHeight()-1; treeHeight++){
				showLevelDetail.addItem("grid level "+ treeHeight);
			}
		}
		showLevelDetail.addActionListener(new ShowLevelDetailListener());
		
		JButton showConsoleBtn = new JButton("show Console Output");
		showConsoleBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				consoleOutputFrame.setVisible(true);
			}
		});
		
		basicComp.add(openCSVFileButton);
		basicComp.add(Box.createRigidArea(new Dimension(10,0)));
		basicComp.add(input);
		basicComp.add(updateQueryButton);
		basicComp.add(Box.createRigidArea(new Dimension(10,0)));
		basicComp.add(inputQueryWidth);
		basicComp.add(inputQueryHeight);
		basicComp.add(updateQueryParamButton);
		basicComp.add(inputTreeHeight);
		basicComp.add(updateTreeHeightBtn);
		basicComp.add(zoomGraphic);
		compareComp.add(useKeywordFromData);
		compareComp.add(useSort);
		compareComp.add(useAccurateMAX);
		compareComp.add(useCandidates);
		compareComp.add(useGroupingKeywords);
		compareComp.add(usePruneRedudant);
		compareComp.add(isWithAnimation);
		compareComp.add(Box.createRigidArea(new Dimension(10,0)));
		compareComp.add(showLevelDetail);
		compareComp.add(showConsoleBtn);

		add(compareComp);
		add(Box.createRigidArea(new Dimension(0,10)));
		add(basicComp);
		add(Box.createRigidArea(new Dimension(0,10)));
	}
	
	private void updateMyQuery(){
		ALoudCall<Void, Void> myCal = new ALoudCall<Void, Void>() {
            @Override
            public Void call() throws Exception {
            	Main.updateQuery();
                return null;
            }
        };
        
		(new AListenerTask<Void, Void>(myCal) {
			@Override
			protected void process(List<Void> chunks) {
			}
		}).execute();
	}
	
	private void updateMyGrid(){
		//delete this part and include the part below to use thread TODO
		try {
			Main.updateGrid();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		showLevelDetail.removeAllItems();
		showLevelDetail.addItem("clear all level info.");
		//info of last level not necessary
		if(Grid.getInstance()!=null){
			for(int treeHeight =1; treeHeight<= Grid.getInstance().getTreeHeight()-1; treeHeight++){
				showLevelDetail.addItem("grid level "+ treeHeight);
			}
		}
//		ALoudCall<Void, Void> myCal = new ALoudCall<Void, Void>() {
//            @Override
//            public Void call() throws Exception {
//            	TwitterDataLoader.getInstance().loadFilteredFile(f.getParentFile()+"\\"+filteredFileName);
//				TwitterDataLoader.getInstance().calTopHashTagsFromFile("topHashtags.txt");//TODO input per GUI
//				Main.updateGrid();
//				shoutOut(null);
//                return null;
//            }
//        };
//        
//		(new AListenerTask<Void, Void>(myCal) {
//			@Override
//			protected void process(List<Void> chunks) {
//			}
//		}).execute();			
	}
	
	class ShowLevelDetailListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			@SuppressWarnings("unchecked")
			JComboBox<String> jComboBox = (JComboBox<String>)(e.getSource());
			int level = jComboBox.getSelectedIndex();
			//get the user input from keywords
			LinkedHashSet<String> keywords =  new LinkedHashSet<String>(); 
			if(input.getText() != null && !input.getText().isEmpty()){
				//treeSet is sorted by default alphabetically
				keywords.addAll(Arrays.asList(input.getText().replaceAll("\\s","").split(",")));
				if(Main.useSort)
					keywords = SortFunctions.sortSetByKeywordFrequency(keywords);
			}else{
				keywords.add(Main.NOKEYWORD);
			}	
			if(Main.useGUI)
				Main.gridFrame.getGridGraphic().setLevelForDetail(level, keywords);
		}
	}
	
	class ChangeQueryParameterListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getSource().equals(updateQueryParamButton)){
				if(inputQueryWidth != null && !inputQueryWidth.getText().isEmpty())
					Main.myQuery.setQueryWidth(Double.parseDouble(inputQueryWidth.getText()));
				else
					Main.myQuery.setQueryWidth(0.02);
				if(inputQueryHeight != null && !inputQueryHeight.getText().isEmpty())
					Main.myQuery.setQueryHeight(Double.parseDouble(inputQueryHeight.getText()));
				else
					Main.myQuery.setQueryHeight(0.02);				
			}else if(e.getSource().equals(updateTreeHeightBtn)){
				if(inputTreeHeight.getText()!=null && !inputTreeHeight.getText().isEmpty()){
					Main.treeHeight = Integer.parseInt(inputTreeHeight.getText());
				}else
					Main.treeHeight = 7;
				updateMyGrid();
			}
		}
	}
	
	class AddQueryListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			//treeSet is sorted by frequency as default
			LinkedHashSet<String> keywords =  new LinkedHashSet<String>(); 
			if(input.getText() != null && !input.getText().isEmpty()){
				keywords.addAll(Arrays.asList(input.getText().replaceAll("\\s","").split(",")));
				if(Main.useSort)
					keywords = SortFunctions.sortSetByKeywordFrequency(keywords);
			}else{
				keywords.add(Main.NOKEYWORD);
			}
			Main.myQuery.setKeywords(keywords);
			updateMyQuery();
		}
	}

	class createTreeFromFileActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			JFileChooser fc = new JFileChooser();
			int returnVal = fc.showOpenDialog(parentPanel);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				filteredFile = fc.getSelectedFile();
				try {
					Main.myQuery = new TextQuery();
					
					if (!filteredFile.exists() || filteredFile.isDirectory()) {
						new Exception("filtered file does not exist!");
					}else{
						TwitterDataLoader.getInstance().loadFilteredFile(filteredFile);
					}
					Main.prepareInputData();
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				updateMyGrid();
			} else {
				System.out.println("openning file failed");
			}
		}
	}

	class ZoomGraphicListener implements ChangeListener{
		@Override
		public void stateChanged(ChangeEvent e) {
			JSlider source = (JSlider)e.getSource();
		    if (!source.getValueIsAdjusting()) {
		        int zoom = source.getValue();
		        if(Main.useGUI){
		        	Main.gridFrame.getGridGraphic().setGridGraphicWidth(zoom);
		        	Main.gridFrame.getGridGraphic().setGridGraphicHeight(zoom);
		        	Main.gridFrame.getGridGraphic().repaint();
		        }
		    }
		}
	}

}
