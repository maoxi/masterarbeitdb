package graphics;

import java.awt.Color;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class SweeplineFrame extends JFrame{
	private SweeplineGraphics sweeplineGraphic;
	
	public SweeplineGraphics getSweeplineGraphic() {
		return sweeplineGraphic;
	}
	
	public SweeplineFrame() {
		setTitle("Sweepline of potential candidates");
		setExtendedState(Frame.MAXIMIZED_BOTH);
		sweeplineGraphic = new SweeplineGraphics();
		add(new JScrollPane(sweeplineGraphic));
		setBackground(Color.white);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
