package graphics;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JPanel;

import myGrid.Grid;
import myGrid.Main;
import myGrid.MinPQ;
import myGrid.MyPoint;
import mySweepline.Interval1D;
import mySweepline.Interval2D;
import mySweepline.MaxRS.Event;
import mySweepline.SweeplineNode;
import mySweepline.SweeplineTree;

//TODO only necessary when several classes share the same Frame, delete it if not
@SuppressWarnings("serial")
public class SweeplineGraphics extends JPanel {
	public static int graphWidth = 6000;
	public static int graphHeight = 6000;
	public static int screenmargin = 20;
	public static int zoomLimit = 20000;

	private SweeplineTree tree;
	private ArrayList<SweeplineNode> F_IN;
	private ArrayList<SweeplineNode> F_L;
	private ArrayList<SweeplineNode> F_R;

	private Interval1D thisMaxWindow;
	private ArrayList<MyPoint> pointsInProcessing;
	private ArrayList<MyPoint> allPoints;
	private double[] borders;
	private SweeplineTree windows;
	private MinPQ<Event> eventsRectangle;
	private Event event;

	public void setTree(SweeplineTree tree) {
		this.tree = tree;
		repaint();
	}

	public void setF_IN(ArrayList<SweeplineNode> f_IN) {
		F_IN = f_IN;
		repaint();
	}

	public void setF_L(ArrayList<SweeplineNode> f_L) {
		F_L = f_L;
		repaint();
	}

	public void setF_R(ArrayList<SweeplineNode> f_R) {
		F_R = f_R;
		repaint();
	}

	public void setThisMaxWindow(Interval1D thisMaxWindow) {
		this.thisMaxWindow = thisMaxWindow;
		repaint();
	}

	public void setPointsInProcessing(ArrayList<MyPoint> pointsInProcessing) {
		this.pointsInProcessing = pointsInProcessing;
		repaint();
	}

	public void setBorders(double[] borders) {
		this.borders = borders;
		repaint();
	}

	public void setWindows(SweeplineTree windows) {
		this.windows = windows;
		repaint();
	}

	public void setEvent(Event event) {
		this.event = event;
		repaint();
	}

	public void setEventsRectangle(MinPQ<Event> eventsRectangle) {
		this.eventsRectangle = eventsRectangle;
		repaint();
	}

	public void setAllPoints(ArrayList<MyPoint> allPoints) {
		this.allPoints = allPoints;
	}
	
	public void clearGraph(){
		tree = null;
		F_IN = null;
		F_L = null;
		F_R = null;
		thisMaxWindow = null;
		windows = null;
		eventsRectangle = null;
		event = null;
		pointsInProcessing = null;
		allPoints = null;
//		borders = null;  not included because it influences zoom
		repaint();
	}
	
	// set for stroll panel	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(graphWidth, 1500);
	}

	public SweeplineGraphics() {
		// repaint when zoom
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int direction = e.getWheelRotation();
				double zoom = direction * 1.08;
				zoom = (zoom > 0) ? 1 / zoom : -zoom;
				if (graphWidth * zoom < zoomLimit && graphHeight * zoom < zoomLimit) {
					graphWidth = (int) (graphWidth * zoom);
					graphHeight = (int) (graphHeight * zoom);
				}
				
				repaint();
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		processCandidateBorders(g2d);
		processCandidatePoints(g2d);
		processAllPoints(g2d);
		processEventsRectangles(g2d);
		processEventLine(g2d);
		processWindow(g2d);

		drawMaxWindow(g2d);

		if (tree != null) {
			SweeplineNode root = tree.treeNodes[1];
			drawTree(g2d, root, 20, (int)(((borders[3]-borders[2])/(Grid.getInstance().getTreeHeight()+2))*graphHeight));
		}
		drawFPath(g2d, F_L);
		drawFPath(g2d, F_IN);
		drawFPath(g2d, F_R);
	}

	// y1: y position of current node, y2: y position of next node
	private void drawTree(Graphics2D g, SweeplineNode n, float y1, float dist_y) {
		g.setColor(Color.BLACK);
		if (n != null) {
			drawTree(g, n.getLeft(), y1 + dist_y, dist_y);
			drawTree(g, n.getRight(), y1 + dist_y, dist_y);
			drawNode(g, n, y1);

			if (n.getLeft() != null) {
				g.drawLine( n.getX(), (int)y1, n.getLeft().getX(),(int)(y1 + dist_y));
			}
			if (n.getRight() != null) {
				g.drawLine( n.getX(), (int)y1, n.getRight().getX(), (int)(y1 + dist_y));
			}
		}
	}

	private void drawNode(Graphics2D g, SweeplineNode n, float y) {
		n.setX(translateX(n.getDiscriminant()));
		n.setY((int) y);
		// draw Windows
		if (n.getWindow() != null) {
			g.drawRect(n.getX(), n.getY(), 4, 4);
			g.drawString("cnt: " + n.getWindow().getCount(), n.getX(), n.getY());
		} else {
			g.drawOval(n.getX(), n.getY(), 2, 2);
			// g.drawString(n.idxV + "", (int) x, (int) y);
		}
	}

	private void drawFPath(Graphics2D g, ArrayList<SweeplineNode> F_Path) {
		if (F_Path != null) {
			g.setColor(new Color((int) (Math.random() * 200), (int) (Math.random() * 200), (int) (Math.random() * 200)));
//			Stroke oldStroke = g.getStroke();
//			g.setStroke(new BasicStroke(3));
			for (int i = 0; i <(F_Path.size() -1); i++) {
				int x1 = F_Path.get(i).getX();
				int y1 = F_Path.get(i).getY();
				int x2= F_Path.get(i + 1).getX();
				int y2 = F_Path.get(i + 1).getY();
				g.drawLine(x1, y1, x2, y2);
			}
//			g.setStroke(oldStroke);
		}
	}

	private void processCandidateBorders(Graphics2D g2d) {  
		if (borders != null && borders.length > 0) {
			g2d.drawRect(translateX(borders[0]), translateY(borders[2]), translateX(borders[1]) - translateX(borders[0]), translateY(borders[3])
					- translateY(borders[2]));
		}
	}

	private void processCandidatePoints(Graphics2D g2d) {
		if (pointsInProcessing != null && pointsInProcessing.size() > 0) {
			for (MyPoint point : pointsInProcessing) {
				g2d.setColor(Color.red);
				g2d.fillOval(translateX(point.getX()),
						translateY(point.getY()), 5, 5);
			}
		}
	}

	private void processAllPoints(Graphics2D g2d){
		if (allPoints != null && allPoints.size() > 0) {
			for (MyPoint point : allPoints) {
				g2d.setColor(Color.GRAY);
				g2d.drawOval(translateX(point.getX()),
						translateY(point.getY()), 3, 3);
			}
		}
	}
	
	private void processEventsRectangles(Graphics2D g2d) {
		if (eventsRectangle != null) {
			Iterator<Event> it = eventsRectangle.iterator();
			while (it.hasNext()) {
				Event e = it.next();
				Interval2D rect = e.getRect();
				int x1 = translateX(rect.intervalHorizontal.getLow());
				int x2 = translateX(rect.intervalHorizontal.getHigh());
				int y1 = translateY(rect.intervalVertical.getLow());
				int y2 = translateY(rect.intervalVertical.getHigh());
				g2d.setColor(new Color(200, 200, 200));
				g2d.drawRect(x1, y1, x2 - x1, y2 - y1);
			}
		}
	}

	private void processEventLine(Graphics2D g2d) {
		if (event != null) {
			Interval2D rect = event.getRect();
			int x1 = translateX(rect.intervalHorizontal.getLow());
			int x2 = translateX(rect.intervalHorizontal.getHigh());
			int y_OfLine = translateY(event.getTime());
			g2d.setColor(Color.BLUE);
			g2d.setStroke(new BasicStroke(3));
			g2d.drawLine(translateX(borders[0]), y_OfLine, translateX(borders[1]), y_OfLine);
			g2d.setColor(Color.RED);
			g2d.drawLine(x1, y_OfLine, x2, y_OfLine);
		}
	}

	private void processWindow(Graphics2D g2d) {
		if (windows != null) {
			SweeplineNode[] nodes = windows.treeNodes;
			for (SweeplineNode sweeplineNode : nodes) {
				if (sweeplineNode == null)
					continue;
				Interval1D window = sweeplineNode.getWindow();
				if (window != null) {
					// only draw those inside
					int x1 =  translateX(window.getLow());
					int x2 =  translateX(window.getHigh());
					int width = x2 - x1;
					int y1 = translateY(borders[2]);
					int height = translateY(borders[3]) - translateY(borders[2]);
					Color myColour = new Color(255, 0, 0, (int) (100.0 * (window.getCount()-1) / pointsInProcessing.size()));
					g2d.setColor(myColour);
					g2d.fillRect(x1, y1, width, height);
					Stroke oldStroke = g2d.getStroke();
					BasicStroke dashed =  new BasicStroke(1.0f,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,10.0f, new float[]{9}, 0.0f);
					g2d.setStroke(dashed);
					g2d.setColor(new Color(20, 20, 20));
					g2d.drawLine(x1, 0, x1, graphHeight);
					g2d.setStroke(oldStroke);
				}
			}
		}
	}

	private void drawMaxWindow(Graphics2D g2d) {
		// draw result
		if (thisMaxWindow != null) {
			g2d.setColor(Color.RED);

			int width = (int) (Main.myQuery.getQueryWidth() * graphWidth);
			int height = (int) (Main.myQuery.getQueryHeight() * graphHeight);
			int x1 = translateX(thisMaxWindow.getHigh()) - width;
			int y1 = translateY(thisMaxWindow.getHeight()) - height;
			g2d.drawRect(x1, y1, width, height);
		}
	}

	private int translateX(double x) {
		return (int) ((x - borders[0]) * graphWidth);
	}

	private int translateY(double y) {
		return (int) ((y - borders[2]) * graphHeight);
	}
}