package myGrid;

import java.util.Arrays;
import java.util.LinkedHashSet;

public abstract class AbsNodeMinMaxBounds {
	protected TNode tNode;
	
	public AbsNodeMinMaxBounds(TNode tNode) {
		this.tNode = tNode;
	}

	protected int calLowerBound(String keyword){
		int lowerBound = 0;
		for (Node childNode : tNode.getChildren()) {
			if(childNode.getLevel()==Grid.getInstance().getTreeHeight()){
				if(((Cell)childNode).getKeyword_pMap()!=null && ((Cell)childNode).getKeyword_pMap().get(keyword)!=null)
					lowerBound = Math.max(lowerBound, ((Cell)childNode).getKeyword_pMap().get(keyword).size());
			}else{
				if(((TNode)childNode).hasPoints())
				lowerBound = Math.max(lowerBound, ((TNode)childNode).calPoint(new LinkedHashSet<String>(Arrays.asList(keyword))).size());
			}
		}
		return lowerBound;
	}
	
	protected int calUpperBound(String keyword){
		if(Main.useAccurateMAX){
			int upperBound=0;
			for (Node childNode : tNode.getChildren()) {
				upperBound = Math.max(childNode.calPointsMAXNeighbourS(keyword), upperBound);
			}
			return upperBound;
		}else{
			int iThInGrid = tNode.getiThInGrid();
			int jThInGrid = tNode.getjThInGrid();
			int sumSouthWest = tNode.calRelevantPointCountESWN4(keyword, new int[]{iThInGrid-1, iThInGrid}, new int[]{jThInGrid-1, jThInGrid});
			int sumNorthWest = tNode.calRelevantPointCountESWN4(keyword, new int[]{iThInGrid-1, iThInGrid}, new int[]{jThInGrid, jThInGrid+1});
			int sumSouthEast = tNode.calRelevantPointCountESWN4(keyword, new int[]{iThInGrid, iThInGrid+1}, new int[]{jThInGrid-1, jThInGrid});
			int sumNorthEast = tNode.calRelevantPointCountESWN4(keyword, new int[]{iThInGrid, iThInGrid+1}, new int[]{jThInGrid, jThInGrid+1});
			return Math.max(Math.max(sumSouthEast, sumSouthWest), Math.max(sumNorthEast, sumNorthWest)); 
		}
	}
	
}
