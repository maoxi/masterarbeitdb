package myGrid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class Cell extends Node {
	private LinkedHashSet<MyPoint> listOfPoints = new LinkedHashSet<MyPoint>();
	protected HashMap<String, ArrayList<MyPoint>> keyword_pMap;
	
	public Cell(double x1, double y1, double x2, double y2, int level, int nodeType) {
		super(x1, y1, x2, y2, level, nodeType);
	}

	public LinkedHashSet<MyPoint> getListOfPoints() {
		return listOfPoints;
	}
	
//	@Override
//	public HashMap<String, Integer> createKeyword_pCntMap() {
//		HashSet<String> allTopTags =  TwitterDataLoader.getInstance().getTopHashTags(); 
//		if(allTopTags==null || allTopTags.size()==0)
//			throw new RuntimeException("No Keywordfrequency Map found, data not imported correctly");
//		
//		keyword_pCntMap = new HashMap<String, Integer>();
//		for(MyPoint p: listOfPoints){
//			for(String keyword :p.getKeywords()){
//				int countKeyword= keyword_pCntMap.get(keyword)==null? 1:keyword_pCntMap.get(keyword)+1;
//				keyword_pCntMap.put(keyword, countKeyword);
//			}
//		}
//		return keyword_pCntMap;
//	}

	public HashMap<String, ArrayList<MyPoint>> createKeyword_pMap() {
		HashSet<String> allTopTags =  TwitterDataLoader.getInstance().getTopHashTags(); 
		if(allTopTags==null || allTopTags.size()==0)
			throw new RuntimeException("No Keywordfrequency Map found, data not imported correctly");
		
		for(MyPoint p: listOfPoints){
			for(String keyword :p.getKeywords()){
				if(keyword_pMap == null)	
					keyword_pMap = new HashMap<String, ArrayList<MyPoint>>();
				if(keyword_pMap.get(keyword)==null){
					ArrayList<MyPoint> points = new ArrayList<MyPoint>();
					points.add(p);
					keyword_pMap.put(keyword, points);
				}else{
					ArrayList<MyPoint> oldList = keyword_pMap.get(keyword);
					oldList.add(p);
					keyword_pMap.remove(keyword);
					keyword_pMap.put(keyword, oldList);
				}
			}
		}
		return keyword_pMap;
	}
	
	@Override
	public LinkedHashSet<MyPoint> calPoint(LinkedHashSet<String> keywords){
		LinkedHashSet<MyPoint> returnPoints = null;
//		if (getListOfPoints() != null) {
//			for (MyPoint p : getListOfPoints()) {
//				if(p.containCertainKeywords(keywords))
//					returnPoints.add(p);
//			}
//		}
		for(String keyword: keywords){
			if(getKeyword_pMap()!=null && getKeyword_pMap().get(keyword)!=null){
				if(returnPoints == null)
					returnPoints = new LinkedHashSet<MyPoint>();
				returnPoints.addAll(getKeyword_pMap().get(keyword));
			}
		}
		return returnPoints;
	}

	public HashMap<String, ArrayList<MyPoint>> getKeyword_pMap() {
		return keyword_pMap;
	}
	
	public void addPoint(MyPoint p) {
		listOfPoints.add(p);
	}

	@Override
	public String toString() {
		return "Cell is at level: " + level + " index: [" + iThInGrid + ", " + jThInGrid + "] |  ( " + x1 + " , " + y1 + " , " + x2 + " , " + y2+"), with "+calPoint(new LinkedHashSet<String>()).size()+" points";
	}
}
