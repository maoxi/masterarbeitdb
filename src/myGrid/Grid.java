package myGrid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Grid {
	private static Grid instance;

	private int treeFan;
	private int treeHeight; // TNodes from [1, treeHeight-1], Cells lies on the level= treeHeight

	private double MIN_X;
	private double MIN_Y;
	private double MAX_X;
	private double MAX_Y;
	private double xCellWidth; // width of cell on x axis, calculated through (Max-Min)/(idxX-1)
	private double yCellWidth;
	private int xCellCntInGrid; // cell count from the whole Grid on x axis
	private int yCellCntInGrid;

	public HashSet<MyPoint> datasetAsPoints = new HashSet<MyPoint>();

	private Map<Integer, Node[][]> tree; // integer为Height, TNode[][] 为越底层越细分的Node数组
	private HashMap<Integer, TreeMap<String, LinkedHashSet<Node>>> levelCandidates;
	
	public static Grid getInstance() {
		if (instance != null)
			return instance;
		else{
			return null;
		}
//			throw new RuntimeException("No data tree is created!!!");
	}

	public static void init(double MIN_X, double MIN_Y, double MAX_X, double MAX_Y, int treeFan, int treeHeight) {
		instance = new Grid(MIN_X, MIN_Y, MAX_X, MAX_Y, treeFan, treeHeight);
	}

	private Grid(double MIN_X, double MIN_Y, double MAX_X, double MAX_Y, int treeFan, int treeHeight) {
		this.MIN_X = MIN_X;
		this.MIN_Y = MIN_Y;
		this.MAX_X = MAX_X;
		this.MAX_Y = MAX_Y;
		this.xCellCntInGrid = (int) Math.pow(treeFan, treeHeight - 1);
		this.yCellCntInGrid = (int) Math.pow(treeFan, treeHeight - 1);
		this.xCellWidth = ((MAX_X - MIN_X) / (xCellCntInGrid));
		this.yCellWidth = ((MAX_Y - MIN_Y) / (yCellCntInGrid));
		System.out.println("MIN_X = " + MIN_X + " MAX_X = " + MAX_X + " xCellWidth = " + xCellWidth);
		System.out.println("MIN_Y = " + MIN_Y + " MAX_Y = " + MAX_Y + " yCellWidth = " + yCellWidth);

		if (treeFan == 0 || treeHeight == 0)
			return;
		this.treeFan = treeFan;
		this.treeHeight = treeHeight;
	}

	public void createTree() {
		prepareTree();
		if (xCellCntInGrid != yCellCntInGrid) {
			throw new RuntimeException("HG needs same number of cells in the X and Y dimensions...exiting.");
		}

		// double fanout = Math.pow(xCellCntInGrid * yCellCntInGrid, 1.0 / treeHeight); //
		// Height越大，fanout越�?，fanout为该层cell数
		// if (Math.abs(fanout - (Math.round(fanout))) > 0.1) {
		// throw new RuntimeException("HG VARIABLES ARE BAD.");
		// }

		System.out.println("Creating Hierarchical grid with Height: " + treeHeight + " Granularity: " + xCellCntInGrid + " x " + xCellCntInGrid);
		long startTime = System.currentTimeMillis();

		buildNode(MIN_X, MIN_Y, MAX_X, MAX_Y, 1, null);
//		((TNode) tree.get(1)[0][0]).createKeyword_pMap();
		Node[][] cells =  tree.get(treeHeight);
		for (int i = 0; i<cells.length; i++){
			for (int j = 0; j<cells[i].length; j++){
					((Cell)cells[i][j]).createKeyword_pMap();
			}
		}
		
		if(Main.useCandidates){
			levelCandidates = new HashMap<Integer, TreeMap<String,LinkedHashSet<Node>>>();
			for(int c_level= treeHeight-1; c_level>=1; c_level--){
				Node[][] nodes = tree.get(c_level);
				for (int i = 0; i<nodes.length; i++){
					for (int j = 0; j<nodes[i].length; j++){
						if(((TNode)nodes[i][j]).hasPoints()) //if has points inside
							((TNode)nodes[i][j]).getNodeMinMaxBoundsNew().create();
					}
				}
			}
			if(Main.pruneRedundantCandidate)
				pruneRedundantCandidate();
			if(Main.useLocalKWGroup){
				for(int c_level= treeHeight-1; c_level>=1; c_level--){
					Node[][] nodes = tree.get(c_level);
					for (int i = 0; i<nodes.length; i++){
						for (int j = 0; j<nodes[i].length; j++){
							if(((TNode)nodes[i][j]).hasPoints()) //if has points inside
								((TNode)nodes[i][j]).getNodeMinMaxBoundsNew().groupKeywords();
						}
					}
				}
			}
		}else{
			for(int c_level= treeHeight-1; c_level>=1; c_level--){
				Node[][] nodes = tree.get(c_level);
				for (int i = 0; i<nodes.length; i++){
					for (int j = 0; j<nodes[i].length; j++){
						if(((TNode)nodes[i][j]).hasPoints()) //if has points inside
							((TNode)nodes[i][j]).getNodeMinMaxBoundsOld().create();
				     }
			     }
			}
//			((TNode) tree.get(1)[0][0]).getNodeMinMaxBoundsOld().create();
		}

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tree Construction Time: " + elapsedTime + " ms");
	}

	private void prepareTree() {
		tree = new HashMap<Integer, Node[][]>();
		for (int idxHeight = 1; idxHeight <= treeHeight; idxHeight++) {
			int xNodeCnt = (int) Math.pow(treeFan, idxHeight - 1); // node count for each tree level
			int yNodeCnt = (int) Math.pow(treeFan, idxHeight - 1);

			Node[][] treeGrid = new Node[xNodeCnt][yNodeCnt];
			tree.put(idxHeight, treeGrid);
		}
		
		// fill cells with data, �?始化第一层，(底层)的cell
		for (int i = 0; i < xCellCntInGrid; i++) {
			for (int j = 0; j < yCellCntInGrid; j++) {
				double cell_minX = MIN_X + (i * xCellWidth);
				double cell_minY = MIN_Y + (j * yCellWidth);
				double cell_maxX = MIN_X + ((i + 1) * xCellWidth);
				double cell_maxY = MIN_Y + ((j + 1) * yCellWidth);
				Cell c = new Cell(cell_minX, cell_minY, cell_maxX, cell_maxY, treeHeight, Node.CELL);
				c.setIndex(i, j);
				for (MyPoint p : datasetAsPoints) {
					if (cell_minX <= p.getX() && p.getX() < cell_maxX && cell_minY <= p.getY() && p.getY() < cell_maxY) {
						c.addPoint(p);
					}
				}
				tree.get(treeHeight)[i][j] = c;
			}
		}
	}

	// length is in number of cells contained
	private Node buildNode(double x1, double y1, double x2, double y2, int level, Node parentNode) {
		int xThNodeInLevel = (int) ((x1 - MIN_X) / xCellWidth / (xCellCntInGrid / Math.pow(treeFan, level - 1)));
		int yThNodeInLevel = (int) ((y1 - MIN_Y) / yCellWidth / (yCellCntInGrid / Math.pow(treeFan, level - 1)));

		TNode currentNode = new TNode(x1, y1, x2, y2, level, Node.TN);
		currentNode.setParent(parentNode);
		Node[][] gridTable = tree.get(level);
		gridTable[xThNodeInLevel][yThNodeInLevel] = currentNode; // insert Tnode into tree
		currentNode.setIndex(xThNodeInLevel, yThNodeInLevel);

		ArrayList<Cell> childCells = getCells(x1, y1 , x2, y2);
		currentNode.setCellMembers(childCells);

		// 从level=1 处往level=treeHeight处迭代
		if (level == treeHeight - 1) { // is at level just above the leaf, recursive ended
			for (Cell cell : childCells) {
				cell.setParent(currentNode);
			}
			currentNode.setChildren(childCells);
		}
		else {
			ArrayList<Node> children = new ArrayList<Node>();
			// split the currentnode into childnodes, childCellCnt:这个Node有几个cell
			int childCellCnt = (int) (xCellCntInGrid / Math.pow(treeFan, level));
			for (int xx = 0; xx < treeFan; ++xx) {
				for (int yy = 0; yy < treeFan; ++yy) {
					double x_offset = xCellWidth * childCellCnt;
					double y_offset = yCellWidth * childCellCnt;
						Node child = buildNode(x1 + (xx * x_offset), y1 + (yy * y_offset), x1 + ((xx + 1) * x_offset), y1 + ((yy + 1) * y_offset), level + 1,
								currentNode);
						children.add(child);
					}
				}
			currentNode.setChildren(children);
		}

		return currentNode;
	}

	// return cells covered by this area
	// 计算 i,j, 返回table[i][j]中存储了cell
	ArrayList<Cell> getCells(double x1, double y1, double x2, double y2) {

		int q_x1 = (int) Math.round(((x1 - MIN_X) / xCellWidth));
		int q_y1 = (int) Math.round(((y1 - MIN_Y) / yCellWidth));
		int q_x2 = (int) Math.round(((x2 - MIN_X) / xCellWidth));
		int q_y2 = (int) Math.round(((y2 - MIN_Y) / yCellWidth));

		ArrayList<Cell> cells = new ArrayList<Cell>();

		// check to see if within boundary.
		if (q_x1 >= 0 && q_x1 < xCellCntInGrid && q_y1 >= 0 && q_y1 < yCellCntInGrid) {
			for (int i = q_x1; i < q_x2; ++i)
				for (int j = q_y1; j < q_y2; ++j)
					cells.add((Cell) tree.get(treeHeight)[i][j]);
			return cells;
		}
		else {
			System.out.println("Range unbounded.");
			return null;
		}
	}
	
	ArrayList<Cell> getCoveringCells(double x1, double y1, double x2, double y2) {
		int q_x1 = (int) Math.floor(((x1 - MIN_X) / xCellWidth));
		int q_y1 = (int) Math.floor(((y1 - MIN_Y) / yCellWidth));
		int q_x2 = (int) Math.ceil(((x2 - MIN_X) / xCellWidth));
		int q_y2 = (int) Math.ceil(((y2 - MIN_Y) / yCellWidth));
		
		ArrayList<Cell> cells = new ArrayList<Cell>();
		
		// check to see if within boundary.
		if (q_x1 >= 0 && q_x1 < xCellCntInGrid && q_y1 >= 0 && q_y1 < yCellCntInGrid) {
			for (int i = q_x1; i < q_x2; ++i)
				for (int j = q_y1; j < q_y2; ++j)
					cells.add((Cell) tree.get(treeHeight)[i][j]);
			return cells;
		}
		else {
			System.out.println("Range unbounded.");
			return null;
		}
	}
	
	public void pruneRedundantCandidate(){
		for(int level =1; level<= treeHeight-2; level++){
			TreeMap<String, LinkedHashSet<Node>>  candidatesCurrentL = levelCandidates.get(level);
			if(candidatesCurrentL == null || candidatesCurrentL.size()==0)
				continue;
			//foreach String
			Iterator<Entry<String, LinkedHashSet<Node>>> iter_keyword = candidatesCurrentL.entrySet().iterator();
			while(iter_keyword.hasNext()){
				Entry<String, LinkedHashSet<Node>> entry = iter_keyword.next();
				String keyword = entry.getKey();
				Iterator<Node> iter_nodes = entry.getValue().iterator();
				//for each Nodes, get its child nodes.
				while(iter_nodes.hasNext()){
					TNode parentNode = (TNode)iter_nodes.next();
					int parentMin = (parentNode).getNodeMinMaxNew().get(keyword).getMin();
					int parentMax = (parentNode).getNodeMinMaxNew().get(keyword).getMax();
					for(Node childNode :((TNode)parentNode).getChildren()){
						//if min/max in childNode is the same with parent(as long as one of those is found)
						if(	((TNode)childNode).getNodeMinMaxNew()!=null 
								&& ((TNode)childNode).getNodeMinMaxNew().get(keyword)!=null 
								&& ((TNode)childNode).getNodeMinMaxNew().get(keyword).getMax() == parentMax 
								&& ((TNode)childNode).getNodeMinMaxNew().get(keyword).getMin() == parentMin){
							MinMaxPair minMax = ((TNode)childNode).getNodeMinMaxNew().get(keyword);
							if(minMax.getRedudantLevel()==null){
								HashSet<Integer> savedLevel = ((TNode)parentNode).getNodeMinMaxNew().get(keyword).getRedudantLevel();
								if(savedLevel==null)
									minMax.setRedudantLevel(new HashSet<>(Arrays.asList(parentNode.getLevel())));
								else{//save those redundant levels from parents and parent itself to childNode
									savedLevel.add(parentNode.getLevel());
									minMax.setRedudantLevel(savedLevel);
								}
							}
							//delete the parent Node
							iter_nodes.remove();
							//delete the minMax of this keyword from this node
							parentNode.getNodeMinMaxNew().remove(keyword);
							break;
						}
					}
				}
				//if this keyword has no candidates necessary to be saved anymore
				if(entry.getValue().size()==0)
					iter_keyword.remove();
			}
		}
	}
	
	public void printTree() {
		System.out.println("-------------------------------------------------------------------------------------------------------------");
		Map<Integer, Node[][]> printTree = tree;
		for (Map.Entry<Integer, Node[][]> entry : printTree.entrySet()) {
			System.out.println("level: " + entry.getKey());
			Node[][] nodes = entry.getValue();
			int nodesLengthX = nodes.length;
			int nodesLengthY = nodes[nodesLengthX - 1].length;
			for (int i = 0; i < nodesLengthX; i++) {
				for (int j = 0; j < nodesLengthY; j++) {
					if (nodes[i][j] instanceof TNode) {
						TNode currentNode = (TNode) nodes[i][j];
						String nodeType;
						if (currentNode.getNodeType() == 0) {
							nodeType = "CELL";
						} else {
							nodeType = "TNode";
						}
						System.out.println(nodeType + " [" + currentNode.getiThInGrid() + ", " + currentNode.getjThInGrid() + "]: at " + currentNode.getX1()
								+ ", " + currentNode.getY1() + ", " + currentNode.getX2() + ", " + currentNode.getY2() + ", has point count = "
								+ currentNode.calPoint(new LinkedHashSet<String>()).size());
					} else {
						Cell currentCell = (Cell) nodes[i][j];
						String nodeType;
						if (currentCell.getNodeType() == 0) {
							nodeType = "CELL";
						} else {
							nodeType = "TNode";
						}
						System.out.println(nodeType + " [" + currentCell.getiThInGrid() + ", " + currentCell.getjThInGrid() + "]: at " + currentCell.getX1()
								+ ", " + currentCell.getY1() + ", " + currentCell.getX2() + ", " + currentCell.getY2() + ", has point count = "
								+ currentCell.calPoint(new LinkedHashSet<String>()).size());
					}
				}
			}
		}
	}

	public void setDatasetAsPoints(HashSet<MyPoint> datasetAsPoints) {
		this.datasetAsPoints = datasetAsPoints;
	}

	public void setMIN_X(double mIN_X) {
		MIN_X = mIN_X;
	}

	public void setMIN_Y(double mIN_Y) {
		MIN_Y = mIN_Y;
	}

	public void setMAX_X(double mAX_X) {
		MAX_X = mAX_X;
	}

	public void setMAX_Y(double mAX_Y) {
		MAX_Y = mAX_Y;
	}

	public void setIdxX(int idxX) {
		this.xCellCntInGrid = idxX;
	}

	public void setIdxY(int idxY) {
		this.yCellCntInGrid = idxY;
	}

	public int getIdxX() {
		return xCellCntInGrid;
	}

	public int getIdxY() {
		return yCellCntInGrid;
	}

	public double getMIN_X() {
		return MIN_X;
	}

	public double getMIN_Y() {
		return MIN_Y;
	}

	public double getMAX_X() {
		return MAX_X;
	}

	public double getMAX_Y() {
		return MAX_Y;
	}

	public int getTreeFan() {
		return treeFan;
	}

	public int getTreeHeight() {
		return treeHeight;
	}

	public double getxCellWidth() {
		return xCellWidth;
	}

	public double getyCellWidth() {
		return yCellWidth;
	}

	public Map<Integer, Node[][]> getTree() {
		return tree;
	}

	public HashSet<MyPoint> getDatasetAsPoints() {
		return datasetAsPoints;
	}
	
	public HashMap<Integer, TreeMap<String, LinkedHashSet<Node>>> getLevelCandidates()
	{
		return this.levelCandidates;
	}
}
