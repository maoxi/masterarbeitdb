package myGrid;

import graphics.GridFrame;
import graphics.SweeplineFrame;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.concurrent.TimeUnit;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

import mySweepline.MaxRS;
import objectexplorer.MemoryMeasurer;
import objectexplorer.ObjectGraphMeasurer;

//Check if its the same instance: System.identityHashCode(instance)
public class Main {
	public static GridFrame gridFrame;
	public static SweeplineFrame sweeplineFrame;
	public static TextQuery myQuery;
	public static final String NOKEYWORD = "NOKEYWORD";
	public static boolean useOriginalKeywordFrequency = true;
	public static boolean useSort = false;
	public static boolean useAccurateMAX = false;
	public static boolean useCandidates = false;
	public static boolean useLocalKWGroup = false; //only enabled when useCandidates set to true
	public static boolean pruneRedundantCandidate = false;
	public static boolean useGUI = true;
	public static int treeHeight = 7;
	public static int delayTime = 0;
	public static Timer timer;

	public static void main(String[] args) throws NumberFormatException, IOException {
		TwitterDataLoader.getInstance().init();
		Main.myQuery = new TextQuery();

		if (!useGUI) {
			prepareExperiment();
			updateGrid();
		}else{
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					gridFrame = new GridFrame();
					sweeplineFrame = new SweeplineFrame();
				}
			});
			updateGrid();
			updateQuery();
		}
	}

	public static void prepareExperiment(){
		myQuery.setKeywords(new LinkedHashSet<>(Arrays.asList("jobs")));
		try {
			TwitterDataLoader.getInstance().loadFilteredFile(new File("C:\\Users\\Shawn\\Documents\\20140211smallfiltered.txt"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		prepareInputData();
	}
	
	public static void prepareInputData(){
		if(Main.useOriginalKeywordFrequency){
			TwitterDataLoader.getInstance().calKeywordsFrequencyUsingDatasetPoints();
		}else{
			try {
				TwitterDataLoader.getInstance().calKeywordsFrequencyUsingFile("topHashtags.txt");
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			//filter those which are not in the topHashTags list.
			TwitterDataLoader.getInstance().filterNotTopHashTags();
		}
		if(Main.useSort)
			TwitterDataLoader.getInstance().sortKeywordsInPoint(); //resort the keywordList in Points using frequency
	}

	public static void updateGrid() throws NumberFormatException, IOException {
		if ((TwitterDataLoader.getInstance().getDatasetAsPoints() == null || TwitterDataLoader.getInstance().getDatasetAsPoints().size() == 0))
			return;
		if (Main.useGUI)
			gridFrame.getGridGraphic().clearGraph();
		int fan = 2;
		// int level = (int) Math.round(Math.log(twitterPoints.size()) / Math.log(fan)); //
		// 超过10电脑就拖�?动了
		Grid.init(0, 0, 1, 1, fan, treeHeight); // empty grid with no data TODO change to 8
		Grid.getInstance().setDatasetAsPoints(TwitterDataLoader.getInstance().getDatasetAsPoints());
		TwitterDataLoader.getInstance().emptyPoints();
		Grid.getInstance().createTree();
		System.gc();
		System.out.println("grid containing: "+ ObjectGraphMeasurer.measure(Grid.getInstance()));
		System.out.println("grid measured size: "+ MemoryMeasurer.measureBytes(Grid.getInstance()));
		
		if (Main.useGUI)
			gridFrame.getGridGraphic().repaint();
		updateQuery();
	}

	public static void updateQuery() throws NumberFormatException, IOException {
		if (myQuery.getKeywords() == null || myQuery.getKeywords().size() == 0)
			return;
		if (Main.useGUI){
			gridFrame.getGridGraphic().clearGraph();
			gridFrame.getGridGraphic().setKeywordPoints();
		}
		myQuery.query();

		// running sweepline to get results
		System.out.println("-------------------------------------------------------------------");
		if (Main.useGUI)
			gridFrame.getGridGraphic().setCandidateNodeList();

		MaxRS.prepareRunningSweepline();

		System.out.println("max range is: [ " + MaxRS.maxWindow.getLow() + ", " + MaxRS.maxWindow.getHigh() + "], with count of points= "
				+ (MaxRS.maxWindow.getCount() - 1));
		double x1 = MaxRS.maxWindow.getHigh() - Main.myQuery.getQueryWidth();
		double x2 = MaxRS.maxWindow.getHigh();
		double y1 = MaxRS.maxWindow.getHeight() - Main.myQuery.getQueryHeight();
		double y2 = MaxRS.maxWindow.getHeight();
		ArrayList<Cell> cells = Grid.getInstance().getCoveringCells(x1, y1, x2, y2);
		ArrayList<MyPoint> points = new ArrayList<MyPoint>();
		for(Cell cell: cells){
			points.addAll(cell.getListOfPoints());
		}
		for (MyPoint point : points) {
			if (point.getX() >= x1 && point.getX() <= x2 && point.getY() >= y1 && point.getY() <= y2) {
				if (point.containCertainKeywords(Main.myQuery.getKeywords())) {
					System.out.println("Point with ID = " + point.getID() + " is found in this max range");
				}
			}
		}
	}

	public static void delayShort() {
		try {
			TimeUnit.MILLISECONDS.sleep(delayTime); // 500
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void delayLong() {
		try {
			TimeUnit.MILLISECONDS.sleep(delayTime * 3); // 1500
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
