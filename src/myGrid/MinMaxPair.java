package myGrid;

import java.util.HashSet;

public class MinMaxPair {
	private int min, max;
	private HashSet<Integer> redudantLevel; //used to mark whether this Min/Max is the toppest update for keyword
	
	public MinMaxPair(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMax() {
		return max;
	}

	public int getMin() {
		return min;
	}
	
	public HashSet<Integer> getRedudantLevel() {
		return redudantLevel;
	}
	
	public void setMax(int max) {
		this.max = max;
	}
	public void setMin(int min) {
		this.min = min;
	}
	
	public void setRedudantLevel(HashSet<Integer> redudantLevel) {
		this.redudantLevel = redudantLevel;
	}
	
	public int compareTo(MinMaxPair o){
		if ((this.min > o.getMin()) || ((this.min == o.getMin()) && (this.max > o.getMax()))) {
			return 1;
		}
		if ((this.min == o.getMin()) && (this.max == o.getMax())) {
			return 0;
		}
		return -1;
	}
}
