package myGrid;

import java.util.LinkedHashSet;

public class MyPoint {

	private final long p_id;
	private double p_x, p_y;
	private LinkedHashSet<String> keywords;

	public MyPoint(double x, double y, long id, LinkedHashSet<String> keywords) {
		this.p_x = x;
		this.p_y = y;
		this.p_id = id;
		this.keywords = keywords;
	}

	public void setP_x(double p_x) {
		this.p_x = p_x;
	}
	
	public void setP_y(double p_y) {
		this.p_y = p_y;
	}
	
	public double getX() {
		return p_x;
	}

	public double getY() {
		return p_y;
	}

	public long getID() {
		return p_id;
	}

	@Override
	public String toString() {
		return "ID: " + p_id + " -> (" + p_x + ", " + p_y + ")" + keywords.toString();
	}

	public LinkedHashSet<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(LinkedHashSet<String> keywords) {
		this.keywords = keywords;
	}

	public boolean containCertainKeywords(LinkedHashSet<String> certainKeywords) {
		if(keywords == null || certainKeywords == null)
			return false;
		// return true, so long as one keyword contained in this point
		for (String certainKeyword : certainKeywords) {
			if (keywords.contains(certainKeyword))
				return true;
		}
		return false;
	}

	@Override
    public boolean equals(Object obj) {
		if (this.p_id == ((MyPoint) obj).getID()) {
			return true;
		}
		return false;
    }

	@Override
	public int hashCode() {
		return (""+p_id).hashCode();
	}
}
