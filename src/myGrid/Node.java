package myGrid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

public abstract class Node{
	public static final int CELL = 0;
	public static final int TN = 1;

	protected int iThInGrid, jThInGrid; // 在Grid中x方�?�上的第i個Node，used mainly for cal Upperbound,
										// begins with 0

	protected double x1, x2, y1, y2;
	protected int level;
	protected int nodeType;
	//for those in Cell should be calculated at the beginning, 
	//for those in TNode should be generated in runtime and deleted after usage.
//	protected HashMap<String, Integer> keyword_pCntMap;

	protected Node parent;

	public Node(double _x1, double _y1, double _x2, double _y2, int _level, int nodeType)
	{
		setDimensions(_x1, _y1, _x2, _y2);
		level = _level;
		this.nodeType = nodeType;
	}

	private void setDimensions(double _x1, double _y1, double _x2, double _y2) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
	}
	
	// i.te in current level in Grid, begin with 0
	public void setIndex(int iThInGrid, int jThInGrid) { 
		this.iThInGrid = iThInGrid;
		this.jThInGrid = jThInGrid;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public int getiThInGrid() {
		return iThInGrid;
	}

	public int getjThInGrid() {
		return jThInGrid;
	}

	public double getX1() {
		return x1;
	}

	public double getX2() {
		return x2;
	}

	public double getY1() {
		return y1;
	}

	public double getY2() {
		return y2;
	}

	public int getLevel() {
		return level;
	}

	public int getNodeType() {
		return nodeType;
	}

	public Node getParent() {
		return parent;
	}

//	public HashMap<String, Integer> getKeyword_pCntMap() {
//		return keyword_pCntMap;
//	}

	// count points for keywordList
	public abstract LinkedHashSet<MyPoint> calPoint(LinkedHashSet<String> keywords);

//	public abstract HashMap<String, Integer> createKeyword_pCntMap();
//	public abstract HashMap<String, ArrayList<MyPoint>> createKeyword_pMap();
	
	public int calRelevantPointCountESWN4(String keyword, int[] indexX, int[] indexY) {
		int returnCount = 0;
		Node[][] nodeCurrentLevel = Grid.getInstance().getTree().get(level);
		for (int i : indexX) {
			for (int j : indexY) {
				if (i >= 0 && i < nodeCurrentLevel.length && j >= 0 && j < nodeCurrentLevel[i].length) {
					LinkedHashSet<MyPoint> ps = nodeCurrentLevel[i][j].calPoint(new LinkedHashSet<String>(Arrays.asList(keyword)));
					if(ps!=null)
						returnCount += ps.size();
				}
			}
		}
		return returnCount;
	}

	// return all the queried points within the surrounding neighbour
	public ArrayList<MyPoint> calPointsSWPLNeighbourL(boolean relevantPntsOnly) {
		ArrayList<MyPoint> listOfPoints = null;
		Node[][] nodesCurrentLevel = Grid.getInstance().getTree().get(level);
		for (int i = iThInGrid - 1; i <= iThInGrid + 1; i++) {
			for (int j = jThInGrid - 1; j <= jThInGrid + 1; j++) {
				// if i and j within Grid
				if (i >= 0 && i < nodesCurrentLevel.length && j >= 0 && j < nodesCurrentLevel[i].length) {
					LinkedHashSet<MyPoint> points;
					if (relevantPntsOnly)
						points = nodesCurrentLevel[i][j].calPoint(Main.myQuery.getKeywords());
					else
						points = nodesCurrentLevel[i][j].calPoint(new LinkedHashSet<String>(Arrays.asList(Main.NOKEYWORD)));
					if(points!=null){
						if(listOfPoints==null)
							listOfPoints = new ArrayList<MyPoint>();
						listOfPoints.addAll(points);
					}
				}
			}
		}
		return listOfPoints;
	}

	public ArrayList<Node> calNodesSWPLNeighbourL() {
		ArrayList<Node> neighbourNodes = null;
		Node[][] nodesCurrentLevel = Grid.getInstance().getTree().get(level);
		for (int i = iThInGrid - 1; i <= iThInGrid + 1; i++) {
			for (int j = jThInGrid - 1; j <= jThInGrid + 1; j++) {
				// if i and j within Grid
				if (i >= 0 && i < nodesCurrentLevel.length && j >= 0 && j < nodesCurrentLevel[i].length) {
					if(neighbourNodes == null)
						neighbourNodes = new ArrayList<Node>();
					neighbourNodes.add(nodesCurrentLevel[i][j]);
				}
			}
		}
		return neighbourNodes;
	}

	public double[] calBordersSWPLNeighbourL() {
		double left, right, top, bottom = 0;
		Node[][] nodesCurrentLevel = Grid.getInstance().getTree().get(level);
		left = (iThInGrid - 1 >= 0) ? nodesCurrentLevel[iThInGrid - 1][jThInGrid].getX1() : x1;
		right = (iThInGrid + 1 < nodesCurrentLevel.length) ? nodesCurrentLevel[iThInGrid + 1][jThInGrid].getX2() + Main.myQuery.getQueryWidth() : x2
				+ Main.myQuery.getQueryWidth();
		top = (jThInGrid - 1 >= 0) ? nodesCurrentLevel[iThInGrid][jThInGrid - 1].getY1() : y1;
		bottom = (jThInGrid + 1 < nodesCurrentLevel.length) ? nodesCurrentLevel[iThInGrid][jThInGrid + 1].getY2() + Main.myQuery.getQueryHeight() : y2
				+ Main.myQuery.getQueryHeight();

		double[] borders = { left, right, top, bottom };
		return borders;
	}

	/* refined neighbours with smaller border */
	// used in refined neighbour to calculate 4 set of 9 cells for MAX
	public int calPointsMAXNeighbourS(String keyword) {
		LinkedHashSet<String> temp = new LinkedHashSet<String>();
		temp.add(keyword);
		int count = 0;
		Node[][] nodesCurrentLevel = Grid.getInstance().getTree().get(level);
		for (int i = iThInGrid - 1; i <= iThInGrid + 1; i++) {
			for (int j = jThInGrid - 1; j <= jThInGrid + 1; j++) {
				// if i and j within Grid
				if (i >= 0 && i < nodesCurrentLevel.length && j >= 0 && j < nodesCurrentLevel[i].length) {
					if(nodesCurrentLevel[i][j].calPoint(temp)!=null)
						count += nodesCurrentLevel[i][j].calPoint(temp).size();
				}
			}
		}
		return count;
	}

	public ArrayList<MyPoint> calPointsSWPLNeighbourS(boolean relevantPntsOnly) {
		ArrayList<MyPoint> listOfPoints = null;
		if (this instanceof Cell) {
			return calPointsSWPLNeighbourL(relevantPntsOnly);
		} else {
			Node[][] nodesChildLevel = Grid.getInstance().getTree().get(level + 1);
			Node firstChild = ((TNode) this).getChildren().get(0);
			int child_iThInGrid = firstChild.getiThInGrid();
			int child_jThInGrid = firstChild.getjThInGrid();
			for (int i = child_iThInGrid - 1; i <= child_iThInGrid + 2; i++) {
				for (int j = child_jThInGrid - 1; j <= child_jThInGrid + 2; j++) {
					// if i and j within Grid
					if (i >= 0 && i < nodesChildLevel.length && j >= 0 && j < nodesChildLevel[i].length) {
						LinkedHashSet<MyPoint> points;
						if (relevantPntsOnly)
							points = nodesChildLevel[i][j].calPoint(Main.myQuery.getKeywords());
						else
							points = nodesChildLevel[i][j].calPoint(new LinkedHashSet<String>(Arrays.asList(Main.NOKEYWORD)));
						if(points!=null){
							if(listOfPoints == null)
								listOfPoints = new ArrayList<MyPoint>();
							listOfPoints.addAll(points);
						}
					}
				}
			}
			return listOfPoints;
		}
	}

	public ArrayList<Node> calNodesSWPLNeighbourS() {
		ArrayList<Node> neighbourNodes = null;
		Node[][] nodesChildLevel = Grid.getInstance().getTree().get(level + 1);
		if (this instanceof Cell) {
			return calNodesSWPLNeighbourL();
		} else {
			Node firstChild = ((TNode) this).getChildren().get(0);
			int child_iThInGrid = firstChild.getiThInGrid();
			int child_jThInGrid = firstChild.getjThInGrid();
			for (int i = child_iThInGrid - 1; i <= child_iThInGrid + 2; i++) {
				for (int j = child_jThInGrid - 1; j <= child_jThInGrid + 2; j++) {
					// if i and j within Grid
					if (i >= 0 && i < nodesChildLevel.length && j >= 0 && j < nodesChildLevel[i].length) {
						if(neighbourNodes == null)
							neighbourNodes = new ArrayList<Node>();
						neighbourNodes.add(nodesChildLevel[i][j]);
					}
				}
			}
			return neighbourNodes;
		}
	}

	public double[] calBordersSWPLNeighbourS() {
		double left, right, top, bottom = 0;
		Node[][] nodesChildLevel = Grid.getInstance().getTree().get(level + 1);
		if (this instanceof Cell) {
			return calBordersSWPLNeighbourL();
		} else {
			Node firstChild = ((TNode) this).getChildren().get(0);
			int child_iThInGrid = firstChild.getiThInGrid();
			int child_jThInGrid = firstChild.getjThInGrid();
			left = (child_iThInGrid - 1 >= 0) ? nodesChildLevel[child_iThInGrid - 1][child_jThInGrid].getX1() : x1;
			right = (child_iThInGrid + 2 < nodesChildLevel.length) ? nodesChildLevel[child_iThInGrid + 2][jThInGrid].getX2() + Main.myQuery.getQueryWidth()
					: x2
							+ Main.myQuery.getQueryWidth();
			top = (child_jThInGrid - 1 >= 0) ? nodesChildLevel[child_iThInGrid][child_jThInGrid - 1].getY1() : y1;
			bottom = (child_jThInGrid + 2 < nodesChildLevel.length) ? nodesChildLevel[child_iThInGrid][child_jThInGrid + 2].getY2()
					+ Main.myQuery.getQueryHeight() : y2
					+ Main.myQuery.getQueryHeight();
			double[] borders = { left, right, top, bottom };
			return borders;
		}
	}

    @Override
	public boolean equals(Object obj) {
    	if ((this.level == ((Node) obj).getLevel()) && (this.iThInGrid == ((Node) obj).getiThInGrid()) && (this.jThInGrid == ((Node) obj).getjThInGrid())) {
			return true;
		}
		return false;
    }

	@Override
	public int hashCode() {
		return (""+level).hashCode()+(""+iThInGrid).hashCode()+(""+jThInGrid).hashCode();
	}
}
