package myGrid;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.TreeMap;

import utilities.SortFunctions;

public class NodeMinMaxBoundsNew extends AbsNodeMinMaxBounds{
	private LinkedHashMap<String, MinMaxPair> nodeMinMax; //should not be initialized till really be used

	public NodeMinMaxBoundsNew(TNode tNode) {
		super(tNode);
	}

	public void setNodeMinMaxBoundMap(LinkedHashMap<String, MinMaxPair> keywordMinMax) {
		this.nodeMinMax = keywordMinMax;
	}

	public LinkedHashMap<String, MinMaxPair> getNodeMinMax() {
		return nodeMinMax;
	}

	public void create() {
		int level = tNode.getLevel();

		for (String keyword : TwitterDataLoader.getInstance().getTopHashTags()) {
			int min = calLowerBound(keyword);
			int max = calUpperBound(keyword);
			MinMaxPair currentLevelMinMax = new MinMaxPair(min, max);
			if(nodeMinMax==null)
				nodeMinMax = new LinkedHashMap<String, MinMaxPair>();
			if (min != 0) {
				HashMap<Integer, TreeMap<String, LinkedHashSet<Node>>> gridCandidates = Grid.getInstance().getLevelCandidates();
				if (gridCandidates.get(level) == null
						|| gridCandidates.get(level).size() == 0) {
					nodeMinMax.put(keyword, currentLevelMinMax);
					nodeMinMax = SortFunctions.sortMapByKeywordFrequency(nodeMinMax);
					LinkedHashSet<Node> candidateNodes = new LinkedHashSet<Node>();
					candidateNodes.add(this.tNode);
					TreeMap<String, LinkedHashSet<Node>> keyNodesMap = new TreeMap<String, LinkedHashSet<Node>>();
					keyNodesMap.put(keyword, candidateNodes);
					gridCandidates.put(level, keyNodesMap);
				}
				else if (gridCandidates.get(level).get(keyword) == null
						|| (gridCandidates.get(level).get(keyword)).size() == 0) {
					nodeMinMax.put(keyword, currentLevelMinMax);
					nodeMinMax = SortFunctions.sortMapByKeywordFrequency(nodeMinMax);
					LinkedHashSet<Node> candidateNodes = new LinkedHashSet<Node>();
					candidateNodes.add(this.tNode);
					TreeMap<String, LinkedHashSet<Node>> keyNodesMap = gridCandidates.get(level);
					keyNodesMap.put(keyword, candidateNodes);
				}
				// if the new added Node has the best Min. Remove those whose max is smaller then
				// best min. from candidateList
				else if (((TNode) (gridCandidates.get(level).get(keyword).toArray()[0])).getNodeMinMaxNew().get(keyword) != null
						&& ((TNode) (gridCandidates.get(level).get(keyword).toArray()[0])).getNodeMinMaxNew().get(keyword).getMin() <= max) {
					nodeMinMax.put(keyword, currentLevelMinMax);
					nodeMinMax = SortFunctions.sortMapByKeywordFrequency(nodeMinMax);
					gridCandidates.get(level).get(keyword).add(tNode);
					LinkedHashSet<Node> sortedNodes = SortFunctions.sortSetByNodeMin(gridCandidates.get(level).get(keyword), keyword);
					if (tNode.equals(sortedNodes.toArray()[0])) {
						Iterator<Node> nodeIter = sortedNodes.iterator();
						while (nodeIter.hasNext()) {
							Node node = nodeIter.next();
							if ((((TNode) node).getNodeMinMaxNew().get(keyword)).getMax() < min) {
								((TNode) node).getNodeMinMaxNew().remove(keyword);
								nodeIter.remove();
							}
						}
					}
					gridCandidates.get(level).remove(keyword);
					gridCandidates.get(level).put(keyword, sortedNodes);
				}
//				for (Node childNode : this.tNode.getChildren()) {
//					// last condition: create only once by the first keyword
//					if ((childNode.getKeyword_pCntMap() != null) && (childNode.getKeyword_pCntMap().get(keyword) != null) &&
//							(this.tNode.getLevel() <= Grid.getInstance().getTreeHeight() - 2) && (((TNode) childNode).getNodeMinMaxNew() == null)) {
//						((TNode) childNode).getNodeMinMaxBoundsNew().create();
//					}
//				}
			}
		}
	}

	public void groupKeywords() {
		if(nodeMinMax==null)
			return;
		// Grouping keywords, if the keyword has similar frequencies, change the pointer to a same
		// min/max
		MinMaxPair minMax02 = null;
		MinMaxPair minMax26 = null;
		MinMaxPair minMax69 = null;
		TreeMap<String, MinMaxPair> nodeMinMaxChanged = new TreeMap<>();
		for (Map.Entry<String, MinMaxPair> entry : nodeMinMax.entrySet()) {
			// grouping according to max value
			int entryMax = entry.getValue().getMax();
			int entryMin = entry.getValue().getMin();
			if (entryMax < 2) {
				if (minMax02 == null)
					minMax02 = entry.getValue();
				else {
					minMax02.setMin(Math.min(minMax02.getMin(), entryMin));
					minMax02.setMax(Math.max(minMax02.getMax(), entryMax));
					nodeMinMaxChanged.put(entry.getKey(), minMax02);
				}
			} else if (entryMax < 6) {
				if (minMax26 == null)
					minMax26 = entry.getValue();
				else {
					minMax26.setMin(Math.min(minMax26.getMin(), entryMin));
					minMax26.setMax(Math.max(minMax26.getMax(), entryMax));
					nodeMinMaxChanged.put(entry.getKey(), minMax26);
				}
			} else if (entryMax < 9) {
				if (minMax69 == null)
					minMax69 = entry.getValue();
				else {
					minMax69.setMin(Math.min(minMax69.getMin(), entryMin));
					minMax69.setMax(Math.max(minMax69.getMax(), entryMax));
					nodeMinMaxChanged.put(entry.getKey(), minMax69);
				}
			}
		}
		nodeMinMax.putAll(nodeMinMaxChanged);
//		for (Node childNode : this.tNode.getChildren()) {
//			if ((childNode.getKeyword_pCntMap() != null) && (this.tNode.getLevel() <= Grid.getInstance().getTreeHeight() - 2)) {
//				((TNode) childNode).getNodeMinMaxBoundsNew().groupKeywords();
//			}
//		}
	}
}
