package myGrid;

import java.util.HashMap;

public class NodeMinMaxBoundsOld extends AbsNodeMinMaxBounds {
	private HashMap<String, HashMap<Integer, MinMaxPair>> nodeMinMaxBoundMap;
	
	public NodeMinMaxBoundsOld(TNode tNode) {
		super(tNode);
	}
	
	public void setNodeMinMaxBoundMap(HashMap<String, HashMap<Integer, MinMaxPair>> keywordMinMaxBoundSummary) {
		this.nodeMinMaxBoundMap = keywordMinMaxBoundSummary;
	}
	
	public HashMap<String, HashMap<Integer, MinMaxPair>> getNodeMinMaxBoundMap() {
		return nodeMinMaxBoundMap;
	}
	
	public void create() { 
		int level = tNode.getLevel();
		nodeMinMaxBoundMap = new HashMap<String, HashMap<Integer, MinMaxPair>>();
		
		for(final String keyword : TwitterDataLoader.getInstance().getTopHashTags()) {
			HashMap<Integer, MinMaxPair> keywordMinMaxBound = new HashMap<Integer, MinMaxPair>();
			int min = calLowerBound(keyword);
			int max = calUpperBound(keyword);
			if(min==0 && max ==0)
				continue;
			MinMaxPair currentLevelMinMax = new MinMaxPair(min, max);
			keywordMinMaxBound.put(level, currentLevelMinMax);
			
//			for (Node childNode : tNode.getChildren()) {
//				if(childNode.getKeyword_pCntMap()!=null && childNode.getKeyword_pCntMap().get(keyword)!= null){
//					if (tNode.getLevel() <= Grid.getInstance().getTreeHeight() - 2 && ((TNode)childNode).getNodeMinMaxOld()==null) {
//						((TNode) childNode).getNodeMinMaxBoundsOld().create();
//					}
//				}
//			}
			
			// update the lower-/upper-Bound for level down in the list of current Level
			// last level of TNode does not have ChildLevelMinMax anymore
			if (level < Grid.getInstance().getTreeHeight() - 1) {
				for (int l = level+1; l <= Grid.getInstance().getTreeHeight()-1; l++) {  //from child layer to the last TNode layer
					MinMaxPair tempMinMaxPair = new MinMaxPair(0, 0);
					for (Node childNode : tNode.getChildren()) {
						if(((TNode)childNode).getNodeMinMaxOld()!=null
							&&((TNode)childNode).getNodeMinMaxOld().size()!=0 
							&&((TNode)childNode).getNodeMinMaxOld().get(keyword) != null){
							MinMaxPair childMinMaxPair = ((TNode)childNode).getNodeMinMaxOld().get(keyword).get(l);
							// tempMinMaxPair is the Maximum of all lowerBounds and upperBounds
							tempMinMaxPair = new MinMaxPair(Math.max(tempMinMaxPair.getMin(), childMinMaxPair.getMin()), Math.max(tempMinMaxPair.getMax(),
									childMinMaxPair.getMax()));
						}else
							continue;
					}
					keywordMinMaxBound.put(l, tempMinMaxPair);  
				}
			}
			
			nodeMinMaxBoundMap.put(keyword, keywordMinMaxBound);
		}
	}
}
