package myGrid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class TNode extends Node {
	private ArrayList<? extends Node> children;

	private ArrayList<Cell> cellMembers; // a node stores several cells
	NodeMinMaxBoundsOld nodeMinMaxBoundsOld;
	NodeMinMaxBoundsNew nodeMinMaxBoundsNew;

	public TNode(double x1, double y1, double x2, double y2, int level, int nodeType){
		super(x1, y1, x2, y2, level, nodeType);
	}

	@Override
	public LinkedHashSet<MyPoint> calPoint(LinkedHashSet<String> keywords){
		LinkedHashSet<MyPoint> returnPoints = null;
		for (Cell cell : cellMembers) {
//			if (cell != null && cell.getListOfPoints() != null) {
//				for (MyPoint p : cell.getListOfPoints()) {
//					if(p.containCertainKeywords(keywords)){
//						if(returnPoints==null)
//							returnPoints = new LinkedHashSet<MyPoint>();
//						returnPoints.add(p);
//					}
//				} 
//			}
			if(cell.getKeyword_pMap()!=null){
				for(String keyword: keywords){
					if(returnPoints==null)
						returnPoints = new LinkedHashSet<MyPoint>();
					if(cell.getKeyword_pMap() !=null && cell.getKeyword_pMap().get(keyword)!=null)
						returnPoints.addAll(cell.getKeyword_pMap().get(keyword));
				}
			}
		}
		return returnPoints;
	}
	
	public boolean hasPoints(){
		for (Cell cell : cellMembers) {
			if(cell.getKeyword_pMap()!=null){
				return true;
			}
		}
		return false;
	}
	
	/*@Override
	public HashMap<String, Integer> createKeyword_pCntMap() {
		for (Node child : children) {
			HashMap<String, Integer> childHashmap = child.createKeyword_pCntMap();
			if(childHashmap!=null && childHashmap.size()!=0){ 
				if(keyword_pCntMap==null)
					keyword_pCntMap	= new HashMap<String, Integer>(); 
				childHashmap.forEach((k, v) -> keyword_pCntMap.merge(k, v, Integer::sum));
			}
		}
		return keyword_pCntMap;
	}*/
//	
//	@Override
//	public HashMap<String, ArrayList<MyPoint>> createKeyword_pMap() {
//		for (Node child : children) {
//			HashMap<String, ArrayList<MyPoint>> childHashmap = child.createKeyword_pMap();
//			if(childHashmap!=null && childHashmap.size()!=0){ 
//				if(keyword_pMap==null)
//					keyword_pMap = new HashMap<String, ArrayList<MyPoint>>();
//				Iterator<Entry<String, ArrayList<MyPoint>>> it = childHashmap.entrySet().iterator();
//				while(it.hasNext()){
//					Entry<String, ArrayList<MyPoint>> entry= it.next();
//					String keyword = entry.getKey();
//					ArrayList<MyPoint> value = entry.getValue(); 
//					if(keyword_pMap.get(keyword)==null){
//						ArrayList<MyPoint> points = new ArrayList<MyPoint>();
//						points.addAll(value);
//						keyword_pMap.put(keyword, points);
//					}else{
//						ArrayList<MyPoint> oldList = keyword_pMap.get(keyword);
//						oldList.addAll(value);
//						keyword_pMap.remove(keyword);
//						keyword_pMap.put(keyword, oldList);
//					}
//				}
//			}
//		}
//		return keyword_pMap;
//	}

	public void setChildren(ArrayList<? extends Node> children2) {
		this.children = children2;
	}

	public void setCellMembers(ArrayList<Cell> cellMembers) {
		this.cellMembers = cellMembers;
	}
	
	public ArrayList<? extends Node> getChildren() {
		return children;
	}

	public ArrayList<Cell> getCellMembers() {
		return cellMembers;
	}

	public NodeMinMaxBoundsOld getNodeMinMaxBoundsOld() {
		if(nodeMinMaxBoundsOld==null)
			this.nodeMinMaxBoundsOld = new NodeMinMaxBoundsOld(this);
		return nodeMinMaxBoundsOld;
	}
	
	public NodeMinMaxBoundsNew getNodeMinMaxBoundsNew() {
		if(nodeMinMaxBoundsNew == null)
			this.nodeMinMaxBoundsNew = new NodeMinMaxBoundsNew(this);
		return nodeMinMaxBoundsNew;
	}
	
	public HashMap<String, HashMap<Integer, MinMaxPair>> getNodeMinMaxOld(){
		if(nodeMinMaxBoundsOld == null)
			return null;
		return nodeMinMaxBoundsOld.getNodeMinMaxBoundMap();
	}
	
	public LinkedHashMap<String, MinMaxPair> getNodeMinMaxNew(){
		if(nodeMinMaxBoundsNew==null)
			return null;
		return nodeMinMaxBoundsNew.getNodeMinMax();
	}

	@Override
	public String toString() {
		return "TNode is at level: " + level + " index: [" + iThInGrid + ", " + jThInGrid + "] |  ( " + x1 + " , " + y1 + " , " + x2 + " , " + y2 + ")";
	}
}
