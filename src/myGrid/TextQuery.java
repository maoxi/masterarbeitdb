package myGrid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

public class TextQuery {
	private LinkedHashSet<String> queryKeywords;
	private double queryWidth;
	private double queryHeight;
	private int fittingLevel;

	private LinkedHashSet<Node> candidateNodeList; //results of pruning

	public TextQuery() {
		this.candidateNodeList = new LinkedHashSet<Node>();
		this.queryWidth = 0.02;
		this.queryHeight = 0.02;
	}

	public double getQueryHeight() {
		return queryHeight;
	}
	public double getQueryWidth() {
		return queryWidth;
	}
	
	public void setQueryHeight(double queryHeight) {
		this.queryHeight = queryHeight;
	}
	public void setQueryWidth(double queryWidth) {
		this.queryWidth = queryWidth;
	}
	
	public void setKeywords(LinkedHashSet<String> keywords) {
		this.queryKeywords = keywords;
	}
	
	public LinkedHashSet<Node> getCandidateNodeList() {
		return candidateNodeList;
	}
	
	public LinkedHashSet<String> getKeywords() {
		return queryKeywords;
	}
	
	public int getFittingLevel() {
		return fittingLevel;
	}
	
	// query the indexStructure of grid and return potential Nodes
	public void query() {
		candidateNodeList = new LinkedHashSet<Node>();
		Grid grid = Grid.getInstance();

		// calculate the fitting level
		if (queryWidth < grid.getxCellWidth() || queryHeight < grid.getyCellWidth()) { 
			// if the lowest Level width still bigger als query width
			System.out.println("Not enough Granularity in grid, please set your Query Width and Height");
			throw new RuntimeException("Not enough Granularity in grid");
		}
		// Math.pow(grid.getMAX_X()-grid.getMIN_X(), 1/Math.pow(grid.getTreeFan(), level))>= this.width
		//level starts from 1 instead of 0, that's why +1
		int fittingLevelX = (int) Math.floor(Math.log((grid.getMAX_X() - grid.getMIN_X()) / queryWidth) / Math.log(grid.getTreeFan())) +1; 
		int fittingLevelY = (int) Math.floor(Math.log((grid.getMAX_Y() - grid.getMIN_Y()) / queryHeight) / Math.log(grid.getTreeFan())) +1;
		fittingLevel = Math.min(fittingLevelX, fittingLevelY);

		if(Main.useCandidates){
			if(Main.pruneRedundantCandidate){
				for (final String keyword : queryKeywords) {
					//search on fittinglevel for candidates
					LinkedHashSet<Node> keywordCandidates = searchCandidateInThisAndLowerLevel(keyword);
					if(keywordCandidates!=null)
						this.candidateNodeList.addAll(keywordCandidates);
				}
			}else{
				for (final String keyword : queryKeywords) {
					LinkedHashSet<Node> keywordCandidates = Grid.getInstance().getLevelCandidates().get(fittingLevel).get(keyword);
					if(keywordCandidates!=null)
						this.candidateNodeList.addAll(keywordCandidates);
				}
			}	
		}else{
			for (final String keyword : queryKeywords) {
				pruneEachKeyword(keyword);
			}
		}
	}
	
	private LinkedHashSet<Node> searchCandidateInThisAndLowerLevel(String keyword){
		//candidates current level
		LinkedHashSet<Node> currentLevelCandidates = Grid.getInstance().getLevelCandidates().get(fittingLevel).get(keyword);
		int bestMin = ((TNode) (currentLevelCandidates.toArray()[0])).getNodeMinMaxNew().get(keyword).getMin();
		//candidates lower level
		for(int level= fittingLevel+1; level<= Main.treeHeight-1; level++){
			//the minMax marked with "top" must be in the level candidates, as its parent is already a candidate(same minMax)
			LinkedHashSet<Node> levelCandidates = Grid.getInstance().getLevelCandidates().get(level).get(keyword);
			//TODO compare the best upperbound in the sorted levelCandidates 
			//with the best lowerbound in current level (avoid searching through the whole level down to the bottom)
			//could be possible that the fitting level has no candidate at all=> no best lowerbound
			if(levelCandidates!=null){
//				SortFunctions.sortSetByNodeMin(levelCandidates, keyword);  TODO should be already sorted
				for(Node node:levelCandidates){
					MinMaxPair pair = ((TNode)node).getNodeMinMaxNew().get(keyword);
					if(pair.getMax()<bestMin){
						break;
					}
					if(pair.getRedudantLevel().contains(level)){
						currentLevelCandidates.add(node);
					}
				}
			}
		}
		return currentLevelCandidates;
	}
	
	private void pruneEachKeyword(String keyword){
		Map<Integer, Node[][]> tree = Grid.getInstance().getTree();
		HashMap<Integer, MinMaxPair> minMaxBound= ((TNode) tree.get(1)[0][0]).getNodeMinMaxOld().get(keyword);
		if(minMaxBound==null) //whole data does not contain this keyword
			return;
		
		// calculate the best lowerBound
		int lowerBound = minMaxBound.get(fittingLevel).getMin();
		if(lowerBound == 0)
			return;
		System.out.println("keyword: "+keyword+", fittingLevel= "+fittingLevel+", Lowerbound used for pruning is: "+lowerBound);
		
		// lowest level with cells, TODO 考虑下能�?�和prune method�?�并
		if (fittingLevel == tree.size()) {   //TODO delete this part and restrict the query onto the levels above Cell level
			Node[][] cells = tree.get(fittingLevel);
			for (int i = 0; i < cells.length; i++) {
				for (int j = 0; j < cells[cells.length - 1].length; j++) {
					Cell currentCell = (Cell) cells[i][j];
					if (currentCell.getKeyword_pMap()!=null 
							&& currentCell.getKeyword_pMap().get(keyword)!=null 
							&& currentCell.getKeyword_pMap().get(keyword).size()> lowerBound) {
						candidateNodeList.add(currentCell);
					}
				}
			}
		}else{
			prune(tree.get(1)[0][0], lowerBound, keyword);
		}
	}
	
	private void prune(Node node, int lowerBound, String keyword) {
		int currentlevel = node.getLevel();
		TNode currentNode = (TNode) node;
		if(currentNode.getNodeMinMaxOld()==null || currentNode.getNodeMinMaxOld().get(keyword)==null)
			return;
		if (lowerBound <= currentNode.getNodeMinMaxOld().get(keyword).get(fittingLevel).getMax()) {
			if (currentlevel == fittingLevel) {
				candidateNodeList.add(currentNode);
			} else {
				ArrayList<? extends Node> children = currentNode.getChildren();
				for (Node child : children) {
					prune(child, lowerBound, keyword);
				}
			}
		}
	}
}
