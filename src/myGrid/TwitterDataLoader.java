package myGrid;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import utilities.SortFunctions;

public class TwitterDataLoader {
	private static TwitterDataLoader instance;
	private HashSet<MyPoint> datasetAsPoints;
	private LinkedHashMap<String, Integer> topHashTagsMap;
	private LinkedHashSet<String> topHashTagsList;
	public DataType dataType = DataType.ORIGINAL;

	public static TwitterDataLoader getInstance() {
		if (instance != null)
			return instance;
		else{
			init();
			return null;
		}
	}
	
	public static void init() {
		instance = new TwitterDataLoader();
	}
	
	public void emptyPoints(){
		datasetAsPoints = null;
	}

	private TwitterDataLoader() {
		datasetAsPoints = new HashSet<MyPoint>();
		topHashTagsMap = new LinkedHashMap<>();
		topHashTagsList = new LinkedHashSet<>();
	}

	public static enum DataType{
		ORIGINAL, CLUMPED, RANDOM, UNIFORM
	}
	
	/**
	 * twitter Data Keywords extraction
	 * 
	 * @param file
	 * @return 1 if process ran without any errors
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public void loadFilteredFile(File keywordfile) throws NumberFormatException, IOException {
		System.out.println("Loading keywords from filtered twitter file " + keywordfile);
		datasetAsPoints = new HashSet<MyPoint>();  //TODO 
		topHashTagsMap.clear();
		topHashTagsList.clear();
		
		FileInputStream fstream1 = null;
		try {
			fstream1 = new FileInputStream(keywordfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		DataInputStream in1 = new DataInputStream(fstream1);
		LineNumberReader lr1 = new LineNumberReader(new InputStreamReader(in1));

		String strLine1 = null;		while ((strLine1 = lr1.readLine()) != null) {
			LinkedHashSet<String> unsortedKeywords = new LinkedHashSet<String>();
			String[] f = strLine1.split("\\s+");
			long id = Long.parseLong(f[0]);
			
			double x = Double.parseDouble(f[1]);
			double y = Double.parseDouble(f[2]);
			
			for (int j = 3; j < f.length; j++) { // the rest are keywords
				unsortedKeywords.add(f[j]);
				unsortedKeywords.add(Main.NOKEYWORD);
			}
			MyPoint p = new MyPoint(x, y, id, unsortedKeywords);
			datasetAsPoints.add(p);
		}
		lr1.close();
		distributeData();
	}	
	
	private void distributeData(){
		Iterator<MyPoint> pointsIt;
		int pCnt = datasetAsPoints.size();
			switch (dataType) {
				case UNIFORM:
					pointsIt = datasetAsPoints.iterator();
					int i = 1;
					while(pointsIt.hasNext()){
						MyPoint p = pointsIt.next();
						int pCntPerLine = (int) Math.ceil(Math.sqrt(pCnt));
						double x = i % pCntPerLine * (1.0/pCntPerLine)+Math.random()*(1.0/pCntPerLine);
						double y = i/pCntPerLine *(1.0/pCntPerLine)+Math.random()*(1.0/pCntPerLine);
						p.setP_x(x);
						p.setP_y(y);
						i++;
					}
					break;
				case RANDOM:
					pointsIt = datasetAsPoints.iterator();
					while(pointsIt.hasNext()){
						MyPoint p = pointsIt.next();
						p.setP_x(Math.random());
						p.setP_y(Math.random());
					}
					break;
				case CLUMPED:
					pointsIt = datasetAsPoints.iterator();
					while(pointsIt.hasNext()){
						MyPoint p = pointsIt.next();
						p.setP_x(Math.random());
						p.setP_y(Math.random());
					}
					int cntCluster = 10; //number of cluster
					HashSet<MyPoint> newPoints = new HashSet<MyPoint>();
					double radius = Math.sqrt(1.0/cntCluster);
					for(int j=0; j< cntCluster; j++){  // do this 50 times
						double randomX = Math.random();
						double randomY = Math.random();
						LinkedHashSet<MyPoint> sampleSet = new LinkedHashSet<MyPoint>();

						pointsIt = datasetAsPoints.iterator();
						while(pointsIt.hasNext()){  //iterate and take points(within random area) out
							MyPoint p = pointsIt.next();
							if(p.getX()> randomX-radius && p.getX()< randomX+radius
									&& p.getY()> randomY-radius && p.getY()< randomY+radius){
								sampleSet.add(p);
								pointsIt.remove();
							}
						}
						if(sampleSet.isEmpty())
							continue;
						
						double[] x = new double[sampleSet.size()];
						double[] y = new double[sampleSet.size()];
						int index=0;
						for(MyPoint point: sampleSet){
							x[index] = point.getX();
							y[index] = point.getY();
							index++;
						}
						double[] sortedX = x.clone();
						double[] sortedY = y.clone(); 
						Arrays.sort(sortedX);
						Arrays.sort(sortedY);
						double minX = sortedX[0];
						double maxX = sortedX[sortedX.length-1];
						double minY = sortedY[0];
						double maxY = sortedY[sortedY.length-1];
						double stdDeviationX = (maxX-minX)/(2.0*10.0);
						double stdDeviationY = (maxY-minY)/(2.0*10.0);
						
						Random randomXDistr = new Random();
						Random randomYDistr = new Random();
						
						index=0;
						Iterator<MyPoint> iterP = sampleSet.iterator();
						while(iterP.hasNext()){
							MyPoint point = iterP.next();
							
							//http://stackoverflow.com/questions/2751938/random-number-within-a-range-based-on-a-normal-distribution
							double gausX = randomXDistr.nextGaussian()*stdDeviationX+(minX+maxX)/2.0;
							double gausY = randomYDistr.nextGaussian()*stdDeviationY+(minY+maxY)/2.0;
							point.setP_x(gausX);
							point.setP_y(gausY);
							index++;
						}
						newPoints.addAll(sampleSet);
					}
					datasetAsPoints = newPoints;
					break;
				default:
					break;
			}
	}
	
	public void calKeywordsFrequencyUsingDatasetPoints() {
		if (datasetAsPoints != null) {
			for (MyPoint myPoint : datasetAsPoints) {
				LinkedHashSet<String> keywords = myPoint.getKeywords();
				for (String keyword : keywords) {
					if (topHashTagsMap.containsKey(keyword)) {
						topHashTagsMap.put(keyword, topHashTagsMap.get(keyword) + 1);
					} else {
						topHashTagsMap.put(keyword, 1);
					}
				}
			}
			topHashTagsMap.put(Main.NOKEYWORD, datasetAsPoints.size());
			topHashTagsMap = sortByValue(topHashTagsMap);
			setTopHashTagsList(topHashTagsMap);
		}
	}

	// using a pre-sorted list of words with frequency
	public void calKeywordsFrequencyUsingFile(String keywordfile) throws NumberFormatException, IOException {
		System.out.println("Loading top Hashtags from twitter file " + keywordfile);

		FileInputStream fstream1 = null;
		try {
			fstream1 = new FileInputStream(keywordfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		DataInputStream in1 = new DataInputStream(fstream1);
		BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));

		String strLine1 = null;
		while ((strLine1 = br1.readLine()) != null) {
			String[] f = strLine1.split("\\s+");
			String tag = f[0];
			int frequency = Integer.parseInt(f[1]);
			topHashTagsMap.put(tag, frequency);
		}
		br1.close();
		topHashTagsMap.put(Main.NOKEYWORD, datasetAsPoints.size());//TODO should be bigger than the biggest one
		topHashTagsMap = sortByValue(topHashTagsMap);
		setTopHashTagsList(topHashTagsMap);
	}

	public void filterNotTopHashTags(){
		Iterator<MyPoint> pIter = datasetAsPoints.iterator();
		while(pIter.hasNext()){
			MyPoint p = pIter.next();
			LinkedHashSet<String> filteredKeywords = new LinkedHashSet<String>();
			for(String keyword :p.getKeywords()){
				if(topHashTagsList.contains(keyword))
					filteredKeywords.add(keyword);
			}
			if(filteredKeywords.size()==1){  // only NOKEYWORD inside
				pIter.remove();
			}else{
				p.setKeywords(filteredKeywords);
			}
		}
	}

	// keywords in points are also sorted according to the frequency of the keyword
	public void sortKeywordsInPoint() {
		Iterator<MyPoint> pIter = datasetAsPoints.iterator();
		while(pIter.hasNext()){
			MyPoint p = pIter.next();
			p.setKeywords(SortFunctions.sortSetByKeywordFrequency(p.getKeywords()));
		}
	}
	
	public void setTopHashTagsList(LinkedHashMap<String, Integer> map) {
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			this.topHashTagsList.add(entry.getKey());
		}
	}

	public HashSet<MyPoint> getDatasetAsPoints() {
		return datasetAsPoints;
	}

	public HashSet<String> getTopHashTags() {
		return topHashTagsList;
	}

	public HashMap<String, Integer> getTopHashTagsMap() {
		return topHashTagsMap;
	}
	
	public static <K, V extends Comparable<? super V>> LinkedHashMap<K, V> sortByValue(Map<K, V> map){
		List<Map.Entry<K, V>> list =new LinkedList<Map.Entry<K, V>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>(){
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2){
				return -1*((o1.getValue()).compareTo(o2.getValue()));
			}
		});

		LinkedHashMap<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list){
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}