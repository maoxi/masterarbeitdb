package mySweepline;

import java.util.Comparator;

public class Interval1D implements Comparable<Interval1D> {
	private double low; // left endpoint
	private double high; // right endpoint
	private int count = 1; // point weight
	private double height = -1; // 这个interval 所在的高度
	private boolean isTop = false;

	public Interval1D() {
		// TODO Auto-generated constructor stub
	}

	public Interval1D(double low, double high) {
		if (low <= high) {
			this.low = low;
			this.high = high;
		}

		else
			throw new RuntimeException("Illegal interval");
	}

	public Interval1D(Interval1D other) {
		this.low = other.low;
		this.high = other.high;
		this.count = other.count;
		this.height = other.height;
	}

	public Interval1D(double low, double high, double height, int count) {
		if (low <= high) {
			this.low = low;
			this.high = high;
			this.count = count;
			this.height = height;
		}

		else
			throw new RuntimeException("Illegal interval");
	}

	public Interval1D(double low, double high, double height) {
		if (low <= high) {
			this.low = low;
			this.high = high;
			this.height = height;
		}

		else
			throw new RuntimeException("Illegal interval");
	}

	// does this interval intersect that one?
	public boolean intersects(Interval1D that) {
		if (Double.compare(that.high, this.low) < 0)
			return false;
		if (Double.compare(this.high, that.low) < 0)
			return false;
		return true;
	}

	public boolean contains(double x) {
		if (Double.compare(low, x) <= 0 && Double.compare(x, high) <= 0)
			return true;
		return false;
	}

	public int compareTo(Interval1D that) {
		if (Double.compare(this.low, that.low) < 0)
			return -1;
		else if (Double.compare(this.low, that.low) > 0)
			return +1;
		else if (Double.compare(this.high, that.high) < 0)
			return -1;
		else if (Double.compare(this.high, that.high) > 0)
			return +1;
		else
			return 0;
	}

	public static class heightComparator implements Comparator<Interval1D> {

		public int compare(Interval1D arg0, Interval1D arg1) {
			if (Double.compare(arg0.height, arg1.height) < 0)
				return -1;
			else if (Double.compare(arg0.height, arg1.height) > 0)
				return +1;
			else
				return 0;
		}

	}

	@Override
	public String toString() {
		if (height == -1)
			return "[" + low + ", " + high + "]";
		else
			return "[" + low + ", " + high + ", " + height + ", " + count + "]";
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public boolean isTop() {
		return isTop;
	}

	public void setTop(boolean isTop) {
		this.isTop = isTop;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLength() {
		return high - low;
	}

	Interval1D copyInterval() {
		Interval1D copiedInterval = new Interval1D();
		copiedInterval.setLow(this.getLow());
		copiedInterval.setHigh(this.getHigh());
		copiedInterval.setCount(this.getCount());
		copiedInterval.setHeight(this.getHeight());
		copiedInterval.setTop(this.isTop());
		return copiedInterval;
	}

	@Override
	public boolean equals(Object obj) {
		return !super.equals(obj);
	}

	@Override
	public int hashCode() {
		return (""+low+high+height).hashCode();
	}
}