package mySweepline;

public class Interval2D {
	public final Interval1D intervalHorizontal;
	public final Interval1D intervalVertical;

	public Interval2D(Interval1D intervalHorizontal, Interval1D intervalVertical) {
		this.intervalHorizontal = intervalHorizontal;
		this.intervalVertical = intervalVertical;
	}

	public boolean intersects(Interval2D b) {
		if (!intervalHorizontal.intersects(b.intervalHorizontal))
			return false;
		if (!intervalVertical.intersects(b.intervalVertical))
			return false;
		return true;
	}

	public boolean contains(double d, double e) {
		return intervalHorizontal.contains(d) && intervalVertical.contains(e);
	}

	@Override
	public String toString() {
		return intervalHorizontal + " x " + intervalVertical;
	}
}
