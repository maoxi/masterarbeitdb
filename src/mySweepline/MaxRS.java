package mySweepline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import myGrid.Main;
import myGrid.MinMaxPair;
import myGrid.MinPQ;
import myGrid.MyPoint;
import myGrid.Node;
import myGrid.TNode;
import utilities.SortFunctions;
import utilities.Utilities;

public class MaxRS {
	public static Interval1D maxWindow;
	public static HashSet<Interval1D> maxWindow_eachCandidate;
	public static HashSet<MyPoint> resultPoints;

	public static class Event implements Comparable<Event> {
		double time; // y轴值
		Interval2D rect; // 注�?�?是水平的interval，而是整个rect.
		boolean isTop;

		public Event(double time, Interval2D rect, boolean isTop) {
			this.time = time;
			this.rect = rect;
			this.isTop = isTop;
		}

		public Interval2D getRect() {
			return rect;
		}

		public double getTime() {
			return time;
		}

		public int compareTo(Event that) {
			if (this.time < that.time)
				return -1;
			else if (this.time > that.time)
				return +1;
			else
				return 0;
		}

		public static class heightComparator implements Comparator<Event> {

			public int compare(Event arg0, Event arg1) {
				if (arg0.time > arg1.time) // arg0.time greater than arg1.time
					return 1;
				else if (arg0.time < arg1.time)
					return -1;
				// if y is the same, use intervalX to decide
				else if (arg0.rect.intervalHorizontal.getLow() < arg1.rect.intervalHorizontal.getLow()
						|| arg0.rect.intervalHorizontal.getLow() > arg1.rect.intervalHorizontal.getLow())
					return -1;
				else
					return 0;
			}
		}
	}

	public static void prepareRunningSweepline() {
		LinkedHashSet<Node> candidates = Main.myQuery.getCandidateNodeList();
		maxWindow = new Interval1D(-999, 999);
		maxWindow_eachCandidate = new HashSet<Interval1D>();
		resultPoints = new HashSet<MyPoint>();
		candidates = SortFunctions.sortSetByNodeMinMaxSum(candidates);

		long startTime = System.currentTimeMillis();
		for (Node node : candidates) {
			if (Main.useSort) {
				int sumMaxKeyword = 0;
				for (String queryKeyword : Main.myQuery.getKeywords()) {
					MinMaxPair minMaxPair = null;
					if (Main.useCandidates)
						minMaxPair = ((TNode) node).getNodeMinMaxNew().get(queryKeyword);
					else if (Main.useCandidates == false && ((TNode) node).getNodeMinMaxOld().get(queryKeyword) != null) {
						minMaxPair = ((TNode) node).getNodeMinMaxOld().get(queryKeyword).get(node.getLevel());
					}
					if (minMaxPair != null)
						sumMaxKeyword += minMaxPair.getMax();
				}
				if (maxWindow.getCount() > sumMaxKeyword)
					continue;
			}

			ArrayList<MyPoint> candidatePoints;
			ArrayList<MyPoint> allPoints;
			ArrayList<Node> nodes;
			double[] borders;
			if (Main.useAccurateMAX) {
				candidatePoints = node.calPointsSWPLNeighbourS(true);
				allPoints = node.calPointsSWPLNeighbourS(false);
				nodes = node.calNodesSWPLNeighbourS();
				borders = node.calBordersSWPLNeighbourS();
			} else {
				candidatePoints = node.calPointsSWPLNeighbourL(true);
				allPoints = node.calPointsSWPLNeighbourL(false);
				nodes = node.calNodesSWPLNeighbourL();
				borders = node.calBordersSWPLNeighbourL();
			}
			if (Main.useGUI) {
				Main.gridFrame.getGridGraphic().setPointsInProcessing(candidatePoints);
				Main.gridFrame.getGridGraphic().setNodesInProcessing(nodes);
				Main.sweeplineFrame.getSweeplineGraphic().clearGraph();
				Main.sweeplineFrame.getSweeplineGraphic().setBorders(borders);
				Main.sweeplineFrame.getSweeplineGraphic().setAllPoints(allPoints);
				Main.sweeplineFrame.getSweeplineGraphic().setPointsInProcessing(candidatePoints);
			}

			Interval2D[] rects = new Interval2D[candidatePoints.size()];
			// sorted event to save all y-intervals
			MinPQ<Event> pqEvent = new MinPQ<Event>(new Event.heightComparator());

			// disambiguate x coordinates in case of same x_coord.
			Set<Double> x_coordsSet = new HashSet<Double>();
			x_coordsSet.clear();
			x_coordsSet.add(borders[0]);
			x_coordsSet.add(borders[1]);
			for (int i = 0; i < candidatePoints.size(); i++) {
				MyPoint p = candidatePoints.get(i);
				double x_cor = p.getX();
				double x_cor_w = p.getX() + Main.myQuery.getQueryWidth();
				// double x_cor = Utilities.truncateDecimal(p.getX(), 10).doubleValue(); TODO
				// double x_cor_w = Utilities.truncateDecimal(p.getX() + width, 10).doubleValue();
				while (true) {
					if (x_coordsSet.contains(x_cor) || x_coordsSet.contains(x_cor_w)) {
						Random randomGenerator = new Random();
						int randomInt = randomGenerator.nextInt(10000);
						double incr = 0.00000001 * randomInt;
						x_cor = Utilities.truncateDecimal(p.getX() + incr, 10).doubleValue();
						x_cor_w = Utilities.truncateDecimal(x_cor + Main.myQuery.getQueryWidth(), 10).doubleValue();

					} else {
						x_coordsSet.add(x_cor);
						x_coordsSet.add(x_cor_w);
						break;
					}
				}
				rects[i] = new Interval2D(new Interval1D(x_cor, x_cor_w), new Interval1D(p.getY(), p.getY() + Main.myQuery.getQueryHeight()));

				Event e1 = new Event(rects[i].intervalVertical.getLow(), rects[i], true);
				Event e2 = new Event(rects[i].intervalVertical.getHigh(), rects[i], false);
				pqEvent.insert(e1);
				pqEvent.insert(e2);
			}
			runSweepline(x_coordsSet, pqEvent, borders);
		}
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Time for executing sweepline: " + elapsedTime + " ms");
		if (Main.useGUI)
			Main.gridFrame.getGridGraphic().setRectResult(maxWindow);
	}

	private static void runSweepline(Set<Double> x_coordsSet, MinPQ<Event> pqEvent, double[] borders) {
		Double[] x_coordsArray = x_coordsSet.toArray(new Double[x_coordsSet.size()]);
		Arrays.sort(x_coordsArray);
		SweeplineTree sweepLineTree = new SweeplineTree(x_coordsArray); // create the tree
		if (Main.useGUI)
			Main.sweeplineFrame.getSweeplineGraphic().setTree(sweepLineTree);
		System.out.println("Priority Queue contains " + pqEvent.size() + " events.");

		Interval1D top_floor = new Interval1D(borders[0], borders[1], borders[2]);
		sweepLineTree.treeNodes[1].window = top_floor; // �?始化最大的window

		if (Main.useGUI)
			Main.sweeplineFrame.getSweeplineGraphic().setEventsRectangle(pqEvent);
		while (!pqEvent.isEmpty()) {
			Event e = pqEvent.delMin();
			Interval1D rectIntervalH = e.rect.intervalHorizontal;
			Interval1D rectIntervalV = e.rect.intervalVertical;
			Interval1D event_intervalH;
			if (e.isTop) {
				event_intervalH = new Interval1D(rectIntervalH.getLow(), rectIntervalH.getHigh(), rectIntervalV.getLow());
			} else {
				event_intervalH = new Interval1D(rectIntervalH.getLow(), rectIntervalH.getHigh(), rectIntervalV.getHigh());
			}
			sweepLineTree.createF_IN(event_intervalH);
			sweepLineTree.createF_L(event_intervalH.getLow());
			sweepLineTree.createF_R(event_intervalH.getHigh());
			if (Main.useGUI) {
				Main.sweeplineFrame.getSweeplineGraphic().setF_IN(sweepLineTree.getF_IN());
				Main.sweeplineFrame.getSweeplineGraphic().setF_L(sweepLineTree.getF_L());
				Main.sweeplineFrame.getSweeplineGraphic().setF_R(sweepLineTree.getF_R());
				Main.sweeplineFrame.getSweeplineGraphic().setEvent(e);
			}

			// the bigger value is the bottom of rectangle
			if (e.isTop) {
				sweepLineTree.processF_IN(event_intervalH);
				sweepLineTree.processF_LR(event_intervalH, true);
				sweepLineTree.processF_LR(event_intervalH, false);
			} else {
				sweepLineTree.processRectBottom(event_intervalH);
			}
			sweepLineTree.processInsert();
			if (Main.useGUI)
				Main.sweeplineFrame.getSweeplineGraphic().setWindows(sweepLineTree);
			Main.delayShort();
		}

		if (sweepLineTree.getMaximum_interval() != null) {
			System.out.println("Maximum Range for this candidate: " + sweepLineTree.getMaximum_interval() + " with count: "
					+ (sweepLineTree.getMaximum_interval().getCount() - 1));

			// for graphics
			maxWindow_eachCandidate.add(sweepLineTree.getMaximum_interval());
			if (maxWindow.getCount() < sweepLineTree.getMaximum_interval().getCount()) {
				maxWindow = sweepLineTree.getMaximum_interval();
			}
			if (Main.useGUI) {
				Main.gridFrame.getGridGraphic().setCandidatesResults(maxWindow_eachCandidate);
				Main.sweeplineFrame.getSweeplineGraphic().setThisMaxWindow(sweepLineTree.getMaximum_interval());
			}
		}
		Main.delayLong();
	}
}
