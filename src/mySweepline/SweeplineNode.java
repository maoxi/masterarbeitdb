package mySweepline;

import myGrid.MinPQ;

public class SweeplineNode extends GraphCoordPoint{
	protected MinPQ<Interval1D> pq; // key

	protected int idxV; // associated node number
	protected boolean isLeaf;
	protected double discriminant; // �?�设有4个�?节点的�?，则discriminant为 2和3节点值的平�?�值, 用于划分是属于左边还是�?�边
	protected int sizeOfSubTree = 1; // size of subtree rooted at this node
	protected int from; // �?树是节点: 从。。。到。。 �?个节点中都存储了�?树的范围
	protected int to;

	protected Interval1D window; // only one window can be associated to an internal node

	protected SweeplineNode parent;
	protected SweeplineNode left, right;

	public SweeplineNode() {
	}
	
	@Override
	public String toString() {
		if(window!=null)
			return "window: ["+window.getLow()+","+window.getHigh()+"], from "+ from + " to "+to+" discriminant "+discriminant;
		else
			return "from "+ from + " to "+to+" discriminant "+discriminant;
	}
	
	public Interval1D getWindow() {
		return window;
	}
	
	public SweeplineNode getLeft() {
		return left;
	}
	
	public SweeplineNode getRight() {
		return right;
	}
	
	public double getDiscriminant() {
		return discriminant;
	}
}
