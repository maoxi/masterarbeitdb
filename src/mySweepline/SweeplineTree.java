package mySweepline;

import graphics.SweeplineGraphics;

import java.util.ArrayList;

public class SweeplineTree extends SweeplineGraphics {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Interval1D maximum_interval;

	public SweeplineNode[] treeNodes; // LBBT树 index从1开始
	private Double[] x_coordsArray; // 最�?得到的是一个array,由这个array造出LBBT树
	private ArrayList<SweeplineNode> F_IN = new ArrayList<SweeplineNode>(); // index starts from 0
	private ArrayList<SweeplineNode> F_L = new ArrayList<SweeplineNode>(); // include forknode
	private ArrayList<SweeplineNode> F_R = new ArrayList<SweeplineNode>();
	private SweeplineNode fork_node; // only included in F_L
	private SweeplineNode leaf_of_F_L;
	private SweeplineNode leaf_of_F_R;
	private ArrayList<NodeWindowPair> nodeWindowPairsToInsert = new ArrayList<NodeWindowPair>();

	class NodeWindowPair {
		SweeplineNode node;
		Interval1D window;

		public NodeWindowPair(SweeplineNode node, Interval1D interval) {
			this.node = node;
			this.window = interval;
		}

		@Override
		public String toString() {
			return "NodeWindowPair-> window: " + window + " below node n: " + node;
		}
	}

	/**
	 * Time-Complexity: O(n*log(n)) builds tree bottom up
	 * 
	 * @param x_coordsArray
	 *            : the Initialization array
	 */
	public SweeplineTree(Double[] x_coordsArray) {
//		maximum_interval = new Inteval1D(-999, 999);

		this.x_coordsArray = x_coordsArray;
		// The max size of this array is about 2 * 2 ^ log2(n) + 1
		// TODO should be 2^(ceil(log2(n)+1)-1) �?明�?笔记,�?�设�?层都填满
		int size = (int) (2 * Math.pow(2.0, Math.floor((Math.log(x_coordsArray.length) / Math.log(2.0)) + 1)));
		treeNodes = new SweeplineNode[size];
		buildTree(1, 0, x_coordsArray.length);
	}

	// Initialize the Nodes of the Segment tree
	private SweeplineNode buildTree(int idxV, int from, int size) {
		SweeplineNode newNode = new SweeplineNode();
		newNode.from = from;
		newNode.to = from + size - 1;
		newNode.idxV = idxV;
		treeNodes[idxV] = newNode;

		if (size == 1) {
			newNode.isLeaf = true;
			newNode.discriminant = x_coordsArray[from];
			newNode.sizeOfSubTree = 1;
			return newNode;
		} else {
			newNode.isLeaf = false;

			SweeplineNode leftNode = buildTree(2 * idxV, from, size / 2); // 左边
			SweeplineNode rightNode = buildTree(2 * idxV + 1, from + size / 2, size - size / 2); // 右边

			newNode.left = leftNode;
			newNode.right = rightNode;
			leftNode.parent = treeNodes[idxV];
			rightNode.parent = treeNodes[idxV];
			newNode.sizeOfSubTree = treeNodes[2 * idxV].sizeOfSubTree + treeNodes[2 * idxV + 1].sizeOfSubTree;
			newNode.discriminant = (x_coordsArray[from + (size / 2) - 1] + x_coordsArray[from + (size / 2)]) / 2;
			return newNode;
		}
	}

	public int getTotalNodesSize() {
		return treeNodes.length;
	}

	public ArrayList<SweeplineNode> createF_L(double L) {
		F_L.clear();
		int i = fork_node.idxV;
		while (!(treeNodes[i].isLeaf)) {
			F_L.add(treeNodes[i]);
			i = (Double.compare(L, treeNodes[i].discriminant) < 0) ? 2 * i : 2 * i + 1;
		}
		F_L.add(treeNodes[i]);

		leaf_of_F_L = treeNodes[i];

		if (Double.compare(leaf_of_F_L.discriminant, L) != 0) {
			throw new RuntimeException("Leaf node value mismatch L = " + L + " and leaf_of_F_L value =" + leaf_of_F_L.discriminant);
		}

		return F_L;
	}

	public ArrayList<SweeplineNode> createF_R(double R) {
		F_R.clear();
		int i = fork_node.idxV;
		// one more step so that fork_node is only included in F_L
		i = (Double.compare(R, treeNodes[i].discriminant) < 0) ? 2 * i : 2 * i + 1;
		while (!(treeNodes[i].isLeaf)) {
			F_R.add(treeNodes[i]);
			i = (Double.compare(R, treeNodes[i].discriminant) < 0) ? 2 * i : 2 * i + 1;
		}
		F_R.add(treeNodes[i]);

		leaf_of_F_R = treeNodes[i];

		if (Double.compare(leaf_of_F_R.discriminant, R) != 0) {
			throw new RuntimeException("Leaf node value mismatch L = " + R + " and leaf_of_F_R value =" + leaf_of_F_R.discriminant);
		}

		return F_R;
	}

	public ArrayList<SweeplineNode> createF_IN(Interval1D interval) {
		int i = 1;
		F_IN.clear();
		while (!(treeNodes[i].isLeaf)) {
			if (Double.compare(interval.getHigh(), treeNodes[i].discriminant) < 0) {// int<d
				F_IN.add(treeNodes[i]);
				i = 2 * i;
			} else if (Double.compare(interval.getLow(), treeNodes[i].discriminant) > 0) { // int>d
				F_IN.add(treeNodes[i]);
				i = 2 * i + 1;
			}
			else if (Double.compare(interval.getLow(), treeNodes[i].discriminant) < 0 && Double.compare(interval.getHigh(), treeNodes[i].discriminant) > 0) {
				// found node v*
				break;
			}
		}
		fork_node = treeNodes[i];
		return F_IN;
	}

	/*
	 * FORWARD PASS for inserting top of rectangle
	 */
	public void processF_IN(Interval1D interval) {
		for (SweeplineNode n : F_IN) {
			if (n.window != null) {
				divideWindow(n, interval);
			}
		}
	}

	public void processF_LR(Interval1D interval, boolean isOnF_L) {
		ArrayList<SweeplineNode> path = new ArrayList<SweeplineNode>();
		path = isOnF_L ? F_L : F_R;

		for (int i = 0; i < path.size() - 1; i++) { // fork_node included
			SweeplineNode n = path.get(i);
			SweeplineNode n_next = path.get(i + 1);
			
			//update in convex child of node with none empty window
			boolean convex = isOnF_L ? n_next.equals(n.left) : n_next.equals(n.right);
			if (convex && !n.equals(fork_node)) {
				SweeplineNode insideChild = isOnF_L ? n.right : n.left;
				propagateInnerWindowCnt(insideChild, 1, interval);
			}

			if (n.window != null) {
				divideWindow(n, interval);
			}
		}
	}

	private void propagateInnerWindowCnt(SweeplineNode node, int value, Interval1D interval) {
		if (node == null || node.isLeaf) // leaf does not store any window
			return;
		if (node.window != null) {
			node.window.setCount(node.window.getCount() + value);
			node.window.setHeight(interval.getHeight());
			updateMaxInterval(node.window);
		}
		propagateInnerWindowCnt(node.left, value, interval);
		propagateInnerWindowCnt(node.right, value, interval);
	}

	private void updateMaxInterval(Interval1D w) {
		if (maximum_interval == null) {
			maximum_interval = w.copyInterval();
		} else if (maximum_interval.getCount() < w.getCount()) {
			maximum_interval = w.copyInterval();
		}
	}

	private void divideWindow(SweeplineNode n, Interval1D interval) {
		// Interval_left Windowleft written as: iLwL. 6 cases are: iLiRwLwR, iLwLiRwR, iLwLwRlR,
		// wLiLiRwR, wLiLwRiR, wLwRiLiR
		double iL = interval.getLow();
		double iR = interval.getHigh();
		double wL = n.window.getLow();
		double wR = n.window.getHigh();

		// "[]" = interval, "()" window
		// case iLiRwLwR: []()
		if (iR < wL) {
			return; // doNothing
		}
		// case iLwLiRwR: [(]), case may occur in F_R, F_IN
		else if (iL < wL && wL < iR && iR < wR) {
			Interval1D window1 = new Interval1D(n.window.getLow(), interval.getHigh(), interval.getHeight());
			Interval1D window2 = new Interval1D(interval.getHigh(), n.window.getHigh(), interval.getHeight());
			window1.setCount(n.window.getCount() + interval.getCount());
			window2.setCount(n.window.getCount());
			updateMaxInterval(window1);

			n.window = null;
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window1));
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window2));
		}
		// case iLwLwRlR: [()], case may occur in F_L,F_R
		else if (iL < wL && wR < iR) {
			n.window.setCount(n.window.getCount() + interval.getCount());
			n.window.setHeight(interval.getHeight());
			updateMaxInterval(n.window);
		}
		// case wLiLiRwR: ([]), case may only occur in F_IN
		else if (wL < iL && iR < wR) {
			Interval1D window1 = new Interval1D(n.window.getLow(), interval.getLow(), interval.getHeight());
			Interval1D window2 = new Interval1D(interval.getLow(), interval.getHigh(), interval.getHeight());
			Interval1D window3 = new Interval1D(interval.getHigh(), n.window.getHigh(), interval.getHeight());
			window1.setCount(n.window.getCount());
			window2.setCount(n.window.getCount() + interval.getCount());
			updateMaxInterval(window2);
			window3.setCount(n.window.getCount());

			n.window = null;
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window1));
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window2));
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window3));
		}
		// case wLiLwRiR: ([)], case may occur in F_L, F_IN
		else if (wL < iL && iL < wR && wR < iR) {
			Interval1D window1 = new Interval1D(n.window.getLow(), interval.getLow(), interval.getHeight());
			Interval1D window2 = new Interval1D(interval.getLow(), n.window.getHigh(), interval.getHeight());
			window1.setCount(n.window.getCount());
			window2.setCount(n.window.getCount() + interval.getCount());
			updateMaxInterval(window2);

			n.window = null;
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window1));
			nodeWindowPairsToInsert.add(new NodeWindowPair(n, window2));
		}
		// case wLwRiLiR: ()[]
		else {
			return; // doNothing
		}
	}

	/**
	 * find nodes with end points in F_IN + F_L + F_R
	 */
	public void processRectBottom(Interval1D interval) {

		// LxR implies that left side of interval matches right side of a window
		// RxL implies right side of interval matches left side of window
		// and so on...
		SweeplineNode n_LxL = null, n_LxR = null, n_RxL = null, n_RxR = null;

		ArrayList<SweeplineNode> f_nodes = new ArrayList<SweeplineNode>();
		if(F_IN!=null)
			f_nodes.addAll(F_IN);
		if(F_L!=null)
			f_nodes.addAll(F_L);
		if(F_R!=null)
			f_nodes.addAll(F_R);
		for (SweeplineNode z : f_nodes) {
			if (z.window == null)
				continue;

			// if window is completely outside the interval
			if (Double.compare(z.window.getHigh(), interval.getLow()) < 0 || Double.compare(z.window.getLow(), interval.getHigh()) > 0) {
				continue;
			}

			// if window is completely within the interval, (...[...]...)
			if (Double.compare(z.window.getLow(), interval.getLow()) > 0 && Double.compare(z.window.getHigh(), interval.getHigh()) < 0) {
				z.window.setCount(z.window.getCount() - interval.getCount());
				z.window.setHeight(interval.getHeight());
				continue;
			}

			// if interval is right at the boundary
			boolean LxL = Double.compare(interval.getLow(), z.window.getLow()) == 0;
			boolean LxR = Double.compare(interval.getLow(), z.window.getHigh()) == 0;
			boolean RxL = Double.compare(interval.getHigh(), z.window.getLow()) == 0;
			boolean RxR = Double.compare(interval.getHigh(), z.window.getHigh()) == 0;

			if (LxL && RxR) { // window正好和interval�?�?�，该window跨越整个interval
				n_LxL = n_RxR = z;
			} else if (LxL) { // window and interval have the same left boundary
				n_LxL = z;
			} else if (LxR) { // window在interval左侧
				n_LxR = z;
			} else if (RxR) { // window and interval have the same right boundary
				n_RxR = z;
			} else if (RxL) { // window在 interval�?�侧
				n_RxL = z;
			}
		}

		if (n_LxL!=null && n_RxR!=null && n_LxL.equals(n_RxR)) {
			Interval1D window = new Interval1D(n_LxR.window.getLow(), n_RxL.window.getHigh(), Math.max(n_LxR.window.getHeight(), n_RxL.window.getHeight()));
			window.setCount(n_LxR.window.getCount()); // counts are the same in n_LXR and n_RXL
			nodeWindowPairsToInsert.add(new NodeWindowPair(treeNodes[1], window));
		} else {
			Interval1D window1 = new Interval1D(n_LxR.window.getLow(), n_LxL.window.getHigh(), Math.max(n_LxR.window.getHeight(), n_LxL.window.getHeight()));
			window1.setCount(n_LxR.window.getCount());
			Interval1D window2 = new Interval1D(n_RxR.window.getLow(), n_RxL.window.getHigh(), Math.max(n_RxR.window.getHeight(), n_RxL.window.getHeight()));
			window2.setCount(n_RxL.window.getCount());
			nodeWindowPairsToInsert.add(new NodeWindowPair(treeNodes[1], window1));
			nodeWindowPairsToInsert.add(new NodeWindowPair(treeNodes[1], window2));
		}
		n_LxR.window = null;
		n_LxL.window = null;
		n_RxR.window = null;
		n_RxL.window = null;

		processInnernodes(interval);
	}

	private void processInnernodes(Interval1D interval) {
		for (int i = 1; i < F_L.size(); i++) {
			SweeplineNode n = F_L.get(i);
			if ((i + 1) < F_L.size()) {
				SweeplineNode n_next = F_L.get(i + 1);
				if (n_next.equals(n.left)) {
					propagateInnerWindowCnt(n.right, -1, interval);
				}
			}
		}

		// because F_R does not begin from fork_node => check the leftChild of F_R[0]
		for (int i = 0; i < F_R.size(); i++) {
			SweeplineNode n = F_R.get(i);
			if ((i + 1) < F_R.size()) {
				SweeplineNode n_next = F_R.get(i + 1);
				if (n_next.equals(n.right)) {
					propagateInnerWindowCnt(n.left, -1, interval);
				}
			}
		}
	}

	// assign to nodes: interval.Left< nodes.discriminant <interval.right
	public void processInsert() {
		for (NodeWindowPair nip : nodeWindowPairsToInsert) {
			SweeplineNode node = nip.node;
			Interval1D windowToInsert = nip.window;
			while (!(node.isLeaf)) {
				if (Double.compare(windowToInsert.getHigh(), node.discriminant) <= 0) {
					node = node.left;
				} else if (Double.compare(windowToInsert.getLow(), node.discriminant) >= 0) {
					node = node.right;
				} else if (Double.compare(windowToInsert.getLow(), node.discriminant) < 0 && Double.compare(windowToInsert.getHigh(), node.discriminant) > 0) {
					if (node.window != null) {// should not happen
						throw new RuntimeException("Overwriting window with " + windowToInsert + " at node: " + node.idxV);
					}
					// attach window to node and break;
					node.window = windowToInsert;
					break;
				}
			}
		}
		nodeWindowPairsToInsert.clear();
	}

	public Interval1D getMaximum_interval() {
		return maximum_interval;
	}

	public ArrayList<SweeplineNode> getF_IN() {
		return F_IN;
	}

	public ArrayList<SweeplineNode> getF_L() {
		return F_L;
	}

	public ArrayList<SweeplineNode> getF_R() {
		return F_R;
	}
}
