package sweepline;
import java.util.Comparator;

public class Interval1D implements Comparable<Interval1D> {
	public final double low;   // left endpoint
	public final double high;  // right endpoint
	public double count;   // point weight
	public double height = -1;    //这个interval 所在的高度
	boolean isTop = false;

	// precondition: left <= right
	public Interval1D(double d, double e) {
		if (d <= e) {
			this.low  = d;
			this.high = e;
			this.count = 1;
		}

		else throw new RuntimeException("Illegal interval");
	}

	public Interval1D(Interval1D other) {
			this.low  = other.low;
			this.high = other.high;
			this.count = other.count;
			this.height = other.height;
	}
	
	// precondition: left <= right
		public Interval1D(double d, double e, double f, double h) {
			if (d <= e) {
				this.low  = d;
				this.high = e;
				this.count = f;
				this.height = h;
			}

			else throw new RuntimeException("Illegal interval");
		}
	
	
	// precondition: left <= right
	public Interval1D(double d, double e, double h) {
		if (d <= e) {
			this.low  = d;
			this.high = e;
			this.count = 1;
			this.height = h;
		}

		else throw new RuntimeException("Illegal interval");
	}


	// does this interval intersect that one?
	public boolean intersects(Interval1D that) {
		if (Double.compare(that.high, this.low) < 0) return false;
		if (Double.compare(this.high, that.low) < 0) return false;
		return true;
	}

	// does this interval a intersect b?
	public boolean contains(double x) {
		//    	System.out.println("low: "+low+" high: "+high+ " x: "+x);
		//    	boolean asd = (low <= x) && (x <= high);
		//    	System.out.println("(low <= x) && (x <= high)  :  "+ asd);
		if(Double.compare(low,x) <= 0 && Double.compare(x,high) <= 0)
			return true;
		return false;

		//    	return (low <= x) && (x <= high);
	}

	//to check weather "this" is on the left of "that" 
	public int compareTo(Interval1D that) {
		if      (Double.compare(this.low, that.low) < 0)  return -1;
		else if (Double.compare(this.low, that.low) > 0)  return +1;
		else if (Double.compare(this.high, that.high) < 0) return -1;
		else if (Double.compare(this.high, that.high) > 0) return +1;
		else                            return  0;
	}


	public static class heightComparator implements Comparator<Interval1D> {

		public int compare(Interval1D arg0, Interval1D arg1) {			
			if      (Double.compare(arg0.height, arg1.height) < 0)  return -1;
			else if (Double.compare(arg0.height, arg1.height) > 0)  return +1;
			else                            return  0;
		}

	}

	@Override
	public String toString() {
		if(height==-1)
			return "[" + low + ", " + high + "]";
		else
			return "[" + low + ", " + high + ", " + height + ", " + count+"]";	
	}


	// test client
	public static void main(String[] args) {
		Interval1D a = new Interval1D(15.1, 20.1);
		Interval1D b = new Interval1D(25.1, 30.1);
		Interval1D c = new Interval1D(10.1, 40.1);
		Interval1D d = new Interval1D(40.1, 50.1);

		StdOut.println("a = " + a);
		StdOut.println("b = " + b);
		StdOut.println("c = " + c);
		StdOut.println("d = " + d);

		StdOut.println("b intersects a = " + b.intersects(a));
		StdOut.println("a intersects b = " + a.intersects(b));
		StdOut.println("a intersects c = " + a.intersects(c));
		StdOut.println("a intersects d = " + a.intersects(d));
		StdOut.println("b intersects c = " + b.intersects(c));
		StdOut.println("b intersects d = " + b.intersects(d));
		StdOut.println("c intersects d = " + c.intersects(d));

	}
}