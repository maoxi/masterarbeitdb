package sweepline;

/*************************************************************************
 *  Compilation:  javac IntervalST.java
 *  Execution:    java IntervalST
 *  Dependencies: Interval1D.java
 *  
 *  Interval search tree implemented using a randomized BST!!!!!!!
 *
 *  Duplicate policy:  if an interval is inserted that already
 *                     exists, the new value overwrite the old one
 * 
 *************************************************************************/

import java.util.LinkedList;

// Interval search Tree 的特点是按照low value 排大小的 ，即 (10,30) 要比 (15,20) 小
public class IntervalST<Value> {  

	private Node root; // root of the BST, binary search tree

	// BST helper node data type
	private class Node {
		Interval1D interval; // key
		Value value; // associated data, 可以是任何数据，点的集合啊等等，这里用了Generics
		Node left, right; // left and right subtrees
		int N; // size of subtree rooted at this node
		double max; // interval 的右边值

		Node(Interval1D interval, Value value) {
			this.interval = interval;
			this.value = value;
			this.N = 1;  //本身自己这个Node就是一个subtree
			this.max = interval.high;
		}
	}

	/*************************************************************************
	 * BST search
	 *************************************************************************/

	public boolean contains(Interval1D interval) {
		return (get(interval) != null);
	}

	// return value associated with the given key
	// if no such value, return null
	public Value get(Interval1D interval) {
		return get(root, interval);
	}

	//未必从根节点开始, 从节点x开始
	private Value get(Node x, Interval1D interval) {
		if (x == null)
			return null;
		int cmp = interval.compareTo(x.interval);
		if (cmp < 0)
			return get(x.left, interval);
		else if (cmp > 0)
			return get(x.right, interval);
		else
			return x.value;
	}

	/*************************************************************************
	 * randomized insertion
	 *************************************************************************/
	public void put(Interval1D interval, Value value) {
		if (contains(interval)) {
			StdOut.println("duplicate");
			remove(interval);
		}
		root = randomizedInsert(root, interval, value);
		// System.out.println();
	}

	// make new node the root with uniform probability, rotation only happen at the bottom
	private Node randomizedInsert(Node x, Interval1D interval, Value value) {
		if (x == null)
			return new Node(interval, value);
		if (Math.random() * size(x) < 1.0)  // 0 or 1 interval, TODO 为什么不可能是2
			return rootInsert(x, interval, value);
		int cmp = interval.compareTo(x.interval);
		if (cmp < 0)
			x.left = randomizedInsert(x.left, interval, value);  //rotation does not happen here
		else
			x.right = randomizedInsert(x.right, interval, value);
		fix(x);
		return x;
	}
	// insert with rotation 不一定是root上，只要是insert到最底层的那一层
	private Node rootInsert(Node x, Interval1D interval, Value value) {
		if (x == null)
			return new Node(interval, value);
		int cmp = interval.compareTo(x.interval);
		if (cmp < 0) {
			x.left = rootInsert(x.left, interval, value);
			x = rotR(x);
		}
		else {
			x.right = rootInsert(x.right, interval, value);
			x = rotL(x);
		}
		return x;
	}

	/*************************************************************************
	 * deletion
	 *************************************************************************/
	private Node joinLR(Node a, Node b) {
		if (a == null)
			return b;
		if (b == null)
			return a;

		if (Math.random() * (size(a) + size(b)) < size(a)) { //TODO 用random的意义在哪里，一半一半的概率
			a.right = joinLR(a.right, b); //将b 和a.right合并作为a.right
			fix(a);
			return a;
		}
		else {
			b.left = joinLR(a, b.left);
			fix(b);
			return b;
		}
	}

	// remove and return value associated with given interval;找到一个interval并且删除，返回对应存储的value
	// if no such interval exists return null
	public Value remove(Interval1D interval) {
		Value value = get(interval);
		root = remove(root, interval);
		return value;
	}

	private Node remove(Node h, Interval1D interval) {
		if (h == null)   
			return null;
		int cmp = interval.compareTo(h.interval); //因为找的是完全契合的interval，所以不需要intersect，只要知道interval.low小于h.low就知道在左边了
		if (cmp < 0)
			h.left = remove(h.left, interval);
		else if (cmp > 0)
			h.right = remove(h.right, interval);
		else   //if h= interval，即 h就是所找的interval, 则合并h的左右树存为h
			h = joinLR(h.left, h.right);
		fix(h);
		return h;
	}

	/*************************************************************************
	 * Interval searching 
	 *************************************************************************/

	// return an interval in data structure that intersects the given inteval; 找到和某interval有交集的interval的第一个
	// return null if no such interval exists
	// running time is proportional to log N
	public Interval1D search(Interval1D interval) {
		return search(root, interval);
	}

	// look in subtree rooted at x
	public Interval1D search(Node x, Interval1D interval) {
		while (x != null) {
			if (interval.intersects(x.interval))
				return x.interval;
			else if (x.left == null) //左树为空的话在右树上找
				x = x.right;
			else if (x.left.max < interval.low)  //完全不可能有交集
				x = x.right;
			else
				x = x.left;
		}
		return null;
	}

	// return *all* intervals in data structure that intersect the given
	// interval
	// running time is proportional to R log N, where R is the number of
	// intersections
	public Iterable<Interval1D> searchAll(Interval1D interval) {
		LinkedList<Interval1D> list = new LinkedList<Interval1D>();
		searchAll(root, interval, list);
		return list;
	}

	// look in subtree rooted at x
	public boolean searchAll(Node x, Interval1D interval, LinkedList<Interval1D> list) {
		boolean found1 = false;
		boolean found2 = false; //左树
		boolean found3 = false; //右树
		if (x == null)
			return false;
		if (interval.intersects(x.interval)) {
			list.add(x.interval);
			found1 = true;
		}
		if (x.left != null && x.left.max >= interval.low)
			found2 = searchAll(x.left, interval, list);
		if (found2 || x.left == null || x.left.max < interval.low)
			found3 = searchAll(x.right, interval, list);
		return found1 || found2 || found3;
	}

	/*************************************************************************
	 * useful binary tree functions
	 *************************************************************************/

	// return number of nodes in subtree rooted at x
	public int size() {
		return size(root);
	}

	private int size(Node x) {
		if (x == null)
			return 0;
		else
			return x.N;
	}

	// height of tree (empty tree height = 0)
	public int height() {
		return height(root);
	}

	private int height(Node x) {
		if (x == null)
			return 0;
		return 1 + Math.max(height(x.left), height(x.right));
	}

	/*************************************************************************
	 * helper BST functions
	 *************************************************************************/

	// fix auxilliar information (subtree count and max fields)
	private void fix(Node x) {
		if (x == null)
			return;
		x.N = 1 + size(x.left) + size(x.right);
		x.max = max3(x.interval.high, max(x.left), max(x.right));
	}

	private double max(Node x) {
		if (x == null)
			return Integer.MIN_VALUE;
		return x.max;
	}

	// precondition: a is not null
	private double max3(double high, double d, double e) {
		return Math.max(high, Math.max(d, e));
	}

	// right rotate,   Rotation的意义在哪里?
	private Node rotR(Node h) {
		Node x = h.left;
		h.left = x.right; //h的左子树放的是x曾经右边的树
		x.right = h;
		fix(h);
		fix(x);
		return x;
	}

	// left rotate
	private Node rotL(Node h) {
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		fix(h);
		fix(x);
		return x;
	}

	/*************************************************************************
	 * Debugging functions that test the integrity of the tree
	 *************************************************************************/

	// check integrity of subtree count fields
	public boolean check() {
		return checkCount() && checkMax();
	}

	// check integrity of count fields
	private boolean checkCount() {
		return checkCount(root);
	}

	private boolean checkCount(Node x) {  //和fix()做的是同一件事，只不过返回true | false
		if (x == null)
			return true;
		return checkCount(x.left) && checkCount(x.right) && (x.N == 1 + size(x.left) + size(x.right));
	}

	private boolean checkMax() {
		return checkMax(root);
	}

	private boolean checkMax(Node x) {
		if (x == null)
			return true;
		return x.max == max3(x.interval.high, max(x.left), max(x.right));
	}

	/*************************************************************************
	 * BST printer
	 * 
	 * @return
	 *************************************************************************/

	public void inorder() {
		treePrinter(root);
	}

	private void treePrinter(Node cNode) {
		if (cNode == null)
			return;
		treePrinter(cNode.left);
		System.out.println("[" + cNode.interval.low + ", " + cNode.interval.high + "]" + " max = " + cNode.max);
		treePrinter(cNode.right);
	}

	/*************************************************************************
	 * test client
	 *************************************************************************/
	public static void main(String[] args) {
		int N = Integer.parseInt(args[0]);

		// generate N random intervals and insert into data structure
		IntervalST<String> st = new IntervalST<String>();
		for (int i = 0; i < N; i++) {
			double low = (Math.random() * 1000);
			double high = (Math.random() * 50) + low;
			Interval1D interval = new Interval1D(low, high);
			StdOut.println(interval);
			st.put(interval, "" + i);
		}

		// print out tree statistics
		System.out.println("height:          " + st.height());
		System.out.println("size:            " + st.size());
		System.out.println("integrity check: " + st.check());
		System.out.println();

		// generate random intervals and check for overlap
		for (int i = 0; i < N; i++) {
			double low = (Math.random() * 100);
			double high = (Math.random() * 10) + low;
			Interval1D interval = new Interval1D(low, high);
			StdOut.println(interval + ":  " + st.search(interval));
			StdOut.print(interval + ":  ");
			for (Interval1D x : st.searchAll(interval))
				StdOut.print(x + " ");
			StdOut.println();
			StdOut.println();
		}

	}

}
