package sweepline;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class LBBT	{   // 作为水平方向的interval存储使用,有自己的Node格式

	Interval1D current_maximum = new Interval1D(-999,999);

	//	private Node root;   // root of the BST
	public Node[] heap; //LBBT树
	private double[] array;  //最初得到的是一个array,由这个array造出LBBT树
	private int totalNodes;
	ArrayList<Node> F_IN = new ArrayList<Node>();  //f=> fork
	ArrayList<Node> F_L = new ArrayList<Node>();
	ArrayList<Node> F_R = new ArrayList<Node>();
	Node fork_node;  // 从disriminant位于left和right的那个第一个点开始
	Node leaf_of_F_L; // F_L的最后一个元素，已经到叶节点
	Node leaf_of_F_R;


	//这里的Node和interval是用来存储window的，而 ArrayList F_L, F_R 是用来划分window的
	ArrayList<NodeIntervalPair> windowsToInsert = new ArrayList<NodeIntervalPair>();

	//helper variables
	DecimalFormat df = new DecimalFormat();
	String printer_string = "";

	class NodeIntervalPair {
		public NodeIntervalPair(Node n2, Interval1D interval2) {
			n = n2;
			interval = interval2;
		}
		Node n;
		Interval1D interval; 
		
		public String toString(){
			return "NodeIntervalPair-> interval: "+interval+" below node n: "+n;
		}
	}

	// BST helper node data type
	class Node {
		MinPQ<Interval1D> pq;      // key

		int v;              // associated node number
		Node left, right;         // left and right subtrees
		int N;                    // size of subtree rooted at this node        
		double discriminant;  //假设有4个子节点的话，则discriminant为 2和3节点值的平均值, 用于划分是属于左边还是右边

		boolean isLeaf;

		//only for internal nodes
		Interval1D window = null; //only one window can be associated to an internal node
		double maxDegree = 0; //maximum degree among all windows in the subtree
		Node target = null; //pointer to the node whose associated window has maximum degree
		double pendingVal = 0; //used to update degrees of other windows in the subtree
		Node parent; //points to the predecessor node
		public int from;  // 子树是节点: 从。。。到。。  每个节点中都存储了子树的范围
		public int to;

		int size() {
			return to - from + 1;
		}

		Node() {
			this.parent = null;
			this.N        = 1;
		}

		public String toString() {
			return String.valueOf(discriminant);
		}
	}


	/**
	 * Time-Complexity:  O(n*log(n))
	 * builds tree bottom up
	 * @param array the Initialization array
	 */
	public LBBT(double[] array) {
		this.array = array.clone();
		//The max size of this array is about 2 * 2 ^ log2(n) + 1
		totalNodes = (int) (2 * Math.pow(2.0, Math.floor((Math.log(array.length) / Math.log(2.0)) + 1)));
		//		System.out.println("Max number of nodes: "+size);
		heap = new Node[totalNodes];
		build(1, 0, array.length);
	}


	public int size() {
		return totalNodes;
	}

	//Initialize the Nodes of the Segment tree
	//从编号v的Node开始，它的子树从from 到size
	private void build(int v, int from, int size) {
		//		System.out.println("Node: "+v+" from: "+from+" size: "+size);
		//将 heap[] 换成node[] 更容易理解
		heap[v] = new Node();
		heap[v].from = from;
		heap[v].to = from + size - 1;
		heap[v].v = v;

		if (size == 1) { //可以只build叶子，此时size给1, 或者build到叶子层时
			heap[v].isLeaf = true; 
			heap[v].discriminant = array[from]; //只有一个元素，无平均值可选
			heap[v].N = 1; 
			//			System.out.println("   Val: "+array[from]+" ");

		} else {
			heap[v].isLeaf = false;

			//Build childs
			build(2 * v, from, size / 2); //左边
			build(2 * v + 1, from + size / 2, size - size / 2);  //右边

			heap[v].left = heap[2 * v];
			heap[v].right = heap[2 * v + 1];
			heap[2 * v].parent = heap[v];
			heap[2*v + 1].parent = heap[v];
			heap[v].N = heap[2*v].N + heap[2*v + 1].N; 
			//假设有4个子节点的话，则discriminant为 2和3节点值的平均值
			heap[v].discriminant = (array[(heap[v].from + (size/2)) - 1] + array[(heap[v].from + (size/2))])/2; //取array[length/2]中间的两个坐标值得平均值
			
			//			System.out.println(" Node: "+v+" discriminant: "+heap[v].discriminant+" of "+ (heap[v].from + (int)(size/2)) +" and "+ ((heap[v].from + (int)(size/2)) + 1));
		}
	}

	/*
	 * HELPER FUNCTIONS
	 * 
	 */
	//只扩散到下一层的孩子, pendingVal(degrees of other windows in subtree)
	//Save the temporal values that will be propagated lazily
	//Propagate temporal values to children
	private void propagate(Node n) {
		if(n.isLeaf)	return;
		
		if (n.pendingVal != 0) {
			change(n.left, n.pendingVal);
			change(n.right, n.pendingVal);
			n.pendingVal = 0; //unset the pending propagation value
		}
	}
	
	private void change(Node c_n, double value) {
		if(c_n.isLeaf)
			return;

		c_n.pendingVal += value;
		c_n.maxDegree +=  value;
		if(c_n.window!=null){
			c_n.window.count += value;
		}
	}


	/*************************************************************************
	 *  BST printer - prints only existing Windows in the tree
	 * @return 
	 *************************************************************************/

	public void print() { 
		System.out.println("size = "+size());
		if ( heap[1]!=null ) print_tree(heap[1],1);
		System.out.println();
	}

	private void print_tree(Node p,int h) {  // h-> Height
		if ( !p.isLeaf ) print_tree(p.right,h+1); //右树

		for( int j=1; j <= h ; j++ ) System.out.print("     ");

		System.out.print(p.discriminant);

		if(p.window != null){
			System.out.print(" " + p.window.toString() + " ");  
		}

		if ( p.isLeaf )	{ 
			System.out.print(" [" + p.v + "] ");  
			System.out.print("\n");
		}
		else System.out.print("\n");

		if (  !p.isLeaf ) print_tree(p.left,h+1);  //左树
	}




	/*************************************************************************
	 *  Depth First printer - prints all nodes
	 * @return 
	 *************************************************************************/

	//print the tree
	public String print_inorder() { 
		df.setMaximumFractionDigits(15);
		printer_string = ""; //能在treePrinter中被改变的
		if ( heap[1]!=null ) treePrinter(heap[1]);
		return printer_string;
	}

	private void treePrinter(Node p) {  
		if(p.isLeaf) return;

		propagate(p);  //TODO 为什么要propagate来更新所有的值

		treePrinter(p.left);

		if(p.window != null){
			//window.count 是 这个interval里 point的 weight
			String temp = "[" + df.format(p.window.low) + ", " + df.format(p.window.high) +  ", " + df.format(p.window.count) + "]" + " -- ";
			printer_string += temp;  
		}

		treePrinter(p.right);
	}


	/******************
	 * debug search function
	 */
	public Node search(double x) {
		int i = 1;	
		while (!(heap[i].isLeaf)){  //从最上面breite suche，直到叶层，打印在哪层找到
			System.out.print(heap[i].discriminant+" ");
			i = (Double.compare(x,heap[i].discriminant) < 0) ? 2*i : 2*i + 1;
		}
		System.out.println();
		System.out.println(heap[i].discriminant+" found at node: "+ i);
		return heap[i];
	}
	
	/*** F_L存储了找到L这个点的遍历heap树的路径
	 * Search functions that find each terminal of the interval being processed
	 * used in FORWARD PASS for both top and bottom of rectangle
	 */
	public ArrayList<Node> searchF_L(double L) {  
		F_L.clear(); int i = 1;

		if (F_IN != null && !F_IN.isEmpty()) {
		}
		i = fork_node.v;  //从fork_node开始找起，而不是从root开始

		while (!(heap[i].isLeaf)){
			F_L.add(heap[i]);
			i = (Double.compare(L,heap[i].discriminant) < 0) ? 2*i : 2*i + 1;
		}
		F_L.add(heap[i]);
		leaf_of_F_L = heap[i];
		
//		System.out.print("Node L discriminant: "+heap[i].discriminant+" found at node: "+ i);
//		System.out.print("	F_L size: "+F_L.size());
//		System.out.print("\n	F_L: [");
//		Node p,p_prev = null;
//		for(int k = 0; k< F_L.size();k++){
//			p = F_L.get(k);
//			if(p_prev != null && p_prev.right != null && p_prev.right == p){
//				System.out.print(" -R- "+p);
//			}
//			else if(p_prev != null && p_prev.left != null && p_prev.left == p){
//				System.out.print(" -L- "+p);
//			}else{
//				System.out.print(" -- "+p);
//			}
//			p_prev = p;
//		}
//		System.out.print("\n");
		
		if(Double.compare(leaf_of_F_L.discriminant, L) !=0){
			this.print();
			throw new RuntimeException("Leaf node value mismatch L = "+L+" and leaf_of_F_L value ="+leaf_of_F_L.discriminant);
		}
		
		return F_L;
	}

	 //TODO no difference to searchF_L?? 感觉没和上个method有区别啊
	//可能传参数的时候 ，传 Interval的右侧点，所以叫 "R"
	public ArrayList<Node> searchF_R(double R) {
		F_R.clear();int i = 1;

		if (F_IN != null && !F_IN.isEmpty()) {
			i = fork_node.v;
		}

		while (!(heap[i].isLeaf)){
			F_R.add(heap[i]);
			i = (Double.compare(R,heap[i].discriminant) < 0) ? 2*i : 2*i + 1; 
		}

		F_R.add(heap[i]);
		leaf_of_F_R = heap[i];

//		System.out.print("Node R discriminant: "+heap[i].discriminant+" found at node: "+ i);
//		System.out.print("	F_R size: "+F_R.size());
//		System.out.print("\n	F_R: [");
//		Node p,p_prev = null;
//		for(int k = 0; k< F_R.size();k++){
//			p = F_R.get(k);
//			if(p_prev != null && p_prev.right != null && p_prev.right == p){
//				System.out.print(" -R- "+p);
//			}
//			else if(p_prev != null && p_prev.left != null && p_prev.left == p){
//				System.out.print(" -L- "+p);
//			}else{
//				System.out.print(" -- "+p);
//			}
//			p_prev = p;
//		}
//		System.out.print("\n");
		
		if(Double.compare(leaf_of_F_R.discriminant, R) !=0){
			this.print();
			throw new RuntimeException("Leaf node value mismatch R = "+R+" and leaf_of_F_R value ="+leaf_of_F_R.discriminant);
		}
		return F_R;
	}

	//interval的左右 low, high 值都用
	public ArrayList<Node> searchF_IN(Interval1D interval) {
		int i = 1;F_IN.clear();

		while (!(heap[i].isLeaf)){

			if(Double.compare(interval.high, heap[i].discriminant)<0){// 整个interval完全在discriminant 左侧
				F_IN.add(heap[i]);
				i = 2 * i;

			}else if(Double.compare(interval.low,heap[i].discriminant)>0){ // 整个interval在discriminant 右侧
				F_IN.add(heap[i]);
				i = 2 * i + 1;
			}
			//interval跨越 discriminant
			else if(Double.compare(interval.low, heap[i].discriminant)<0 && Double.compare(interval.high, heap[i].discriminant)>0){
				//found node v*
				break;
			}
		}
		fork_node = heap[i];

//		System.out.print("Node v* discriminant: "+heap[i].discriminant+" found at node: "+ i);
//		System.out.print("	F_IN size: "+F_IN.size());
//		System.out.print("\n	F_IN: [");
//		Node p, p_prev = null;
//		for(int k = 0; k< F_IN.size();k++){
//			p = F_IN.get(k);
//			if(p_prev != null && p_prev.right != null && p_prev.right == p){
//				System.out.print(" -R- "+p);
//			}
//			else if(p_prev != null && p_prev.left != null && p_prev.left == p){
//				System.out.print(" -L- "+p);
//			}else{
//				System.out.print(" -- "+p);
//			}
//			p_prev = p;
//		}
//		System.out.print("\n");
		
		return F_IN;
	}

	

	
	/******************
	 * insert functions
	 */


	
	/**
	 * insert window interval to a node below node n. Stores windows to be inserted at the
	 * end of the current iteration by function processInsert()
	 * @param n	
	 * @param interval
	 * 
	 */ 
	//将n和interval一对对插入到windowsToInsert的arrayList中
	//根据F_L, F_R 以及 给定interval，调用processF_L等，再调用insertWindow
	public void insertWindow(Node n, Interval1D interval){
		if(n == null) n = heap[1]; //从根节点开始
		// windowsToInsert中是 起始点和interval对，　在ｐｒｏｃｅｓｓＩｎｓｅｒｔ中才是从ｎ开始找到合适的子树插入ｉｎｔｅｒｖａｌ
		windowsToInsert.add(new NodeIntervalPair(n, interval));  
	}
	
	//如果window完全小于该node的discriminant,则分配给该node的左树，否则右树，如果跨越discriminant,则分配给当前node
	public void processInsert() {
		System.out.print("Processing insert of "+windowsToInsert.size()+" windows\n");
		
		for(NodeIntervalPair nip :windowsToInsert){
//			System.out.println(nip);
			Node n = nip.n;
			Interval1D interval = nip.interval;  //注意这里应该都写成window而不是interval
			while (!(n.isLeaf)){ //往下剥离，直到叶层
//				System.out.println("\t Accessing Node with determinant: "+n.discriminant+" and window: "+ (n.window!= null ? n.window : null) );
				if(Double.compare(interval.high, n.discriminant)<=0){
					n = n.left;
//					System.out.print("...next left");
				}else if(Double.compare(interval.low , n.discriminant)>=0){
					n = n.right;
//					System.out.print("...next right");
				}
				else if(Double.compare(interval.low ,n.discriminant)<0 && Double.compare(interval.high, n.discriminant)>0){
					//attach window to node and break;
					if(n.window != null){
						throw new RuntimeException("Overwriting window with "+interval+" at node: "+n.v);
					}
					System.out.println("Inserted window: "+ interval +" count: "+interval.count+" at discriminant: "+n.discriminant+" found at node: "+ n.v);
					n.window = interval;
					break;
				}else{	//there are duplicates on the x coordinate
//					throw new RuntimeException("Duplicates on the x coordinate.");
				}
			}
		}
//		System.out.println("...all new windows have been inserted");
		windowsToInsert.clear();
	}



	/*
	 * FORWARD PASS for inserting top of rectangle
	 * 
	 */
	// 对所有的 F_IN 中的节点都 细分interval，node中存储的window被interval割分，即conquer and divide中的divide步骤
	public void processF_IN(Interval1D interval) {
		//check all nodes on F_IN
		System.out.println("Processing F_IN ->>>>");
		for(Node n : F_IN){
			propagate(n);
			if(n.window!=null){
				processF_IN(n,interval);
			}
		}

		//	if interval is contained in window x,y, then we must also check 
		//fork_node = leaf_of_F_IN
		propagate(fork_node);
		if(fork_node.window!=null){
			processF_IN(fork_node,interval);
		}

	}

	//interval是分隔标准，n.window被分割
	private void processF_IN(Node n, Interval1D interval) {
		boolean isLeft = Double.compare(n.window.low , interval.low)<0 && Double.compare(interval.low , n.window.high)<0;	//means interval left point falls between window
		boolean isRight = Double.compare(n.window.low , interval.high)<0 && Double.compare(interval.high , n.window.high)<0;	//means interval right point falls between window

		System.out.println("	Interval: "+interval+" Node discriminant: "+n.discriminant+" window: "+n.window+ " isLeft: "+isLeft+" isRight: "+isRight);

		// [] represents the interval
		// () represents the window at the current node
		
		if(isLeft && isRight){	// (...[..]...)
			Interval1D window1 = new Interval1D(n.window.low, interval.low, interval.height);
			window1.count = n.window.count; //更新权重
			Interval1D window2 = new Interval1D(interval.low, interval.high , interval.height);
			window2.count = n.window.count + interval.count ;
			Interval1D window3 = new Interval1D(interval.high, n.window.high, interval.height);
			window3.count = n.window.count;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
			insertWindow(n, window3);
		}
		else if( isLeft && !isRight ){ // (....[....)...]   只有和window交叉重叠的部分才单独隔离，没有和window交叉的直接舍弃
			Interval1D window1 = new Interval1D(n.window.low, interval.low, interval.height);
			window1.count = n.window.count;
			Interval1D window2 = new Interval1D(interval.low, n.window.high , interval.height);
			window2.count = n.window.count + interval.count ;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
		}
		else if(!isLeft && isRight){ // [....(....]....)
			Interval1D window1 = new Interval1D(n.window.low, interval.high , interval.height);
			window1.count = n.window.count + interval.count ;
			Interval1D window2 = new Interval1D(interval.high, n.window.high, interval.height);
			window2.count = n.window.count;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
		}
		else if( Double.compare(n.window.low , interval.low)>0 && Double.compare(n.window.high , interval.high)<0 && n!=fork_node){
			throw new RuntimeException("ERR-> Window "+ n.window +" on F_IN is within the bounds of the interval: "+interval);
		}
	}


	//根据 interval 的左右两端点来划分，名字为: processForkNode_left
	public void processF_L(Interval1D interval) {
		System.out.println("Processing F_L ->>>>");
		for(int i = 1; i < F_L.size() - 1; i++){
			Node n = F_L.get(i); 
			propagate(n);

//			System.out.print("\n");
			if((i+1) < F_L.size()){
				Node n_next = F_L.get(i+1);
				if(n_next.equals(n.left)){  //TODO 是interval的low在discriminant左侧，所以完全覆盖右侧的意思?
					n.right.maxDegree+=interval.count; 
					n.right.pendingVal+=interval.count;

					if(n.right.window!=null){
						n.right.window.count += interval.count;
					}

//					System.out.print("===Tree update n.right: "+n.right+" to degree: "+n.right.maxDegree+" to pendingVal: "+n.right.pendingVal);
//					if(n.right.window!=null)
//						System.out.print(" AND updating window: "+ n.right.window +" count: "+  n.right.window.count);		
//					System.out.print("\n");		
				}
			}
			
			if(n.window==null)	continue;
			processF_L(n,interval);
		}

	}

	//根据interval的左端点来划分，右端点不用来划分，所以只有window1, window2两种情况
	private void processF_L(Node n, Interval1D interval) {
		System.out.println("	Interval: "+interval+" Node discriminant: "+n.discriminant+" window: "+n.window);
		//[()] interval 范围比 window 大
		if(Double.compare(n.window.low , interval.low) > 0 && Double.compare(n.window.high , interval.high) < 0){
			n.window.count+=interval.count;
		}
		//([)]        ([])=>不可能出现，因为F_L是从F_IN开始的节点
		if(Double.compare(interval.low , n.window.low) > 0 && Double.compare(interval.low , n.window.high) < 0){//TODO should be interval.high?
			Interval1D window1 = new Interval1D(n.window.low, interval.low, interval.height);
			window1.count = n.window.count;
			Interval1D window2 = new Interval1D(interval.low, n.window.high , interval.height);
			window2.count = n.window.count + interval.count ;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
		}
	}

	public void processF_R(Interval1D interval) {
		System.out.println("Processing F_R ->>>>");
		for(int i = 1; i < F_R.size() - 1; i++){
			Node n = F_R.get(i); 
			propagate(n);
			
//			System.out.print("\n");

			if((i+1) < F_R.size()){
				Node n_next = F_R.get(i+1);
				if(n_next.equals(n.right)){

					n.left.maxDegree+=interval.count;
					n.left.pendingVal+=interval.count;

					if(n.left.window!=null){
						n.left.window.count += interval.count;
					}
					
//					System.out.print("===Tree update n.left: "+n.left+" to degree: "+n.left.maxDegree+" to pendingVal: "+n.left.pendingVal);
//					if(n.left.window!=null)
//						System.out.print(" AND updating window: "+ n.left.window +" count: "+  n.left.window.count);
//					System.out.print("\n");
				}
			}
			
			if(n.window==null)	continue;
			processF_R(n,interval);
		}
	}

	private void processF_R(Node n, Interval1D interval) {
		System.out.println("	Interval: "+interval+" Node discriminant: "+n.discriminant+" window: "+n.window);
		//[()]
		if(Double.compare(n.window.low , interval.low)>0 && Double.compare(n.window.high , interval.high)<0){
			n.window.count+=interval.count;
		}
		//[  (])  or ([])
		if(Double.compare(n.window.low , interval.high)<0 && Double.compare(interval.high , n.window.high)<0){	//(...-
			Interval1D window1 = new Interval1D(n.window.low, interval.high , interval.height);
			Interval1D window2 = new Interval1D(interval.high, n.window.high, interval.height);
			window1.count = n.window.count + interval.count;
			window2.count = n.window.count;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
		}

	}


	//光对ForkNode这一点用interval将window进行划分
	public void processForkNode(Interval1D interval) {
		System.out.println("Processing Fork ->>>>");

		Node n = fork_node; 
		propagate(n);
		if(n.window==null)	return;

		System.out.println("	Interval: "+interval+" Node discriminant: "+n.discriminant+" window: "+n.window);
		//	[()]
		if(Double.compare(n.window.low , interval.low)>0 && Double.compare(n.window.high , interval.high)<0){
			n.window.count+=interval.count;
		}
		//  [(]) or ([])
		if(Double.compare(n.window.low , interval.high)<0 && Double.compare(interval.high , n.window.high)<0){	//(...-
			Interval1D window1 = new Interval1D(n.window.low, interval.high , interval.height);
			Interval1D window2 = new Interval1D(interval.high, n.window.high, interval.height);
			window1.count = n.window.count + interval.count ;
			window2.count = n.window.count;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
		}  //([)] or ([])
		else	if(Double.compare(interval.low , n.window.low)>0 && Double.compare(interval.low , n.window.high)<0){
			Interval1D window1 = new Interval1D(n.window.low, interval.low, interval.height);
			window1.count = n.window.count;
			Interval1D window2 = new Interval1D(interval.low, n.window.high , interval.height);
			window2.count = n.window.count + interval.count ;

			n.window = null;
			insertWindow(n, window1);
			insertWindow(n, window2);
		}
	}


	/*
	 * FORWARD PASS for bottom of rectangle
	 * remove intervals when bottom of rectangle is encountered.
	 */

	/**
	 * find nodes with end points in F_IN + F_L + F_R
	 */

	public void processRemove(Interval1D interval) {

		//LxR implies that left side of interval matches right side of a window
		//RxL implies right side of interval matches left side of window
		//and so on...
		//LxL combines with LxR
		//RxL combines with RxR 
		//if LxL and RxR are true for the same window then combine all three intervals into one window
		// ..|.LxR..[.LxL.|---|.RxR.].RxL..|..

		//		boolean LxL = n.window.low == interval.low;
		//		boolean LxR = n.window.high == interval.low;
		//		boolean RxL = interval.high == n.window.low;
		//		boolean RxR = interval.high == n.window.high;

		//所有 触及interval左侧的window所在的node
		Node n_LxL = null, n_LxR = null, n_RxL = null, n_RxR = null;

		for(Node z : F_IN){
			if(z.window == null)	continue;

			//if window is completely within the interval
			if(Double.compare(z.window.low , interval.low)>0 && Double.compare(z.window.high , interval.high)<0){ //(...[...]...)
				z.window.count-=interval.count;  
			}
			//if interval is right at the boundary
			boolean LxL = Double.compare(z.window.low , interval.low)==0;  // 
			boolean LxR = Double.compare(z.window.high , interval.low)==0;  //window在interval左侧
			boolean RxL = Double.compare(interval.high , z.window.low)==0;  // window在 interval右侧
			boolean RxR = Double.compare(interval.high , z.window.high)==0;

			if(LxL && RxR){ //window正好和interval重叠，该window跨越整个interval
				n_LxL = n_RxR = z;
			}else if(LxL){
				n_LxL = z;
			}else if(LxR){
				n_LxR = z;
			}else if(RxR){
				n_RxR = z;
			}else if(RxL){
				n_RxL = z;
			}
		}

		for(Node z : F_R){
			if(z.window == null)	continue;

			//if window is completely within the interval
			if(Double.compare(z.window.low , interval.low)>0 && Double.compare(z.window.high, interval.high)<0){ //(...[...]...)
				z.window.count-=interval.count;
			}

			//if interval meets the boundary of the rectangles
			boolean LxL = Double.compare(z.window.low , interval.low)==0;
			boolean LxR = Double.compare(z.window.high , interval.low)==0;
			boolean RxL = Double.compare(interval.high , z.window.low)==0;
			boolean RxR = Double.compare(interval.high , z.window.high)==0;

			if(LxL && RxR){
				n_LxL = n_RxR = z;
			}else if(LxL){
				n_LxL = z;
			}else if(LxR){
				n_LxR = z;
			}else if(RxR){
				n_RxR = z;
			}else if(RxL){
				n_RxL = z;
			}
		}

		for(Node z : F_L){
			if(z.window == null)	continue;

			if(z!=fork_node){
				if(Double.compare(z.window.low , interval.low)>0 && Double.compare(z.window.high , interval.high)<0){ //(...[...]...)
					z.window.count-=interval.count;
				}
			}

			boolean LxL = Double.compare(z.window.low , interval.low)==0;
			boolean LxR = Double.compare(z.window.high , interval.low)==0;
			boolean RxL = Double.compare(interval.high , z.window.low)==0;
			boolean RxR = Double.compare(interval.high , z.window.high)==0;

			if(LxL && RxR){
				n_LxL = n_RxR = z;
			}else if(LxL){
				n_LxL = z;
			}else if(LxR){
				n_LxR = z;
			}else if(RxR){
				n_RxR = z;
			}else if(RxL){
				n_RxL = z;
			}
		}

		System.out.println("Removing Interval: "+interval+"  n_LxL: "+n_LxL.window+" n_LxR: "+n_LxR.window+" n_RxL: "+n_RxL.window+" n_RxR: "+n_RxR.window);

		// ..|.LxR..[.LxL.|---|.RxR.].RxL..|..
		if(n_LxL.equals(n_RxR)){
			Interval1D window = new Interval1D(n_LxR.window.low, n_RxL.window.high , n_RxL.window.height);
			window.count = n_LxR.window.count;
			n_LxR.window=null;n_LxL.window=null;n_RxR.window=null;n_RxL.window=null;
			insertWindow(null, window);
		}else{
			Interval1D window1 = new Interval1D(n_LxR.window.low, n_LxL.window.high , n_LxR.window.height);
			window1.count = n_LxR.window.count; //window1为去除interval的左边界合并后的interval,后将重新插入
			Interval1D window2 = new Interval1D(n_RxR.window.low, n_RxL.window.high , n_RxL.window.height);
			window2.count = n_RxL.window.count;
			n_LxR.window=null;n_LxL.window=null;n_RxR.window=null;n_RxL.window=null; //删除原node
			insertWindow(null, window1);
			insertWindow(null, window2);
		}


		{
			propagate(fork_node);
		}

		for(int i = 1; i < F_R.size(); i++){   //TODO 为什么没有F_IN的对应函数
			Node n = F_R.get(i); 
			propagate(n);

//			System.out.print("\n");
			
			if((i+1) < F_R.size()){
				Node n_next = F_R.get(i+1);
				if(n_next.equals(n.right)){
					propagate(n_next);
					n.left.maxDegree-=interval.count;
					n.left.pendingVal-=interval.count;

					if(n.left.window!=null){
						n.left.window.count -= interval.count;
					}
					
//					System.out.print("===Tree de-update n.left: "+n.left+" to degree: "+n.left.maxDegree+" to pendingVal: "+n.left.pendingVal);
//					if(n.left.window!=null)
//						System.out.print(" AND updating window: "+ n.left.window +" count: "+  n.left.window.count);
//					System.out.print("\n");

				}
			}	
		}

		for(int i = 1; i < F_L.size(); i++){
			Node n = F_L.get(i); 
			propagate(n);
			
//			System.out.print("\n");

			if((i+1) < F_L.size()){
				Node n_next = F_L.get(i+1);
				if(n_next.equals(n.left)){
					propagate(n_next);
					n.right.maxDegree-=interval.count;
					n.right.pendingVal-=interval.count;

					if(n.right.window!=null){
						n.right.window.count -= interval.count;
					}

//					System.out.print("===Tree de-update n.right: "+n.right+" to degree: "+n.right.maxDegree+" to pendingVal: "+n.right.pendingVal);
//					if(n.right.window!=null)
//						System.out.print(" AND updating window: "+ n.right.window +" count: "+  n.right.window.count);		
//					System.out.print("\n");
				}
			}
		}

	}


	/*
	 * starting from the leaf nodes L and R update all target pointers
	 * tracing up to the root node. Result will be that the root node 
	 * holds the link to the window with the maximum degree.
	 */

	public void backwardPass() {
		System.out.print("\nUPDATING targets -> ");

		Node n = leaf_of_F_L.parent;
		while(n!=fork_node.parent){
//			System.out.println("\t node on F_L discriminant: "+n.discriminant+" Left side window: "+ n.left.window +" target: "+ ((n.left.target!=null) ? n.left.target.window : null) + " || Right side window: "+n.right.window+" target: "+ ((n.right.target!=null) ? (n.right.target.window==null ? "window null" : n.right.target.window) : null));
			updateTargets(n);
			n = n.parent;
		}

		n = leaf_of_F_R.parent;
		while(n!=fork_node.parent){
//			System.out.println("\t node on F_R discriminant: "+n.discriminant+" Left side window: "+ n.left.window +" target: "+ ((n.left.target!=null) ? n.left.target.window : null) + " || Right side window: "+n.right.window+" target: "+ ((n.right.target!=null) ? (n.right.target.window==null ? "window null" : n.right.target.window) : null));
			updateTargets(n);
			n = n.parent;
		}

		n = fork_node;
		while(n!=null){
//			System.out.println("\t node on F_IN discriminant: "+n.discriminant+" Left side window: "+ n.left.window +" target: "+ ((n.left.target!=null) ? n.left.target.window : null) + " || Right side window: "+n.right.window+" target: "+ ((n.right.target!=null) ? (n.right.target.window==null ? "window null" : n.right.target.window) : null));
			updateTargets(n);
			n = n.parent;
		}
		
		//finally record the maximum
		if(heap[1].target !=null){
			if(heap[1].target.window.count > current_maximum.count){
//				System.out.println(" UPDATED CURRENT MAXIMUM: "+heap[1].target.window+" with count: "+heap[1].target.window.count+ " Node :"+heap[1].target);
				current_maximum = new Interval1D (heap[1].target.window);
			}
		}
		
		System.out.print("... finished updating.\n");
	}

	/*
	 * updates the target pointers to all nodes in the subtree rooted at this node
	 */
	private void updateTargets(Node n){

		double left_maxDegree =0, right_maxDegree = 0;
		Node left_target=null,right_target = null;

		//left subtree
		if(n.left.window != null && n.left.target != null && n.left.target.window != null){
			 //TODO n.left.maxDegree 和 n.left.targe有相关性? window.count是该Node直属的window下point的数量，二maxDegree是target里的point数量，这个未必是直属的
			if(Double.compare(n.left.maxDegree , n.left.window.count)>0 ){ 
				left_maxDegree = n.left.maxDegree;
				left_target = n.left.target;
//				System.out.println("\t\t up1dating maxDegree to "+left_maxDegree+ " for : "+ left_target.window);
			}
			else{
				left_maxDegree = n.left.window.count;
				left_target = n.left;
//				System.out.println("\t\t up2dating maxDegree to "+left_maxDegree+ " for : "+ left_target.window);
			}
		}
		else if(n.left.window != null && n.left.target == null){
			left_maxDegree = n.left.window.count;
			left_target = n.left;
//			System.out.println("\t\t up3dating maxDegree to "+left_maxDegree+ " for : "+ left_target.window);
		}
		else if(n.left.window == null && n.left.target != null && n.left.target.window != null){
			left_maxDegree = n.left.maxDegree;
			left_target = n.left.target;
//			System.out.println("\t\t up4dating maxDegree to "+left_maxDegree+ " for : "+ left_target.window);
		}

		if(n.left.target != null && n.left.target.window == null){
			n.left.target = null;
			n.left.maxDegree = 0;
		}



		//right subtree
		if(n.right.window != null && n.right.target != null && n.right.target.window != null){
			if(Double.compare(n.right.maxDegree, n.right.window.count)>0 ){
				right_maxDegree = n.right.maxDegree;
				right_target = n.right.target;
//				System.out.println("\t\t up5dating maxDegree to "+right_maxDegree+ " for : "+ right_target.window);
			}
			else{
				right_maxDegree = n.right.window.count;
				right_target = n.right;
//				System.out.println("\t\t up6dating maxDegree to "+right_maxDegree+ " for : "+ right_target.window);
			}
		}
		else if(n.right.window != null && n.right.target == null){
			right_maxDegree = n.right.window.count;
			right_target = n.right;
//			System.out.println("\t\t up7dating maxDegree to "+right_maxDegree+ " for : "+ right_target.window);
		}
		else if(n.right.window == null && n.right.target != null && n.right.target.window != null){
			right_maxDegree = n.right.maxDegree;
			right_target = n.right.target;
//			System.out.println("\t\t up8dating maxDegree to "+right_maxDegree+ " for : "+ right_target.window);
		}

		if(n.right.target != null && n.right.target.window == null){
			n.right.target = null;
			n.right.maxDegree = 0;
		}


		//get the best among them
		if(left_maxDegree > right_maxDegree){
			n.maxDegree = left_maxDegree;
			n.target = left_target;
		}else{
			n.maxDegree = right_maxDegree;
			n.target = right_target;
		}

//		if(n.target !=null)
//			System.out.println("\t update final: maxDegree to "+n.maxDegree+ " for : "+ n.target.window);

	}
}
