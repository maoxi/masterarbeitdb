package sweepline;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import tree.Grid;
import tree.Point;
import utilities.Utilities;

/*************************************************************************
 * Generate N random 2D intervals and print out all pairwise intersections
 * between them.
 *
 * Limitations ----- - Assumes no two y-intervals are identical. This is because
 * interval search tree disallows duplicates. To fix, create a helper interval
 * data type that uses x-interval to break ties.
 *
 *************************************************************************/

public class MaxRS {  // Maximum Range Scan

	// helper class for events in sweep line algorithm
	public static class Event implements Comparable<Event> {
		double time;  //y轴方向的下端
		Interval2D rect;  //注意不是水平的interval，而是整个rect.
		boolean isTop;
		// low just means edge, should be unnamed
		//Event is the edge, top is used to point out upper edge or the lower edge
		public Event(double low, Interval2D rect, boolean isTop) { 
			this.time = low;
			this.rect = rect;
			this.isTop = isTop;
		}

		public int compareTo(Event that) {
			if (this.time < that.time)
				return -1;
			else if (this.time > that.time)
				return +1;
			else
				return 0;
		}

		public static class heightComparator implements Comparator<Event> {

			public int compare(Event arg0, Event arg1) {
				if (arg0.time > arg1.time)
					return -1;
				else if (arg0.time < arg1.time)
					return +1;
				// if y is the same, use intervalX to decide
				// TODO here? should it not be arg0.rect.intervalx.max <
				// arg1.rect.intervalX.max
				else if (arg0.rect.intervalX.low < arg1.rect.intervalX.low || arg0.rect.intervalX.low > arg1.rect.intervalX.low)
					return -1;
				else
					return 0;
			}
		}
	}

	// TODO: Handle boundary cases.
	// two points with the same x coordinate - cannot create tree when two
	// points have the same x coordinates
	// two points with the same y coordinate - no problem just break tie from
	// left to right

	public static double runSweepLine(ArrayList<Point> points, double height, double width) { //给定范围以及点对应的矩形的宽度和高度

		// for(Point p : points){
		// System.out.println(p);
		// }
		int N = points.size();
		Interval2D[] rects = new Interval2D[N];

		//sortiereng过, event to save all y-intervals
		MinPQ<Event> pqLBBT = new MinPQ<Event>(new Event.heightComparator());  
		double[] lbbtArray = new double[2 * N + 2];   // all x-intervals  low,high交替, 注意只是个array,不是LBBT class

		Point p = null;

		// disambiguate x coordinates 只对 x轴处理，防止完全相同的x轴的坐标
		Set<Double> hSet = new HashSet<Double>(); //临时存储points的x点，用来查看是否有重复
		for (int i = 0; i < N; i++) {
			p = points.get(i);
			double x_cor = Utilities.truncateDecimal(p.getX(), 10).doubleValue();
			double x_cor_w = Utilities.truncateDecimal(p.getX() + width, 10).doubleValue();
			while (true) {
				if (hSet.contains(x_cor) || hSet.contains(x_cor_w)) {
					Random randomGenerator = new Random();
					int randomInt = randomGenerator.nextInt(10000);
					double incr = 0.00000001 * randomInt;
					x_cor = Utilities.truncateDecimal(p.getX() + incr, 10).doubleValue();
					x_cor_w = Utilities.truncateDecimal(x_cor + width, 10).doubleValue();  

				} else {
					hSet.add(x_cor);
					hSet.add(x_cor_w);
					break;
				}
			}
			// lbbtArray 水平方向左右两端点， pqLBBT 垂直方向上下两值的event, 水平方向是一次sortieren过的，而垂直方向随着添加减少一直在sortieren最小值
			//点对应的矩形
			rects[i] = new Interval2D(new Interval1D(x_cor, x_cor_w, p.getWeight(), 0), new Interval1D(p.getY(), p.getY() + height, p.getWeight(), 0));
			Event e1 = new Event(rects[i].intervalY.low, rects[i], false);  //上下都记为event
			Event e2 = new Event(rects[i].intervalY.high, rects[i], true);
			lbbtArray[2 * i] = rects[i].intervalX.low;
			lbbtArray[(2 * i) + 1] = rects[i].intervalX.high;
			pqLBBT.insert(e1);
			pqLBBT.insert(e2);
		}

		/*
		 * for (int i = 0; i < N; i++) { p = points.get(i); rects[i] = new
		 * Interval2D(new Interval1D(p.getX(), p.getX()+width), new
		 * Interval1D(p.getY(), p.getY() + height)); Event e1 = new
		 * Event(rects[i].intervalY.low, rects[i], false); Event e2 = new
		 * Event(rects[i].intervalY.high, rects[i], true); lbbtArray[2*i] =
		 * rects[i].intervalX.low; lbbtArray[(2*i)+1] = rects[i].intervalX.high;
		 * pqLBBT.insert(e1); pqLBBT.insert(e2); }
		 */

		long startTime = System.currentTimeMillis();

		lbbtArray[2 * N] = Grid.MIN_X - 1;  // 最后两个element存了Grid的范围
		lbbtArray[2 * N + 1] = Grid.MAX_X + 1;

		Arrays.sort(lbbtArray);

		// for(int z = 0; z< lbbtArray.length-1; z++){
		//
		// if(Double.compare(lbbtArray[z], lbbtArray[z+1])==0){
		// throw new RuntimeException("FUARK");
		// }
		// System.out.println(lbbtArray[z]);
		// }
		//
		LBBT lbbt_other = new LBBT(lbbtArray);  //此时array被传到 klass LBBT中，用以造LBBT树，并且填充了discriminant的值,没有window的值
		System.out.println("LBBT tree created...span: " + lbbt_other.heap[1].N + " size: " + lbbt_other.size());
		System.out.println("Priority Queue contains " + pqLBBT.size() + " events.");

		double max_height = Grid.MAX_Y + 1;
		Interval1D top_floor = new Interval1D(lbbtArray[0], lbbtArray[2 * N + 1], max_height); //最高最宽的扫描线
		top_floor.count = 0;  //weight=0 
		//heap[1] 作为初始时 lbbt树里的node仅有的window，后面进入的interval都是对其进行划分
		lbbt_other.heap[1].window = top_floor;  

		// ArrayList<String> printer_helper = new ArrayList<String>();

		while (!pqLBBT.isEmpty()) { //从上往下开始扫描
			Event e = pqLBBT.delMin();  //总是从sortier过得pqLBBT中取出y值最小的。
			double time = e.time;
			Interval2D rect = e.rect;
			//取rect的上面这条边
			Interval1D horizontal_interval = new Interval1D(rect.intervalX.low, rect.intervalX.high, rect.intervalX.count, rect.intervalY.high);

			/**
			 * event is the BOTTOM endpoint of rectangle
			 * 如果是遇到rect的下边，则遍历F_IN, F_L, F_R中interval的路径， 然后删除这个interval? TODO 删除这里没有看懂
			 */
			if (time == rect.intervalY.low) {
				horizontal_interval.isTop = false; //isTop值这时候才写入
				System.out.println("------------------------------------------");
				System.out.println("Processing Interval: " + horizontal_interval + " isTop: " + horizontal_interval.isTop);

				// printer_helper.add("Removing Interval: "+horizontal_interval);

				lbbt_other.searchF_IN(horizontal_interval);  //TODO 这个有必要分开写么? 区分top和bottom情况
				lbbt_other.searchF_L(rect.intervalX.low);
				lbbt_other.searchF_R(rect.intervalX.high);
				lbbt_other.processRemove(horizontal_interval);
				lbbt_other.processInsert();

				// printer_helper.add(lbbt_other.print_inorder());
				// for(String p_str : printer_helper){
				// System.out.println(p_str);
				// }
				// printer_helper.clear();

			}
			/**
			 * event is the TOP endpoint of rectangle
			 * 如果是遇到tect的上边，则在F_IN, F_L, F_R 中找到到interval必须的路径，然后在这些树中细分因新的interval而产生的小的interval, 即添加interval
			 */
			else {
				horizontal_interval.isTop = true;
				System.out.println("------------------------------------------");
				System.out.println("Processing Interval: " + horizontal_interval + " isTop: " + horizontal_interval.isTop);


				lbbt_other.searchF_IN(horizontal_interval);  //F_IN 被填充为需要这到interval而遍历的heap的所有
				lbbt_other.searchF_L(rect.intervalX.low);
				lbbt_other.searchF_R(rect.intervalX.high);

				lbbt_other.processF_IN(horizontal_interval);
				lbbt_other.processF_L(horizontal_interval);
				lbbt_other.processF_R(horizontal_interval);
				lbbt_other.processForkNode(horizontal_interval);  //TODO 不是在ProcessF_IN中就处理了?
				lbbt_other.processInsert();
			}
			lbbt_other.backwardPass();   //从上到下扫描过后，通过这个method找到target点，即指向包含最大degree的window的node

			//
			// if(pqLBBT.size()%50 == 0 ){
			// System.out.println("Interval left to process: "+pqLBBT.size());
			// }

			// if(Double.compare(horizontal_interval.low,-112.0632727) == 0 &&
			// Double.compare(horizontal_interval.high,-112.0363227) == 0){
			// lbbt_other.print();
			// }

			// for(String p_str : printer_helper){
			// System.out.println(p_str);
			// }
			// printer_helper.clear();
		}
		// lbbt_other.print();

		// for(String p_str : printer_helper){
		// System.out.println(p_str);
		// }

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Time for executing sweepline: " + elapsedTime + " ms");
		System.out.println("Reported Range: " + lbbt_other.current_maximum + " with count: " + lbbt_other.current_maximum.count);
		return lbbt_other.current_maximum.count;
	}

	public static void main(String[] args) throws FileNotFoundException {

		// PrintStream out = new PrintStream(new
		// FileOutputStream("output.txt"));
		// System.setOut(out);

		int N = Integer.parseInt(args[0]);

		// generate N random intervals
		Interval2D[] rects = new Interval2D[N];

		for (int i = 0; i < N; i++) {
			double xmin = (100 * Math.random());
			double ymin = (100 * Math.random());
			double xmax = xmin + (20 * Math.random());
			double ymax = ymin + (10 * Math.random());
			rects[i] = new Interval2D(new Interval1D(xmin, xmax), new Interval1D(ymin, ymax));
			// System.out.println(rects[i]);
		}
		// System.out.println();

		/*
		 * // create events MinPQ<Event> pq = new MinPQ<Event>();
		 * 
		 * 
		 * for (int i = 0; i < N; i++) { Event e1 = new
		 * Event(rects[i].intervalX.low, rects[i], false); Event e2 = new
		 * Event(rects[i].intervalX.high, rects[i],true); pq.insert(e1);
		 * pq.insert(e2); } System.out.println(
		 * "Number of processed intervals in intersection reporting: "
		 * +pq.size());
		 * 
		 * 
		 * // run sweep-line algorithm IntervalST<Interval2D> st = new
		 * IntervalST<Interval2D>(); while (!pq.isEmpty()) { Event e =
		 * pq.delMin(); double time = e.time; Interval2D rect = e.rect;
		 * 
		 * // next event is the right endpoint of interval i if (time ==
		 * rect.intervalX.high){ //
		 * System.out.println("Removed interval: "+rect.intervalY);
		 * st.remove(rect.intervalY); // st.inorder(); } // next event is the
		 * left endpoint of interval i else { for (Interval1D x :
		 * st.searchAll(rect.intervalY)) { // StdOut.println("Intersection:  " +
		 * rect + ", " + st.get(x)); } //
		 * System.out.println("Inserted interval: "+rect.intervalY);
		 * st.put(rect.intervalY, rect); // st.inorder();
		 * 
		 * } }
		 */

		// LBBT instantiation
		// create events

		MinPQ<Event> pqLBBT = new MinPQ<Event>(new Event.heightComparator());
		double[] lbbtArray = new double[2 * N + 2];

		for (int i = 0; i < N; i++) {
			Event e1 = new Event(rects[i].intervalY.low, rects[i], false);
			Event e2 = new Event(rects[i].intervalY.high, rects[i], true);
			lbbtArray[2 * (i)] = rects[i].intervalX.low;
			lbbtArray[(2 * i) + 1] = rects[i].intervalX.high;
			pqLBBT.insert(e1);
			pqLBBT.insert(e2);
		}
		System.out.println("Number of processed intervals in MaxRS: " + pqLBBT.size());

		lbbtArray[2 * N + 1] = 120.00;

		Arrays.sort(lbbtArray);
		LBBT lbbt_other = new LBBT(lbbtArray);
		// System.out.println("LBBT heap size: "+lbbt_other.heap[1].N +
		// " lbbt size: "+lbbt_other.size());
		double max_height = 120;
		Interval1D top_floor = new Interval1D(lbbtArray[0], lbbtArray[2 * N + 1], max_height);
		top_floor.count = 0;
		lbbt_other.heap[1].window = top_floor;

		// ArrayList<String> printer_helper = new ArrayList<String>();

		// boolean asd = false;
		while (!pqLBBT.isEmpty()) {
			Event e = pqLBBT.delMin();
			double time = e.time;
			Interval2D rect = e.rect;
			Interval1D horizontal_interval = new Interval1D(rect.intervalX.low, rect.intervalX.high, rect.intervalY.high);

			/**
			 * event is the BOTTOM endpoint of rectangle
			 */
			if (time == rect.intervalY.low) {
				horizontal_interval.isTop = false;
				// System.out.println("------------------------------------------");
				// System.out.println("Processing Interval: "+horizontal_interval+" isTop: "+horizontal_interval.isTop);

				lbbt_other.searchF_IN(horizontal_interval);
				lbbt_other.searchF_L(rect.intervalX.low);
				lbbt_other.searchF_R(rect.intervalX.high);
				lbbt_other.processRemove(horizontal_interval);
				lbbt_other.processInsert();

				// printer_helper.add("Removing Interval: "+horizontal_interval);
				// printer_helper.add(lbbt_other.print_inorder());
			}
			/**
			 * event is the TOP endpoint of rectangle
			 */
			else {
				horizontal_interval.isTop = true;
				// System.out.println("------------------------------------------");
				// System.out.println("Processing Interval: "+horizontal_interval+" isTop: "+horizontal_interval.isTop);

				// ArrayList<LBBT.Node> F_IN =
				lbbt_other.searchF_IN(horizontal_interval);
				lbbt_other.searchF_L(rect.intervalX.low);
				lbbt_other.searchF_R(rect.intervalX.high);

				lbbt_other.processF_IN(horizontal_interval);
				lbbt_other.processF_L(horizontal_interval);
				lbbt_other.processF_R(horizontal_interval);
				lbbt_other.processForkNode(horizontal_interval);
				lbbt_other.processInsert();

				// printer_helper.add("Adding Interval: "+horizontal_interval);
				// printer_helper.add(lbbt_other.print_inorder());

			}
			lbbt_other.backwardPass();

		}
		// lbbt_other.print();

		// for(String p_str : printer_helper){
		// System.out.println(p_str);
		// }

		System.out.println("Reported Range: " + lbbt_other.current_maximum + " with count: " + lbbt_other.current_maximum.count);

	}

	public static double runSweepLineFromFile(String fileName, double height, double width) {
		ArrayList<Point> points = new ArrayList<Point>();
		try {
			FileInputStream fstream1 = new FileInputStream(fileName);
			DataInputStream in1 = new DataInputStream(fstream1);
			BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));

			String strLine1 = "";

			while ((strLine1 = br1.readLine()) != null) {
				String[] f = strLine1.split(" ");
				long id = Long.parseLong(f[1]);
				double x_temp = Double.parseDouble(f[2]);
				double y_temp = Double.parseDouble(f[3]);
				// double x2 = Double.parseDouble(f[4]);
				// double y2 = Double.parseDouble(f[5]);

				double x1 = Utilities.truncateDecimal(x_temp, 10).doubleValue();
				double y1 = Utilities.truncateDecimal(y_temp, 10).doubleValue();
				// keep only 10 decimal places
				Point p = new Point(x1, y1, id);
				points.add(p);
			}
			br1.close();
		} catch (NumberFormatException e3) {
			e3.printStackTrace();
		} catch (IOException e3) {
			e3.printStackTrace();
		}
		System.out.println("Executing Sweepline on points: " + points.size());

		int N = points.size();
		Interval2D[] rects = new Interval2D[N];

		MinPQ<Event> pqLBBT = new MinPQ<Event>(new Event.heightComparator());
		double[] lbbtArray = new double[2 * N + 2];

		Point p = null;
		for (int i = 0; i < N; i++) {
			p = points.get(i);
			rects[i] = new Interval2D(new Interval1D(p.getX(), p.getX() + width), new Interval1D(p.getY(), p.getY() + height));
			Event e1 = new Event(rects[i].intervalY.low, rects[i], false);
			Event e2 = new Event(rects[i].intervalY.high, rects[i], true);
			lbbtArray[2 * (i)] = rects[i].intervalX.low;
			lbbtArray[(2 * i) + 1] = rects[i].intervalX.high;
			pqLBBT.insert(e1);
			pqLBBT.insert(e2);
		}

		lbbtArray[2 * N] = Grid.MIN_X - 1;
		lbbtArray[2 * N + 1] = Grid.MAX_X + 1;

		Arrays.sort(lbbtArray);
		LBBT lbbt_other = new LBBT(lbbtArray);
		System.out.println("LBBT tree created...span: " + lbbt_other.heap[1].N + " size: " + lbbt_other.size());
		System.out.println("Priority Queue contains " + pqLBBT.size() + " events.");

		double max_height = 200;
		Interval1D top_floor = new Interval1D(lbbtArray[0], lbbtArray[2 * N + 1], max_height);
		top_floor.count = 0;
		lbbt_other.heap[1].window = top_floor;

		// ArrayList<String> printer_helper = new ArrayList<String>();

		while (!pqLBBT.isEmpty()) {
			Event e = pqLBBT.delMin();
			double time = e.time;
			Interval2D rect = e.rect;
			Interval1D horizontal_interval = new Interval1D(rect.intervalX.low, rect.intervalX.high, rect.intervalY.high);

			/**
			 * event is the BOTTOM endpoint of rectangle
			 */
			if (time == rect.intervalY.low) {
				horizontal_interval.isTop = false;

				// printer_helper.add("Removing Interval: "+horizontal_interval);
				// for(String p_str : printer_helper){
				// System.out.println(p_str);
				// }

				lbbt_other.searchF_IN(horizontal_interval);
				lbbt_other.searchF_L(rect.intervalX.low);
				lbbt_other.searchF_R(rect.intervalX.high);
				lbbt_other.processRemove(horizontal_interval);
				lbbt_other.processInsert();

				// printer_helper.add(lbbt_other.print_inorder());

			}
			/**
			 * event is the TOP endpoint of rectangle
			 */
			else {
				horizontal_interval.isTop = true;

				// printer_helper.add("Adding Interval: "+horizontal_interval);
				// for(String p_str : printer_helper){
				// System.out.println(p_str);
				// }

				lbbt_other.searchF_IN(horizontal_interval);
				lbbt_other.searchF_L(rect.intervalX.low);
				lbbt_other.searchF_R(rect.intervalX.high);

				lbbt_other.processF_IN(horizontal_interval);
				lbbt_other.processF_L(horizontal_interval);
				lbbt_other.processF_R(horizontal_interval);
				lbbt_other.processForkNode(horizontal_interval);
				lbbt_other.processInsert();

				// printer_helper.add(lbbt_other.print_inorder());

			}
			lbbt_other.backwardPass();

			if (pqLBBT.size() % 50 == 0) {
				System.out.println("Interval left to process: " + pqLBBT.size());
			}

			// for(String p_str : printer_helper){
			// System.out.println(p_str);
			// }
			// printer_helper.clear();
		}

		// lbbt_other.print();

		// for(String p_str : printer_helper){
		// System.out.println(p_str);
		// }

		System.out.println("Reported Range: " + lbbt_other.current_maximum + " with count: " + lbbt_other.current_maximum.count);

		return lbbt_other.current_maximum.count;
	}

}
