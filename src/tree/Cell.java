package tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class Cell {
	
	private int i, j;  //在Grid中的第i行，第j列
	private  ArrayList<Point> listOfPoints = null;
	public TNode parent; // 一个node下存储了很多cell
	float c_weight=0; 
	
	/**  |-----X2
	 *	 |     |
	 *	 X1----| 
	 */
		
    private double x1, x2, y1, y2;
   
    HashMap<String,List<TF_pair>> idx = null;   // TODO this has already the keyword strings  < "word" ,<id of point, TF_value>>
    HashMap<Long, Double> docLenMap = null;
    HashMap<String, Double> totalImpact = null;
    

    int cellType;
	
	
	public Cell(){
		listOfPoints = null;
	}
	
	void calculateWeight(){
		if(listOfPoints == null){
			return;
		}
		for(Point p: listOfPoints){
			c_weight += p.p_weight;
		}
	}

	void setIndex(int c_i, int c_j){
		i = c_i;
		j = c_j;
	}

	int getIndexI(){
		return i;
	}

	int getIndexJ(){
		return j;
	}

	float getNumOfOccupants(){
		return c_weight; //Cell weight
	}
	
	void setDimensions(double _x1, double _y1, double _x2, double _y2){
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;

	}

	void newPoint(Point user){ 
		if(listOfPoints==null){
			listOfPoints = new ArrayList<Point>();
			
		} 
		listOfPoints.add(user);
	}

	ArrayList<Point> getListOfPoints(){ return listOfPoints;}
	
	void setType(int type){ cellType = type;}

	int getType(){ return cellType;}
	
	double getX1(){ return x1;}

	double getY1(){ return y1;} 
	
	double getX2(){ return x2;}

	double getY2(){ return y2;}

	//根究不同的textQuery赋予points不同的权重    TODO listofPoints not used at all. 
	public void addPointsInCell(ArrayList<Point> _sweeplineInput, TextQuery textQuery) {   
		if(listOfPoints==null)
			return;
		
		/*
		 * access inverted index and set weight for all users according to the query keywords
		 * i.e. calculate tf-idf of each user
		 */
		
		if(this.idx!=null){
			for(String word: textQuery.terms){
				if(this.idx.containsKey(word)){
					List<TF_pair> pl = this.idx.get(word);
					for(TF_pair tf_user: pl){
						Long u_id = tf_user.getId();
						double impact = tf_user.getValue();  // according to addToIDX, value seems to be occurrence?
						Point p = Grid.locations.get(u_id);
						p.setWeight(p.getWeight() + (impact * textQuery.queryImpacts.get(word)));  
						// textQuery.queryImpacts.get(word) 这个term在这个query中的重要性，即它对我的查询query是否关键
						//impact ???这个term在我的Geo分布中的重要性，即假设我的query是"好吃的中餐馆"，则Geo中的每个cell都有一个中餐馆的weight来确定这个单词在这个cell中出现是否频繁
						//p 的weight就是这个权重的总和值
						
						//每次有新的textQuery来，则对Cell中的每个tf_user,将此impact和query的impact相成，所有值的总和为该point P代表的cell的该word的权值
					}
				}
			}
		}
		_sweeplineInput.addAll(listOfPoints);
	}

	public void addToIDX(String _word, int _occurrences, long _id, boolean _isNew) {
		
		if(idx == null){
			idx = new HashMap<String,List<TF_pair> >();
			docLenMap = new HashMap<Long,Double>();
			totalImpact = new HashMap<String, Double>();
		}
		
		List<TF_pair> tf_list = idx.get(_word);
		if(tf_list != null) {                // word seen
	        tf_list.add(new TF_pair(_id, _occurrences));
	    }
	    else {                               // word NOT seen
			tf_list = new ArrayList<TF_pair>();
			tf_list.add(new TF_pair(_id, _occurrences));
			idx.put(_word, tf_list);
		}
		
		//manage document length of all users involved.
		// need term frequency only
		double tf_w = 1 + Math.log10(_occurrences);
		
		if(!docLenMap.containsKey(_id)){
			docLenMap.put(_id, tf_w*tf_w);
		}
		else{
			docLenMap.put(_id, docLenMap.get(_id) + (tf_w*tf_w));
		}
		
		
	}

}
