package tree;

public class FriendList {
    long id;
    int[] list;
    int size;


    FriendList(int _size, long _id) {
	    id = _id;
	    size = _size;
	    list = null;
    }


    int[] getList(){             	
	    int[] t = new int[size];
	
	    for (int i = 0; i < size; i++){
	        t[i] = list[i];
	    }
	
	    return t;
    }

	int getListSize() {
	    return list.length;
	}

	void setList(int [] _list, int _size){
	    size = _size;
	    list = _list;
	}
	
	
	public String toString() {
	    return "Size: " + size ;
	}      
}

