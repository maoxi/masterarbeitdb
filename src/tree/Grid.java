package tree;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import utilities.Utilities;

public class Grid {
	public static final int CELL = 0;
	public static final int TN = 1;

	/*
	--------------MAX_X,MAX_Y
	|0Y	|	|	|XY	|
	-----------------
	|,,	|	|	|	|
	-----------------
	|01	|	|	|	|
	-----------------
	|00	|10	|..	|X0	|
	MIN_X,MIN_Y-----
	 */

	//spatial grid constants
	public static double MIN_X;
	public static double MIN_Y;
	public static double MAX_X;
	public static double MAX_Y;
	public static double DELTA_X;  //x方向上的cell的宽度, 是通过(Max-Min)/X 算出来的
	public static double DELTA_Y;  
	public static int X;  // x 方向上的index,即 x方向上有几个cell
	public static int Y;  
	public static int DATASET_SIZE;  //Grid中点的数量
//	public static String[] terms;

	boolean printing_flag = true;


	//testing ONLY
	
	HashMap<Long, Double> NC_points_with_weight  = new HashMap<Long, Double>();
	
	
	//tree constants
	/**
	 * R_FAN represents granularity
	 * E.G. R_FAN of 2 implies that the HG_FANOUT is 4
	 * 
	 */
	public static int R_FAN;  // Granularity,比如取sqrt(HG_FANOUT), 单个维度上的分叉， R_FAN*R_FAN是整个xy面上的面积分叉数
	public static int HG_HEIGHT;  //  指数
	public static int HG_FANOUT;  //  底数

	//global indexes for baseline tests
	
	public ArrayList<Point> datasetAsPoints = new ArrayList<Point>();
	HashMap<String,List<TF_pair>> GLOBAL_IDX = null;  //和Cell中的idx一样，是全局的键值对
	HashMap<Long, Double> GLOBAL_DLEN = null;
	HashMap<Long, FriendList> hashTable = null; // socialgraph
	
	Map<Integer, TNode[][]> tree;  // integer为Height, TNode[][] 为越底层越细分的Node数组
	TNode root; 		//root node of tree
	Cell[][] table;		//level 0 spatial grid
	public static Map<Long , Point> locations;	//all users

	//Textual Module
	HashMap<String, Integer> IDF = new HashMap<String, Integer>(); //某string出现的次数


	Grid (){
		table = new Cell[X][Y];
		locations = new HashMap<Long , Point>();
		System.out.println( "MIN_X = " + MIN_X + " MAX_X = " + MAX_X + " DELTA_X = " + DELTA_X );
		System.out.println( "MIN_Y = " + MIN_Y + " MAX_Y = " + MAX_Y + " DELTA_Y = " + DELTA_Y );
		//初始化第一层，(底层)的cell
		for (int i=0; i<X; i++){
			for (int j=0; j<Y; j++){
				Cell c = new Cell();
				c.setDimensions(MIN_X+(i*DELTA_X), MIN_Y+(j*DELTA_Y), MIN_X+((i+1)*DELTA_X), MIN_Y+((j+1)*DELTA_Y));
				// System.out.println( MIN_X+(i*DELTA_X) + " , " +  MIN_Y+(j*DELTA_Y) + " , " +  MIN_X+((i+1)*DELTA_X) + " , " +  MIN_Y+((j+1)*DELTA_Y) +)		
				c.setIndex(i, j);
				c.setType(CELL);
				// System.out.println("Creating cell <"+ c.getIndexI()+" , "+c.getIndexJ()+"> into grid | x1 "+c.getX1()+" y1 "+c.getY1()+" x2 "+c.getX2()+" y2 "+c.getY2()+" range is: ["+ ((c.getX1() - MIN_X)/DELTA_X)+ ", " + ((c.getY1() - MIN_Y)/DELTA_Y) + "] to [" + ((c.getX2() - MIN_X)/DELTA_X) + ", " + ((c.getY2() - MIN_Y)/DELTA_Y) + "]" );
				table[i][j] = c;
			}
		}
		//初始化 由多层TNode组成的整棵树
		tree = new HashMap<Integer, TNode[][]>();
		for(int z = 1; z <= HG_HEIGHT; z++){
			//			System.out.println("Adding at level: "+z+" -> grid of dimensions: "+(int) (X/Math.pow(R_FAN, z))+" x "+(int) (Y/Math.pow(R_FAN, z)));
			int i = (int) (X/Math.pow(R_FAN, z));  //越下一层，cell越多，划分的越细
			int j = (int) (Y/Math.pow(R_FAN, z));

			TNode[][] treeGrid = new TNode[i][j];
			tree.put(z, treeGrid);
		}

	}     


	/**
	 * Builds a hierarchical grid where HG_HEIGHT is the height of the tree and HG_FANOUT
	 * gives the fanout of each intermediate cell in the tree. R_FAN gives the granularity (i.e. sqrt(HG_FANOUT))
	 * We first create a spatial grid of dimensions X,Y , where entire space is split into
	 * equally spaced cells of dimensions (DELTA_X, DELTA_Y). Then, according to the coordinates of the points
	 * they are inserted into a list at their corresponding cell. A bottom-up approach build the 
	 * tree, pushing information up.
	 * 
	 * @return the root node of the tree
	 */
	public TNode createTree(){
		if(X != Y){
			throw new RuntimeException("HG needs same number of cells in the X and Y dimensions...exiting.");
		}

		double fanout = Math.pow(X*Y, 1.0/HG_HEIGHT); //Height越大，fanout越小，fanout为该层cell数
		if (Math.abs(fanout - (Math.round(fanout))) > 0.000000001) {
			throw new RuntimeException("HG VARIABLES ARE BAD.");
		}


		System.out.println("Creating Hierarchical grid with Height: "+HG_HEIGHT+" Fanout: "+HG_FANOUT+" Granularity: "+X+" x "+X);
		long startTime = System.currentTimeMillis();
		//---------depth first
		//		root = buildNode(MIN_X,MIN_Y,MAX_X+DELTA_X,MAX_Y+DELTA_Y, HG_HEIGHT,X, null);
		//---------


		//---------pyramid approach
		buildHierarchicalGrid();  //将上个method里造的tree的空架子填充
		root = tree.get(HG_HEIGHT)[0][0];  //???
		//---------

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Tree Construction Time: "+elapsedTime+" ms");
		return root;
	}

	/**
	 * this takes a pyramid approach
	 * builds complete flat levels one on top of the other 
	 */
	private void buildHierarchicalGrid(){

		for(int z = 1; z <= HG_HEIGHT; z++){
			//			System.out.println("Level: "+z+" -> grid of dimensions: "+(int) (X/Math.pow(R_FAN, z))+" x "+(int) (Y/Math.pow(R_FAN, z)));

			//number of cells at this level
			int X_Cell_Count = (int) (X/Math.pow(R_FAN, z));
			int Y_Cell_Count = (int) (Y/Math.pow(R_FAN, z));

			TNode[][] gridTable = tree.get(z);

			for(int i = 0; i < X_Cell_Count; i ++){
				for(int j = 0; j < Y_Cell_Count; j ++){  //对每个cell, 每个height上的cell数量是不一样的

					if(z == 1){	//just above leaf cells
						TNode currentNode = new TNode(z, (int) Math.round(Math.pow(R_FAN, z)));
						currentNode.setDimensions(MIN_X+(i*DELTA_X*Math.pow(R_FAN, z)), MIN_Y+(j*DELTA_Y*Math.pow(R_FAN, z)), MIN_X+((i+1)*DELTA_X*Math.pow(R_FAN, z)), MIN_Y+((j+1)*DELTA_Y*Math.pow(R_FAN, z)));
						currentNode.setType(TN);
						currentNode.setIndex(i,j);
						gridTable[i][j] = currentNode;
						currentNode.cellMembers = new ArrayList<Cell>(); //TNode本身是不存储Cell的，只在Cellmember中存储

						for(int xx = 0; xx < R_FAN; ++xx){
							for(int yy =0; yy < R_FAN; ++yy){
								Cell childNode = table[xx+(i*R_FAN)][yy+(j*R_FAN)];  // 每层分好的大格中再以1位单位的小cell, 这些在0层的table中已经包括
								childNode.parent = currentNode;
								currentNode.cellMembers.add(childNode);

								childNode.calculateWeight();
								currentNode.numOfOccupants+=childNode.getNumOfOccupants();
								if(childNode.getNumOfOccupants() > currentNode.max_NumOfOccupants){
									currentNode.max_NumOfOccupants = childNode.getNumOfOccupants();
								}
								//								System.out.println("Cell: ["+childNode.getIndexI()+", "+childNode.getIndexJ()+"] has Occupants: "+childNode.getNumOfOccupants()+"\t\t");


								//更新childNode的totalImpact.和currentNode的totalImpact值
								//create hashMap to store cumulative impacts
								if(childNode.idx != null && !childNode.idx.isEmpty()) {  //和Z!=1 的那层比起来多了对cell中idx的处理，这层的childNode是Cell
									for (Entry<String, List<TF_pair>> entry : childNode.idx.entrySet()) {
										double total_impact = 0;
										String word = entry.getKey();
										List<TF_pair> pl = entry.getValue();

										for(TF_pair tf_user: pl){
											Long u_id = tf_user.getId();
											double tf_w = 1+ Math.log10(tf_user.getValue());
											double local_impact = ( tf_w ) / childNode.docLenMap.get(u_id);  //TODO 我就不懂这个docLenMap值什么时候被填充过
											// cout<<"inserting into impactSet user: "<<u_id <<" with tf = "<<tf_user.getTF()<<" doc length: "<<sqrt(it_asd->second)<<" and impact = "<<local_impact<<endl;
											tf_user.setValue(local_impact);
											total_impact += local_impact;
											
										}
										childNode.totalImpact.put(word,total_impact);
										//将child中的totalImpact 更新到 currentNode中
										if(currentNode.totalImpact == null)
											currentNode.totalImpact = new HashMap<String, Double>();

										if(currentNode.totalImpact.containsKey(word)){
											currentNode.totalImpact.put(word, currentNode.totalImpact.get(word) + total_impact);
										}
										else{
											currentNode.totalImpact.put(word, total_impact);
										}

										if(currentNode.max_Impact == null)
											currentNode.max_Impact = new HashMap<String, Double>();

										if(currentNode.max_Impact.containsKey(word)){
											if(total_impact > currentNode.max_Impact.get(word))
												currentNode.max_Impact.put(word, currentNode.max_Impact.get(word) + total_impact);
										}
										else{
											currentNode.max_Impact.put(word, total_impact);
										}
									}
								}
							}
						}

						//						for(int v = z;v<HG_HEIGHT;v++)
						//							System.out.print("\t");
						//						System.out.print(currentNode+"\n");

					}else{	//level > 1
						TNode currentNode = new TNode(z, (int) Math.round(Math.pow(R_FAN, z)));
						currentNode.setDimensions(MIN_X+(i*DELTA_X*Math.pow(R_FAN, z)), MIN_Y+(j*DELTA_Y*Math.pow(R_FAN, z)), MIN_X+((i+1)*DELTA_X*Math.pow(R_FAN, z)), MIN_Y+((j+1)*DELTA_Y*Math.pow(R_FAN, z)));
						currentNode.setType(TN);
						currentNode.setIndex(i,j);
						gridTable[i][j] = currentNode;
						currentNode.children = new ArrayList<TNode>();

						TNode[][] gridTableAtLevelBelow = tree.get(z-1);

						for(int xx = 0; xx < R_FAN; ++xx){
							for(int yy =0; yy < R_FAN; ++yy){
								TNode childNode = gridTableAtLevelBelow[xx+(i*R_FAN)][yy+(j*R_FAN)];
								childNode.parent = currentNode;
								currentNode.children.add(childNode);
								currentNode.numOfOccupants+=childNode.getNumOfOccupants();
								if(childNode.getNumOfOccupants() > currentNode.max_NumOfOccupants){
									currentNode.max_NumOfOccupants = childNode.getNumOfOccupants();
								}
								//								System.out.println("TNode: ["+childNode.getIndexI()+", "+childNode.getIndexJ()+"] has Occupants: "+childNode.getNumOfOccupants()+"\t\t");
								
								//text
								//create hashMap to store cumulative impacts
								if(childNode.totalImpact != null && !childNode.totalImpact.isEmpty()) {
									for (Entry<String, Double> entry : childNode.totalImpact.entrySet()) {
										String word = entry.getKey();
										double total_impact = entry.getValue();  

										if(currentNode.totalImpact == null)
											currentNode.totalImpact = new HashMap<String, Double>();

										if(currentNode.totalImpact.containsKey(word)){
											currentNode.totalImpact.put(word, currentNode.totalImpact.get(word) + total_impact);
										}
										else{
											currentNode.totalImpact.put(word, total_impact);
										}

										if(currentNode.max_Impact == null)
											currentNode.max_Impact = new HashMap<String, Double>();

										if(currentNode.max_Impact.containsKey(word)){
											if(total_impact > currentNode.max_Impact.get(word))  //child_total_impact > current.max_impact
												currentNode.max_Impact.put(word, currentNode.max_Impact.get(word) + total_impact);
										}
										else{
											currentNode.max_Impact.put(word, total_impact);
										}

									}
								}

							}
						}

						currentNode.pruningList = new ArrayList<LevelSummary>();
						//这段代码后为 遍历 当前level= z下的所有 level的TNode，更新_ncmax等等最大值，所以称作LevelSummary

						//						System.out.println("Current Node: "+currentNode);
						for(int q = z-1; q > 0  ; q--){ //注意带"_"的都是当前临时的
							double _numOfOccupants = 0; double _ncmaxNumOfOccupants = 0; double _maxNumOfOccupants = 0;

							HashMap<String, Double> _max_NCimpact = null; 
							HashMap<String, Double> _totalImpact = null; 
							HashMap<String, Double> _max_Impact = null;

							if(q == z-1){ //从当前buildHiearchicalGrid所在的层为最高层开始，往下结算
								for(TNode tn : currentNode.children){
									if(_ncmaxNumOfOccupants < calculateNcMaxOccupants(tn)){
										_ncmaxNumOfOccupants = tn.my_NCmaxNumOfOccupants; //已在calculateNcMaxOccupants中被覆盖
									}
									if(_maxNumOfOccupants < tn.max_NumOfOccupants){
										_maxNumOfOccupants = tn.max_NumOfOccupants;
									}

									if(_numOfOccupants < tn.numOfOccupants){
										_numOfOccupants = tn.numOfOccupants;
									}

									_max_NCimpact = calculateNCImpact(tn);  //max(Q1,Q2,Q3,Q4)

									if(tn.totalImpact != null){
										if(_totalImpact == null )
											_totalImpact = new HashMap<String, Double>(); 
										for (Map.Entry<String, Double> entry : tn.totalImpact.entrySet()) {
											String word = entry.getKey();
											double impact = entry.getValue();
											if(_totalImpact.containsKey(word)){
												if(_totalImpact.get(word) < impact)
													_totalImpact.put(word, impact);
											}
											else{
												_totalImpact.put(word, impact);
											}
										}
									}

									if(tn.max_Impact != null){
										if(_max_Impact == null )
											_max_Impact = new HashMap<String, Double>(); 
										for (Map.Entry<String, Double> entry : tn.max_Impact.entrySet()) {
											String word = entry.getKey();
											double impact = entry.getValue();

											if(_max_Impact.containsKey(word)){
												if(_max_Impact.get(word) < impact)
													_max_Impact.put(word, impact);
											}
											else{
												_max_Impact.put(word, impact);
											}
										}
									}

								}
							}else{
								//								System.out.println("Building at level: "+q);
								for(TNode tn : currentNode.children){
									//									System.out.println("\t Children"+tn);
									//TODO 有东西么?  tn.level= z  即pruningList从 z-q-1层开始,即从0层开始往上
									LevelSummary ls = tn.pruningList.get(tn.level-q-1);  
									//									System.out.println("\t\tAggregating: "+ls);
									//将当前临时的带"_"的值都取 pruningList中最大的,这些值在起始层，即上面这个代码块中都已经赋值
									if(_ncmaxNumOfOccupants < ls.max_NCmax){
										_ncmaxNumOfOccupants = ls.max_NCmax;
									}

									if(_maxNumOfOccupants < ls.max_NumOfOccupants){
										_maxNumOfOccupants = ls.max_NumOfOccupants;
									}

									if(_numOfOccupants < ls.numOfOccupants){
										_numOfOccupants = ls.numOfOccupants;
									}

									if(ls.max_NCimpact != null){
										if(_max_NCimpact == null )
											_max_NCimpact = new HashMap<String, Double>(); 
										for (Map.Entry<String, Double> entry : ls.max_NCimpact.entrySet()) {
											String word = entry.getKey();
											double impact = entry.getValue();

											if(_max_NCimpact.containsKey(word)){
												if(_max_NCimpact.get(word) < impact)
													_max_NCimpact.put(word, impact);
											}
											else{
												_max_NCimpact.put(word, impact);
											}
										}
									}

									if(ls.totalImpact != null){
										if(_totalImpact == null )
											_totalImpact = new HashMap<String, Double>(); 
										for (Map.Entry<String, Double> entry : ls.totalImpact.entrySet()) {
											String word = entry.getKey();
											double impact = entry.getValue();
											if(_totalImpact.containsKey(word)){
												if(_totalImpact.get(word) < impact)
													_totalImpact.put(word, impact);
											}
											else{
												_totalImpact.put(word, impact);
											}
										}
									}

									if(ls.max_Impact != null){
										if(_max_Impact == null )
											_max_Impact = new HashMap<String, Double>(); 
										for (Map.Entry<String, Double> entry : ls.max_Impact.entrySet()) {
											String word = entry.getKey();
											double impact = entry.getValue();

											if(_max_Impact.containsKey(word)){
												if(_max_Impact.get(word) < impact)
													_max_Impact.put(word, impact);
											}
											else{
												_max_Impact.put(word, impact);
											}
										}
									}

								}
							}

							LevelSummary ls = new LevelSummary(q,_numOfOccupants, _ncmaxNumOfOccupants ,_maxNumOfOccupants);
							ls.setTextBounds(_max_NCimpact, _totalImpact, _max_Impact);  //TODO totalImpact和TNode处于哪个层有无正比反比关系?

							currentNode.pruningList.add(ls);



							//							LevelSummary blah = new LevelSummary(q, _ncmax ,_maxNumOfOccupants);
							//							System.out.println("Constructed: "+blah);
						}

						//						for(int v = z;v<HG_HEIGHT;v++)
						//							System.out.print("\t");
						//						System.out.print(currentNode+"\n");


					}

				}
			}

		}

	}



	/**
	 * 
	 * @param x1	spatial extent of the node
	 * @param y1
	 * @param x2
	 * @param y2
	 * @param level		the level at which the node is; starting from 0 at leaf cells
	 * @param length	number of leaf cells it covers
	 * @param parentNode	..
	 * @return
	 */
	//length is in number of cells contained
	@SuppressWarnings("unused")
	private TNode buildNode(double x1, double y1, double x2, double y2, int level, int length, TNode parentNode){

		for(int i = level;i<HG_HEIGHT;i++)
			System.out.print("\t");
		System.out.print("Building Node at level: "+level+" length: "+length + "|  x1 "+x1+" y1 "+y1+" x2 "+x2+" y2 "+y2+" range is: ["+ ((x1 - MIN_X)/DELTA_X)+ ", " + ((y1 - MIN_Y)/DELTA_Y) + "] to [" + ((x2 - MIN_X)/DELTA_X) + ", " + ((y2 - MIN_Y)/DELTA_Y) + "]\n" );

		TNode currentNode = new TNode(x1,y1,x2,y2, level, length, parentNode);
		currentNode.setType(TN);

		//position current tree node in a 2D table at the specific level
		TNode[][] gridTable = tree.get(level);
		int q_x2 = (int) ((int)(Math.round(((x2 - MIN_X)/DELTA_X)))/Math.pow(R_FAN,level));
		int q_y2 = (int) ((int)(Math.round(((y2 - MIN_Y)/DELTA_Y)))/Math.pow(R_FAN,level));		

		for(int i = level;i<HG_HEIGHT;i++)
			System.out.print("\t");
		System.out.print("adding node in table: ["+(q_x2-1)+"]["+(q_y2-1)+"]\n");

		gridTable[q_x2-1][q_y2-1] = currentNode;
		currentNode.setIndex(q_x2-1, q_y2-1);


		double temp_ncmax = 0;
		double temp_maxnumofusers=0;
		if(level == 1){	//is at level just above the leaf
			currentNode.cellMembers = new ArrayList<Cell>();

			//get all the cells in the spatial extent
			ArrayList<Cell> cells = getCells(x1,y1,x2,y2);

			/*
			 * Cells are returned as an Arraylist in the following order
			 * 			---------
			 *			|1	|3	|
			 *			---------
			 *			|0	|2	|
			 *			---------
			 */

			for(Cell c : cells){
				currentNode.cellMembers.add(c);
				c.parent = currentNode;
				currentNode.max_NumOfOccupants += c.getNumOfOccupants();  //TODO 不是应该挑最大值么，怎么变成total了

				//determine ncmax of each cell

				if(c.getNumOfOccupants() > temp_maxnumofusers){
					temp_maxnumofusers = c.getNumOfOccupants();
				}
			}

			currentNode.pruningList = new ArrayList<LevelSummary>();
			long _numOfOccupants = 0; // FIX IF THIS FUNCTION IS USED TODO
			currentNode.pruningList.add(new LevelSummary(level, _numOfOccupants ,temp_ncmax, temp_maxnumofusers ));

			// System.out.println("\t\t Node at level 1 built with length "+length+" and max_degree: "+local_max_degree);
		}else{	//intermediate cell at least two levels above the leaf grid
			currentNode.children = new ArrayList<TNode>();
			temp_ncmax = 0;
			temp_maxnumofusers=0;


			/*
			 * 			---------
			 *			|1	|3	|
			 *			---------
			 *			|0	|2	|
			 *			---------
			 */

			int l_length = (int) Math.round(length / (double)R_FAN);	//split the currentnode into childnodes
			for(int xx = 0; xx < R_FAN; ++xx){
				for(int yy =0; yy < R_FAN; ++yy){
					double x_offset = DELTA_X * l_length;
					double y_offset = DELTA_Y * l_length;
					// System.out.println("\tCreating Children..."+qwer);
					//从level 高处往level=1处迭代
					currentNode.children.add(buildNode(x1+(xx*x_offset), y1+(yy*y_offset), x1+((xx+1)*x_offset), y1+((yy+1)*y_offset),level-1, l_length, currentNode));
				}
			}	

			// create information for current level
			for(TNode tn : currentNode.children){

				//set number of occupants of currentNode
				currentNode.max_NumOfOccupants += tn.max_NumOfOccupants;   //TODO 这个也应该挑选最大值啊，而不是和。。

				//determine ncmax of each cell
				if(calculateNcMaxOccupants(tn)>temp_ncmax){
					temp_ncmax = tn.my_NCmaxNumOfOccupants;  //tn.my_NCmax已经在calculateNcMax中赋值
				}

				//TODO 这里是不是有问题啊，tn都没有用到，和for schleife无关啊,应该写成 if(tn.getNumOfOccupants()> temp_maxnumofusers){..=tn.getNumOfOccupants()}
				if(currentNode.getNumOfOccupants() > temp_maxnumofusers){ 
					temp_maxnumofusers = currentNode.getNumOfOccupants();
				}
			}


			currentNode.pruningList = new ArrayList<LevelSummary>();
			long _numOfOccupants = 0;
			currentNode.pruningList.add(0, new LevelSummary(level, _numOfOccupants, temp_ncmax, temp_maxnumofusers ));
		}

		// System.out.println("\t\t Setting max_degree of level "+level+" length "+length+" node to "+local_max_degree);
		return currentNode;
	}


	/*
	 *  Helper functions for building tree and loading points to cells
	 *  
	 */
	//计算 i,j, 返回table[i][j]中存储了cell
	ArrayList<Cell> getCells(double x1, double y1, double x2, double y2){

		int q_x1 = (int)Math.round(((x1 - MIN_X)/DELTA_X));
		int q_y1 = (int)Math.round(((y1 - MIN_Y)/DELTA_Y));
		int q_x2 = (int)Math.round(((x2 - MIN_X)/DELTA_X));
		int q_y2 = (int)Math.round(((y2 - MIN_Y)/DELTA_Y));

		// System.out.println("\t GIB CELLS: x1 "+x1+" y1 "+y1+" x2 "+x2+" y2 "+y2+" CELSS: ["+ ((x1 - MIN_X)/DELTA_X)+ ", " + ((y1 - MIN_Y)/DELTA_Y) + "] to [" + ((x2 - MIN_X)/DELTA_X) + ", " + ((y2 - MIN_Y)/DELTA_Y) + "]" );
		// System.out.println("\t Need cells in range[" + q_x1 + ", " + q_y1 + "] to [" + q_x2 + ", " + q_y2 + "]" );
		ArrayList<Cell> cells = new ArrayList<Cell>();

		if((q_x2 - q_x1) != 2 || (q_y2 - q_y1) != 2){
			// System.out.println("BOUNDARY UNDEFNINED");
		}

		if(q_x1 >=0 && q_x1 < X && q_y1 >=0 && q_y1 < Y){   //X为Cell最小是x方向上最大可能的格数，q_x1比X大不可能，不可能如此细
			for(int i = q_x1; i < q_x2; ++i)
				for(int j = q_y1; j < q_y2; ++j)
					cells.add(table[i][j]);  // 整个大范围中的部分Cell返回  比如说table[3][2]等，未必是0开头的

			return cells;
		}
		else{
			System.out.println("Range unbounded.");
			return null;
		}
	} 
	//返回一个cell
	Cell getCell(double x, double y){
		double q_x_raw = ((x - MIN_X)/DELTA_X);
		double q_y_raw = ((y - MIN_Y)/DELTA_Y);

		if(q_x_raw < 0 || q_y_raw < 0 ){
			return null;
		}
		int q_x = (int)q_x_raw;
		int q_y = (int)q_y_raw;
		// System.out.println( "\t cell located: [" + q_x + ", " + q_y + "]" );

		if(q_x >=0 && q_x < X && q_y >=0 && q_y < Y)
			return table[q_x][q_y];
		else
			return null;
	}   


	/*
	 * neighbourCount[1] = 1.getNumOfOccupants + XA,XB,XC numOfOccupants 
	 *  	XB	XC
	 * 			---------
	 *		XA	|1	|3	|
	 *			---------
	 *			|0	|2	|
	 *			---------
	 *			
	 *
	 *	i-1,j+1		i,j+1		i+1,j+1
	 *			nc1--------nc3
	 *			|			|
	 *	i-1,j	|	i,j		|	i+1,j
	 *			|			|
	 *			nc0--------nc2
	 *	i-1,j-1		i,j-1		i+1,j-1
	 */	

	//计算大格的Next Cell 的Occupants 最大值 (左上角的三个值及其自己之和作为neighbourCount，返回TNode下的几个单元的如0123中最大的那个NeighbourCount)
	private double calculateNcMaxOccupants(TNode tn) {  //Nc= next cell 取周圍3個點nextCell里的Occupants最大值

		TNode[][] gridTable = tree.get(tn.level);

		for(int i = tn.getIndexI(); i>tn.getIndexI()-2;i--){  //i> tn.getIndexI()-2 就是允許到 i= tn.getIndexI()-1，用的是ij,所有是取小格的Occup
			for(int j= tn.getIndexJ(); j>tn.getIndexJ()-2;j--){
				TNode n = null;
				//TODO 這步的意義何在,防止不超过这个层的TNode负责的范围?
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))  
					n = gridTable[i][j];
				if(n!=null){
					tn.neighbourCount[0] += n.getNumOfOccupants();
				}
			}
		}

		for(int i = tn.getIndexI(); i>tn.getIndexI()-2;i--){
			for(int j= tn.getIndexJ(); j<tn.getIndexJ()+2;j++){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null){
					tn.neighbourCount[1] += n.getNumOfOccupants();
				}
			}
		}

		for(int i = tn.getIndexI(); i<tn.getIndexI()+2;i++){
			for(int j= tn.getIndexJ(); j>tn.getIndexJ()-2;j--){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null){
					tn.neighbourCount[2] += n.getNumOfOccupants();
				}
			}
		}

		for(int i = tn.getIndexI(); i<tn.getIndexI()+2;i++){
			for(int j= tn.getIndexJ(); j<tn.getIndexJ()+2;j++){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null){
					tn.neighbourCount[3] += n.getNumOfOccupants();
				}
			}
		}

		for(int v = 0; v < 4 ; v++){
			if(tn.neighbourCount[v] > tn.my_NCmaxNumOfOccupants){
				tn.my_NCmaxNumOfOccupants = tn.neighbourCount[v];
			}
		}
		return tn.my_NCmaxNumOfOccupants;
	}

	//计算当前Tnode四个角落上的<key, impact>之和， Folien 11页 max(Q1,Q2,Q3,Q4)
	private HashMap<String, Double> calculateNCImpact(TNode tn) {


		HashMap<String, Double> result = new HashMap<String, Double>();
		HashMap<String, Double> tempHM = null;
		List<HashMap<String, Double>> listOfMaps = new ArrayList<HashMap<String, Double>>();

		for(int i = 0; i < 4;i++){
			listOfMaps.add(new HashMap<String, Double>());
		}


		TNode[][] gridTable = tree.get(tn.level);

		tempHM = listOfMaps.get(0);
		for(int i = tn.getIndexI(); i>tn.getIndexI()-2;i--){
			for(int j= tn.getIndexJ(); j>tn.getIndexJ()-2;j--){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level)) //限制在这个TNode中
					n = gridTable[i][j];
				if(n!=null && n.totalImpact!=null){
					for (Map.Entry<String, Double> entry : n.totalImpact.entrySet()) {
						String word = entry.getKey();
						double impact = entry.getValue();

						if(tempHM.containsKey(word)){
							tempHM.put(word, tempHM.get(word) + impact);
						}
						else{
							tempHM.put(word, impact);
						}
					}
				}
			}
		}


		tempHM = listOfMaps.get(1);
		for(int i = tn.getIndexI(); i>tn.getIndexI()-2;i--){
			for(int j= tn.getIndexJ(); j<tn.getIndexJ()+2;j++){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null && n.totalImpact!=null){
					for (Map.Entry<String, Double> entry : n.totalImpact.entrySet()) {
						String word = entry.getKey();
						double impact = entry.getValue();

						if(tempHM.containsKey(word)){
							tempHM.put(word, tempHM.get(word) + impact);
						}
						else{
							tempHM.put(word, impact);
						}
					}
				}
			}
		}


		tempHM = listOfMaps.get(2);
		for(int i = tn.getIndexI(); i<tn.getIndexI()+2;i++){
			for(int j= tn.getIndexJ(); j>tn.getIndexJ()-2;j--){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null && n.totalImpact!=null){
					for (Map.Entry<String, Double> entry : n.totalImpact.entrySet()) {
						String word = entry.getKey();
						double impact = entry.getValue();

						if(tempHM.containsKey(word)){
							tempHM.put(word, tempHM.get(word) + impact);
						}
						else{
							tempHM.put(word, impact);
						}
					}
				}
			}
		}

		tempHM = listOfMaps.get(3);
		for(int i = tn.getIndexI(); i<tn.getIndexI()+2;i++){
			for(int j= tn.getIndexJ(); j<tn.getIndexJ()+2;j++){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null && n.totalImpact!=null){
					for (Map.Entry<String, Double> entry : n.totalImpact.entrySet()) {
						String word = entry.getKey();
						double impact = entry.getValue();

						if(tempHM.containsKey(word)){
							tempHM.put(word, tempHM.get(word) + impact);
						}
						else{
							tempHM.put(word, impact);
						}
					}
				}
			}
		}
		//之前结束后listOfMaps0-3 里面的内容都被(word,impact)键值对填充了，这里最后再将4个map过一遍，取每个word的最大impact存到result中
		for(int i = 0; i < 4 ; i++){
			tempHM = listOfMaps.get(i);
			for (Map.Entry<String, Double> entry : tempHM.entrySet()) {
				String word = entry.getKey();
				double impact = entry.getValue();

				if(result.containsKey(word)){
					if(result.get(word) < impact)
						result.put(word, impact);
				}
				else{
					result.put(word, impact);
				}
			}
		}

		tn.max_NCimpact = result;
		return result;

	}




	/*
	 *
	 *	i-1,j+1		i,j+1		i+1,j+1
	 *			nc1--------nc3
	 *			|			|
	 *	i-1,j	|	i,j		|	i+1,j
	 *			|			|
	 *			nc0--------nc2
	 *	i-1,j-1		i,j-1		i+1,j-1
	 */	

	// 找到有最大值的NeighbourCell里涵盖的所有cells, 添加点points
	public ArrayList<Point> getPointsInNC(TNode tn, TextQuery textQuery) {
		ArrayList<Point> pointsInNC= new ArrayList<Point>();

		TNode[][] gridTable = tree.get(tn.level);
		
		
		int max_index = 0; double counter = 0;  //counter取neighbourCount最大值
		int ncx = 0,ncy = 0; 
		
		for(int i = 0 ; i < 4 ; i++){
			if(counter < tn.neighbourCount[i]){
				counter = tn.neighbourCount[i];
				max_index = i;
			}
		}
		//ncx ncy 取occupants最大值得那格的 index xy 
		if(max_index == 0 || max_index == 1){
			ncx =  tn.getIndexI() - 1;
		}else{
			ncx =  tn.getIndexI();
		}
		if(max_index == 0 || max_index == 2){
			ncy = tn.getIndexJ() - 1 ;
		}else{
			ncy = tn.getIndexJ();
		}
		
		
		for(int i = ncx; i <= ncx + 1 ; i++){
			for(int j = ncy; j <= ncy + 1 ; j++){
//		for(int i = tn.getIndexI() - 1 ; i <= tn.getIndexI() + 1 ; i++){
//			for(int j = tn.getIndexJ() - 1 ; j <= tn.getIndexJ() + 1 ; j++){
				TNode n = null;
				if(i >=0 && i < X/Math.pow(R_FAN,tn.level) && j >=0 && j < Y/Math.pow(R_FAN,tn.level))
					n = gridTable[i][j];
				if(n!=null){
					//					System.out.println(n + " has occupants: "+n.getNumOfOccupants()+" has NCMAX: "+n.my_NCmax);
					//					for(TNode tn_c : n.children){
					//						System.out.println("\t" + tn_c + " has occupants: "+tn_c.getNumOfOccupants()+" has NCMAX: "+tn_c.my_NCmax);
					//					}
					addAllContainedCells(n,pointsInNC, textQuery);
				}
			}
		}
		
		return pointsInNC;
	}

	//不断拆到cell层，将point加入到 NC_points_with_weight中去   
	public void addAllContainedCells (TNode tmp, ArrayList<Point> pointsInNC, TextQuery textQuery){
		if(tmp.level != 1){  //child还不是cell
			for(TNode tn : tmp.children){
				addAllContainedCells(tn,pointsInNC, textQuery);
			}
		}
		else{
			for(Cell c : tmp.cellMembers){
				c.addPointsInCell(pointsInNC, textQuery);
			}
		}
		
		//deep copy the point and their weight for later checking
		for(Point p : pointsInNC){
			this.NC_points_with_weight.put(p.getID(), p.getWeight());
		}
	}

	/**
	 * returns the pointer to the cell by ID (i,j)
	 * @param i
	 * @param j
	 * @return
	 */
	Cell getCell(int i, int j){
		if(i >= 0 && j >= 0 && i < X && j < Y)
			return table[i][j];
		else
			return null;
	}


	/**
	 * 
	 * @param user
	 * @return
	 */
	boolean addPoint(Point user){
		Cell c = getCell(user.getX(), user.getY());
		if(c != null){
			c.newPoint(user);
			//			System.out.println( "Add checkin id = " + user.getID()) ;
			return true;
		}
		else{
			System.out.println( "checkin failed: " + user.getX() +", " + user.getY() );
			return false;
		}
	} 


	Point getPoint(int id){
		return locations.get(id);
	}


	/**
	 * reads the set of points from file
	 * @param fileName
	 * @return true is process ran without any errors
	 */
	// 将filename中的点坐标添加到Grid中的Arraylist<Points>中去
	boolean loadFromFile(String fileName) throws NumberFormatException, IOException {
		System.out.print( "Loading Grid ..." );
		FileInputStream fstream1 = null;
		try {
			fstream1 = new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}			
		DataInputStream in1 = new DataInputStream(fstream1);
		BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));

		String strLine1 = "";
		int count = 0;


		while ((strLine1 = br1.readLine()) != null){
			String[] f = strLine1.split(" ");
			long id =  Long.parseLong(f[1]);
			double x_temp = Double.parseDouble(f[2]);
			double y_temp = Double.parseDouble(f[3]);
			//						double x1 = Double.parseDouble(f[2]);
			//						double y1 = Double.parseDouble(f[3]);

			double x1 = Utilities.truncateDecimal(x_temp, 10).doubleValue();
			double y1 = Utilities.truncateDecimal(y_temp, 10).doubleValue();
			//keep only 10 decimal places

			if(x1 >= MIN_X && x1 <= MAX_X && y1 >= MIN_Y && y1 <= MAX_Y){
				Point p = new Point(x1, y1, id);
				locations.put(p.getID(), p);
				datasetAsPoints.add(p);
				addPoint(p);
				count++;
			}
			else{
				throw new RuntimeException("Points not inside boundary.");
			}
		}
		br1.close();
		Grid.DATASET_SIZE = count;
		System.out.print( "done. "+count+" users loaded.\n");
		return true;
	}


	public void loadSocialModule(String socialgraph) throws NumberFormatException, IOException {
		SocialModule sm = new SocialModule();

		hashTable = sm.load(socialgraph);  //<id, <listSize, friendsList[]>>

		//		 System.out.println(hashTable);

		for (Entry<Long, FriendList> entry : hashTable.entrySet()) {
			//			 	System.out.println(entry);
			Long key = entry.getKey();
			FriendList list = entry.getValue();
			//			    if(list == null){
			//			    	System.out.println("List is null");
			//			    }
			//			    if(list != null){
			Point p = locations.get(key);
			//			    	if(p == null)
			//			    		System.out.println("Point is null for key: "+key);
			p.setWeight( 1 + (list.size/(double)100) );  //朋友越多，该点的权重越大
			//			    	System.out.println("Setting weight for point: "+ p +" : "+ p.p_weight);
			//			    }
		}
	}



	public void addToIDX(String word, int occurrences, long id, boolean isNew){

		Point p = locations.get(id);
		if(p != null){

			//update partial indices in grid cells
			Cell c  = getCell(p.getX(), p.getY());
			c.addToIDX(word, occurrences, id, isNew);
			//只是textual modul的  TODO 更像document Frequency 而不是inverse的
			if(IDF.containsKey(word)){
				IDF.put(word, IDF.get(word) + 1);
			}
			else{
				IDF.put(word, 1);
			}
		
			//update global inverted index
			if(this.GLOBAL_IDX == null){
				this.GLOBAL_IDX = new HashMap<String,List<TF_pair> >();
				this.GLOBAL_DLEN = new HashMap<Long,Double>();
			}
			
			double tf_w = 1 + Math.log10(occurrences);
			
			List<TF_pair> tf_list = this.GLOBAL_IDX.get(word);
			if(tf_list != null) {                // word seen
		        tf_list.add(new TF_pair(id, tf_w));
		    }
		    else {                               // word NOT seen
				tf_list = new ArrayList<TF_pair>();
				tf_list.add(new TF_pair(id, tf_w));
				this.GLOBAL_IDX.put(word, tf_list);
			}
			
			//manage document length of all users involved.
			// need term frequency weight only
			
			if(!this.GLOBAL_DLEN.containsKey(id)){
				this.GLOBAL_DLEN.put(id, tf_w*tf_w);
			}
			else{
				this.GLOBAL_DLEN.put(id, this.GLOBAL_DLEN.get(id) + (tf_w*tf_w));
			}
		}
		else{
			System.out.println("Cannot insert into SIDX. User not in grid.");
		}
	}


}

