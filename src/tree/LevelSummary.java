package tree;

import java.util.HashMap;

public class LevelSummary {
	int level;
	double max_NCmax;
	double max_NumOfOccupants;
	double numOfOccupants;
	HashMap<String, Double> totalImpact;
	HashMap<String, Double> max_NCimpact;
	HashMap<String, Double> max_Impact;
	
	
	LevelSummary(int _level, double _numOfOccupants, double temp_ncmax, double temp_maxnumofusers){
		this.level = _level;
		this.numOfOccupants = _numOfOccupants;
		this.max_NCmax = temp_ncmax;
		this.max_NumOfOccupants = temp_maxnumofusers;
	}
	
	@Override
	public String toString() {
			return "[ level: " + level+ ", numOfOccupants: " + numOfOccupants + ", ncmax: " + max_NCmax +", maxOccupants: "+ max_NumOfOccupants +"]";
	}

	public void setTextBounds(HashMap<String, Double> _max_NCimpact, HashMap<String, Double> _totalImpact, HashMap<String, Double> _max_Impact) {
		max_NCimpact = _max_NCimpact;
		max_Impact = _max_Impact;
		totalImpact = _totalImpact;
	}
	
}
