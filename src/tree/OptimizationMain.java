package tree;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import sweepline.MaxRS;
import utilities.Utilities;


//implements the branch and bound algorithm that
//develops lower and upper bounds to enable pre-process pruning for MaxRS.

public class OptimizationMain  {

	static final double EARTH_CIRCUMFERENCE = 40075.017;
	private static Grid grid = null;

	// test client
	public static void main(String[] args) throws NumberFormatException, IOException {

		PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
		System.setOut(out);

		if(args.length != 10){
			throw new RuntimeException("Check arguments.");
		}
		System.out.print("Arguments: ");
		for(int q = 0 ; q < 10 ; q++){
			System.out.print(args[q]+ " ");
		}
		System.out.println();

		//checkins_PX.txt -112.495437 -111.711288 33.859067 33.219214 1024 2

		Grid.MIN_X = Double.parseDouble(args[3]);
		Grid.MAX_X = Double.parseDouble(args[4]);
		Grid.MIN_Y = Double.parseDouble(args[5]);
		Grid.MAX_Y = Double.parseDouble(args[6]);
		Grid.X = Integer.parseInt(args[7]);
		Grid.Y = Integer.parseInt(args[7]);;
		double height = Double.parseDouble(args[8]);
		double width = Double.parseDouble(args[9]);

		String terms[] = new String[2];
		terms[0] = "local";
		terms[1] = "ramen";

		//		Grid.MIN_X = -112.495437;
		//		Grid.MIN_Y = 33.219214;
		//		Grid.MAX_X = -111.711288;
		//		Grid.MAX_Y = 33.859067;
		//		Grid.X = 1024;
		//		Grid.Y = 1024;

		//make the grid square 将维度上少的那一方变长
		if((Grid.MAX_X - Grid.MIN_X) > (Grid.MAX_Y - Grid.MIN_Y)){
			double difference = (Grid.MAX_X - Grid.MIN_X) - (Grid.MAX_Y - Grid.MIN_Y);
			Grid.MAX_Y =  Grid.MAX_Y + difference / 2 ;
			Grid.MIN_Y =  Grid.MIN_Y - difference / 2 ;
		}else{
			double difference =  (Grid.MAX_Y - Grid.MIN_Y) - (Grid.MAX_X - Grid.MIN_X);
			Grid.MAX_X =  Grid.MAX_X + difference / 2 ;
			Grid.MIN_X =  Grid.MIN_X - difference / 2 ;
		}

		Grid.DELTA_X = ((Grid.MAX_X - Grid.MIN_X)/ (Grid.X-1));   
		Grid.DELTA_Y = ((Grid.MAX_Y - Grid.MIN_Y)/ (Grid.Y-1));
		/*
		 *  R_FAN ^ HG_HEIGHT = X
		 */
		Grid.R_FAN = 2;
		Grid.HG_FANOUT = Grid.R_FAN*Grid.R_FAN;
		Grid.HG_HEIGHT = Utilities.binlog(Grid.X); //如果X为8到16间的数字的话，取3位指数

		//		MaxRS.runSweepLineFromFile("checkins_part.txt",2* 360 /EARTH_CIRCUMFERENCE, 2* 360 /EARTH_CIRCUMFERENCE);

		/**
		 * load points into the lowermost spatial grid.
		 * load social graph
		 * load textual index 
		 * then create a hierarchical grid that summarizes information
		 */
		grid = new Grid();

		/**
		 * load checkins into memory
		 * and create grid
		 */
		grid.loadFromFile(args[0]);   //只有点坐标及id

		/**
		 * load textual info into memory
		 * and create inverted index
		 */
		TextualModule.loadTextFile(args[2], grid);  //有words 以及occurrence 及id，但是没有点坐标

		/**
		 * load social graph into memory
		 */
		grid.loadSocialModule(args[1]);  // id, friendslist, 朋友越多，该点的权重约大

		/**
		 * create hierarchical grid 
		 * pull required information up
		 */
		grid.createTree();


		//		DecimalFormat df = new DecimalFormat("#.##");

		/*
		 * textual query dimensions
		 */
		TextQuery textQuery =  new TextQuery(terms, grid);

		/*
		 * geographical query dimensions
		 */
		height = height * 360 /EARTH_CIRCUMFERENCE;
		width = width * 360 /EARTH_CIRCUMFERENCE;

		height = Math.round(height*100000.0)/100000.0;
		width = Math.round(width*100000.0)/100000.0;

		System.out.println("Query Rectangle has dimensions: "+height+" x "+width+" geodist OR "+height * EARTH_CIRCUMFERENCE / 360 +" x "+ width *EARTH_CIRCUMFERENCE/ 360+" km.");

		/*
		 * determine level wherein the query rectangle fits just inside
		 * E.g. if fit = 3 => we find level where it fits wholly inside one cell : i.e. ceil(log2(3))  = ceil(1.58) = 2
		 * e.g. if fit = 5 => ceil(log2(5)) = 3 => level = 4
		 */

		double rectLong = Math.max(height, width);
		double deltaShort = Math.min(Grid.DELTA_X, Grid.DELTA_Y);  //TODO 这个Delta_X 和 Delta_Y在这个class开始的地方不是已经调节成一样的了么。

		int fit = (int) (rectLong/deltaShort);

		if(fit == 0){
			throw new RuntimeException("The resolution of grid is too low.");
		}

		System.out.println("DELTA_X x DELTA_Y = "+Grid.DELTA_X*EARTH_CIRCUMFERENCE/360+" x "+Grid.DELTA_Y*EARTH_CIRCUMFERENCE/360+" km");
		System.out.println("Query Rectangle fits just inside cell of level: "+(Utilities.binlog(fit)+1));


		long startTime,stopTime,elapsedTime;

		{
			startTime = System.currentTimeMillis();
			ArrayList<Cell> output_E = runBnB_bound_Exact(height,width, textQuery);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			System.out.println("Pre-processing Exact  running time: "+elapsedTime+" ms");

			long totalUsers_E = 0;
			ArrayList<Point> sweeplineInput_E = new ArrayList<Point>();
			for(Cell c : output_E){
				c.addPointsInCell(sweeplineInput_E, textQuery);
				totalUsers_E+=c.getNumOfOccupants();
			}
			System.out.println("Number of cells pruned = "+((Grid.X*Grid.Y) - output_E.size())+" out of total: "+(Grid.X*Grid.Y));
			System.out.println("Number of users pruned = "+(Grid.DATASET_SIZE - totalUsers_E)+" out of total: "+Grid.DATASET_SIZE);

			MaxRS.runSweepLine(sweeplineInput_E, height, width);
		}

		{
			startTime = System.currentTimeMillis();
			addTfIdfWeighting(grid.datasetAsPoints, textQuery);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			System.out.println("Tfidf global calculation time: "+elapsedTime+" ms");

			MaxRS.runSweepLine(grid.datasetAsPoints, height, width);
		}


		/*

		System.out.println("-------------------------------------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------------------------------------");
		{
			startTime = System.currentTimeMillis();
			ArrayList<Cell> output_A = runBnB_bound_Area(height,width);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			System.out.println("Pre-processing Area running time: "+elapsedTime+" ms");


			long totalUsers_A = 0;
			ArrayList<Point> sweeplineInput_A = new ArrayList<Point>();
			for(Cell c : output_A){
				c.addPointsInCell(sweeplineInput_A);
				totalUsers_A+=c.getNumOfOccupants();
			}
			System.out.println("Number of cells pruned = "+((Grid.X*Grid.Y) - output_A.size())+" out of total: "+(Grid.X*Grid.Y));
			System.out.println("Number of users pruned = "+(Grid.DATASET_SIZE - totalUsers_A)+" out of total: "+Grid.DATASET_SIZE);
		}
		System.out.println("-------------------------------------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------------------------------------");

		{
			startTime = System.currentTimeMillis();
			ArrayList<Cell> output_Q =  runBnB_bound_Quadratic(height,width);
			stopTime = System.currentTimeMillis();
			elapsedTime = stopTime - startTime;
			System.out.println("Pre-processing Quadratic running time: "+elapsedTime+" ms");


			long totalUsers_Q = 0;
			ArrayList<Point> sweeplineInput_Q = new ArrayList<Point>();
			for(Cell c : output_Q){
				c.addPointsInCell(sweeplineInput_Q);
				totalUsers_Q+=c.getNumOfOccupants();
			}
			System.out.println("Number of cells pruned = "+((Grid.X*Grid.Y) - output_Q.size())+" out of total: "+(Grid.X*Grid.Y));
			System.out.println("Number of users pruned = "+(Grid.DATASET_SIZE - totalUsers_Q)+" out of total: "+Grid.DATASET_SIZE);
		}

		System.out.println("-------------------------------------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------------------------------------");


		/*
		long startTime = System.currentTimeMillis();
		ArrayList<Cell> output_E = runBnB_bound_Area(height,width);
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("Pre-processing running time: "+elapsedTime+" ms");


		startTime = System.currentTimeMillis();
		ArrayList<Cell> output_Q =  runBnB_bound_Quadratic(height,width);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Pre-processing running time: "+elapsedTime+" ms");


		long totalUsers_Q = 0;
		ArrayList<Point> sweeplineInput_Q = new ArrayList<Point>();
		for(Cell c : output_Q){
			c.addPointsInCell(sweeplineInput_Q);
			totalUsers_Q+=c.getNumOfOccupants();
		}
		System.out.println("Number of cells pruned = "+((Grid.X*Grid.Y) - output_Q.size())+" out of total: "+(Grid.X*Grid.Y));
		System.out.println("Number of users pruned = "+(Grid.DATASET_SIZE - totalUsers_Q)+" out of total: "+Grid.DATASET_SIZE);

		startTime = System.currentTimeMillis();
		MaxRS.runSweepLine(grid.datasetAsPoints,height,width);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Running time before pre-processing: "+elapsedTime+" ms");

		startTime = System.currentTimeMillis();
		MaxRS.runSweepLine(sweeplineInput_E,height,width);
		stopTime = System.currentTimeMillis();
		elapsedTime = stopTime - startTime;
		System.out.println("Running time after pre-processing: "+elapsedTime+" ms");


		//		MaxRS.main(new String[]{Long.toString(totalUsers)});
		//		stopTime = System.currentTimeMillis();
		//		elapsedTime = stopTime - startTime;
		//		System.out.println("Time for executing reduced sweepline: "+elapsedTime+" ms");
		//		
		//		startTime = System.currentTimeMillis();
		//		MaxRS.main(new String[]{Long.toString(Grid.DATASET_SIZE)});
		//		stopTime = System.currentTimeMillis();
		//		elapsedTime = stopTime - startTime;
		//		System.out.println("Time for executing total sweepline: "+elapsedTime+" ms");
		//		
		System.out.println("-------------------------------------------------------------------------------------------------");
		System.out.println("-------------------------------------------------------------------------------------------------");

		 */
	}




	public static ArrayList<Cell> runBnB_bound_Quadratic (double height, double width, TextQuery textQuery){
		int fit = (int) (Math.max(height, width)/Math.min(Grid.DELTA_X, Grid.DELTA_Y));

		if( fit == 0){
			throw new RuntimeException("The resolution of grid is too low.");
		}

		/*
		 * otherwise fit is greater than 1
		 * 
		 * E.g. if fit = 3
		 * we find level where it fits wholly inside one cell : i.e. ceil(log2(3))  = ceil(1.58) = 2
		 * then we go one level up to ensure that the rectangle doesn't cover any more than half the cell: i.e. 2+1 = 3
		 * 
		 * e.g. fit = 5 => ceil(log2(5)) = 3 => level = 4
		 */

		int levelAtFit = (int) Math.ceil(Utilities.binlog(fit)+1);

		long numElementsPopped = 0; double lower_bound = 0;

		ArrayList<Cell> outputToSweepLine =  new ArrayList<Cell>();
		PriorityQueue<TNode> heap = new PriorityQueue<TNode>();
		TNode root = grid.root;
		LevelSummary lsOfRoot = root.pruningList.get((root.level-levelAtFit)-1);
		root.setUB(lsOfRoot.max_NCmax);
		lower_bound = lsOfRoot.max_NumOfOccupants;
		System.out.println("Inserting ROOT: "+root);
		System.out.println("\t Lower Bound: "+lower_bound+" Level Summary at Root for fit: "+ lsOfRoot);
		heap.add(root);
		TNode tmp = null;

		while ( !heap.isEmpty()) {
			++numElementsPopped;
			tmp = heap.remove();
			//			System.out.println("Removed--: upperBound: "+ tmp.hiBound+"  :: "+tmp);

			if(tmp.level < levelAtFit){	//cannot get a valid bound for nodes that are below level of fit
				throw new RuntimeException("Found Node below level of fit!");
			}
			else if (tmp.level == levelAtFit){	// use this node's properties to prune //upperbound = my_ncmax and lowerbound = max_numofoccupants 
				if(lower_bound > tmp.my_NCmaxNumOfOccupants){
					//					System.out.println("\t\tPruned^^");
					continue;
				}else{
					//					System.out.println("\t\tTo Output^^");
					addAllContainedCells(tmp,outputToSweepLine);
				}
			}
			else if(tmp.level > levelAtFit){	//user the pruning list to find bounds

				/*
				 * pruning list is arranged in decreasing order of level starting from one level below tmp.level
				 * e.g level of fit is 1 and tmp level is 5
				 * then access 3rd entry in pruning list i.e. tmp.pruninglist.get(2);
				 */

				if(lower_bound > tmp.hiBound){
					//					System.out.println("\t\tPruned^^: ");
					continue;
				}else{
					if(levelAtFit == tmp.level - 1){
						for(TNode tn: tmp.children){
							//							System.out.println("\tInserting--: "+ tn);
							tn.setUB(tn.my_NCmaxNumOfOccupants);
							//							System.out.println("\t\t UpperBound: "+tn.my_NCmax);
							heap.add(tn);
						}
					}
					else{
						for(TNode tn: tmp.children){
							//							System.out.println("\tInserting--: "+ tn);
							LevelSummary ls = tn.pruningList.get((tn.level-levelAtFit)-1);
							tn.setUB(ls.max_NCmax);
							//							System.out.println("\t\t UpperBound: "+ls.max_NCmax+" Level Summary at Fit: "+ ls);
							heap.add(tn);
						}
					}
				}
			}
		}
		System.out.println("Number of Nodes visited: "+numElementsPopped);
		return outputToSweepLine;
	}

	public static void addAllContainedCells (TNode tmp, ArrayList<Cell> outputToSweepLine){
		if(tmp.level != 1){
			for(TNode tn : tmp.children){
				addAllContainedCells(tn,outputToSweepLine);
			}
		}
		else{
			for(Cell c : tmp.cellMembers){
				outputToSweepLine.add(c);
			}
		}
	}



	/**
	 * uses best first traversal to locate the cell with the largest upper bound.
	 * Calculates sweepline in this cell to establish a lower bound
	 * runs Branch and bound until entire tree is covered
	 * @return
	 */
	public static ArrayList<Cell> runBnB_bound_Exact (double height, double width, TextQuery textQuery){
		int fit = (int) (Math.max(height, width)/Math.min(Grid.DELTA_X, Grid.DELTA_Y));

		if( fit == 0){
			throw new RuntimeException("The resolution of grid is too low.");
		}

		/*
		 * otherwise fit is greater than 1
		 * 
		 * E.g. if fit = 3, we can then find level where it fits wholly inside one cell : i.e. ceil(log2(3))  = ceil(1.58) = 2
		 * another e.g. fit = 5 => ceil(log2(5)) = 3
		 */

		int levelAtFit = (int) Math.ceil(Utilities.binlog(fit)+1);

		long numElementsPopped = 0; double lower_bound = 0;

		ArrayList<Cell> outputToSweepLine =  new ArrayList<Cell>();
		PriorityQueue<TNode> heap = new PriorityQueue<TNode>();
		TNode root = grid.root;
		LevelSummary lsOfRoot = root.pruningList.get((root.level-levelAtFit)-1);
		root.setUB(lsOfRoot.max_NCmax);
		lower_bound = lsOfRoot.max_NumOfOccupants;
		System.out.println("Inserting ROOT: "+root);
		System.out.println("\t Lower Bound pre-computed: "+lower_bound+" Level Summary at Root for fit: "+ lsOfRoot);
		heap.add(root);
		TNode tmp = null;

		boolean isLBcomputed = false;

		while ( !heap.isEmpty()) {
			++numElementsPopped;
			tmp = heap.remove();
			//			System.out.println("Removed--: upperBound: "+ tmp.hiBound+"  :: "+tmp);

			if(isLBcomputed == false){
				if(tmp.level == levelAtFit){	//run sweepline on tmp's largest NC cells to obtain exact lower bound
					ArrayList<Point> pointsInNC =   grid.getPointsInNC(tmp, textQuery);

					//					System.out.println("NUmber of points in NC: "+pointsInNC.size());
					//										for(Point p : pointsInNC){
					//											System.out.println(p);
					//										}
					//					writeToFile(pointsInNC, "checkins_LB.txt");
					//					lower_bound = MaxRS.runSweepLineFromFile("checkins_LB.txt", height, width);
					//					System.out.println("Lower Bound Exact from file: "+lower_bound);

					lower_bound = MaxRS.runSweepLine(pointsInNC, height, width);
					System.out.println("Lower Bound Exact: "+lower_bound);
					isLBcomputed = true;
				}
				else{	//enqueue the nodes below
					if(levelAtFit == tmp.level - 1){
						for(TNode tn: tmp.children){
							//							System.out.println("\tInserting--: "+ tn);
							//obtain total tf-idf and adjust upperbound
							double tfidf_NC = 0;
							for(String word:  textQuery.terms){
								if(tn.max_NCimpact != null && tn.max_NCimpact.containsKey(word)){
									tfidf_NC += tn.max_NCimpact.get(word) * textQuery.queryImpacts.get(word);
								}
							}
							tn.setUB(tn.my_NCmaxNumOfOccupants + tfidf_NC);
							//							System.out.println("\t\t UpperBound: "+tn.my_NCmax);
							heap.add(tn);
						}
					}
					else{
						for(TNode tn: tmp.children){
							//							System.out.println("\tInserting--: "+ tn);
							LevelSummary ls = tn.pruningList.get((tn.level-levelAtFit)-1);

							//obtain total tf-idf and adjust upperbound
							double tfidf_NC = 0;
							for(String word:  textQuery.terms){
								if(ls.max_NCimpact != null && ls.max_NCimpact.containsKey(word)){
									tfidf_NC += ls.max_NCimpact.get(word) * textQuery.queryImpacts.get(word);
								}
							}
							tn.setUB(ls.max_NCmax + tfidf_NC);
							//							System.out.println("\t\t UpperBound: "+ls.max_NCmax+" Level Summary at Fit: "+ ls);
							heap.add(tn);
						}
					}
				}
			}
			else{
				if(tmp.level < levelAtFit){	//cannot get a valid bound for nodes that are below level of fit - output to sweepline directly
					throw new RuntimeException("Illegal level of node.");
				}
				else if (tmp.level == levelAtFit){	// use this node's properties to prune //upperbound = my_ncmax and lowerbound = max_numofoccupants 
					if(lower_bound > tmp.my_NCmaxNumOfOccupants){
						//					System.out.println("\t\tPruned^^");
						continue;
					}else{
						//					System.out.println("\t\tTo Output^^");
						addAllContainedCells(tmp,outputToSweepLine);
					}
				}
				/*
				 * pruning list is arranged in decreasing order of level starting from one level below tmp.level
				 * e.g level of fit is 1 and tmp level is 5
				 * then access 3rd entry in pruning list i.e. tmp.pruninglist.get(2);
				 */
				else if(tmp.level > levelAtFit){	//user the pruning list to find bounds
					if(lower_bound > tmp.hiBound){
						//					System.out.println("\t\tPruned^^: ");
						continue;
					}else{
						if(levelAtFit == tmp.level - 1){
							for(TNode tn: tmp.children){
								//							System.out.println("\tInserting--: "+ tn);
								double tfidf_NC = 0;
								for(String word:  textQuery.terms){
									if(tn.max_NCimpact != null && tn.max_NCimpact.containsKey(word)){
										tfidf_NC += tn.max_NCimpact.get(word) * textQuery.queryImpacts.get(word);
									}
								}
								tn.setUB(tn.my_NCmaxNumOfOccupants + tfidf_NC);
								//							System.out.println("\t\t UpperBound: "+tn.my_NCmax);
								heap.add(tn);
							}
						}
						else{
							for(TNode tn: tmp.children){
								//							System.out.println("\tInserting--: "+ tn);
								LevelSummary ls = tn.pruningList.get((tn.level-levelAtFit)-1);

								//obtain total tf-idf and adjust upperbound
								double tfidf_NC = 0;
								for(String word:  textQuery.terms){
									if(ls.max_NCimpact != null && ls.max_NCimpact.containsKey(word)){
										tfidf_NC += ls.max_NCimpact.get(word) * textQuery.queryImpacts.get(word);
									}
								}
								tn.setUB(ls.max_NCmax + tfidf_NC);
								//							System.out.println("\t\t UpperBound: "+ls.max_NCmax+" Level Summary at Fit: "+ ls);
								heap.add(tn);
							}
						}
					}
				}
			}
		}
		System.out.println("Number of Nodes visited: "+numElementsPopped);
		return outputToSweepLine;
	}


	public static ArrayList<Cell> runBnB_bound_Area (double height, double width, TextQuery textQuery){
		int fit =0,fit_X=0,fit_Y=0;

		fit = (int) (Math.max(height, width)/Math.min(Grid.DELTA_X, Grid.DELTA_Y));


		if( fit == 0){
			throw new RuntimeException("The resolution of grid is too low.");
		}

		/**
		 * E.g. if fit = 3, we can then find level where it fits wholly inside one cell : i.e. ceil(log2(3))  = ceil(1.58) = 2
		 * another e.g. fit = 5 => ceil(log2(5)) = 3
		 */
		int levelAtFit = (int) Math.ceil(Utilities.binlog(fit)+1);

		//find dimensions at level of fit.
		double DELTA_of_levelAtFit = Grid.DELTA_X*Math.pow(Grid.R_FAN,levelAtFit);
		fit_X = (int) Math.ceil(DELTA_of_levelAtFit / width);
		fit_Y = (int) Math.ceil(DELTA_of_levelAtFit / height);

		int times_area_covered = fit_X * fit_Y; 

		long numElementsPopped = 0; double lower_bound = 0;

		ArrayList<Cell> outputToSweepLine =  new ArrayList<Cell>();
		PriorityQueue<TNode> heap = new PriorityQueue<TNode>();
		TNode root = grid.root;
		LevelSummary lsOfRoot = root.pruningList.get((root.level-levelAtFit)-1);
		root.setUB(lsOfRoot.max_NCmax);
		lower_bound = lsOfRoot.numOfOccupants/times_area_covered;
		System.out.println("Inserting ROOT: "+root);
		System.out.println("\t Lower Bound through assumption of quadratic size: "+ lsOfRoot.max_NumOfOccupants +" Level Summary at Root for fit: "+ lsOfRoot);
		System.out.println("\t Lower Bound through assumption of uniformity: "+ lower_bound +" Level Summary at Root for fit: "+ lsOfRoot);
		heap.add(root);
		TNode tmp = null;


		while ( !heap.isEmpty()) {
			++numElementsPopped;
			tmp = heap.remove();
			//			System.out.println("Removed--: upperBound: "+ tmp.hiBound+"  :: "+tmp);

			if(tmp.level < levelAtFit){	//cannot get a valid bound for nodes that are below level of fit
				throw new RuntimeException("Found Node below level of fit!");
			}
			else if (tmp.level == levelAtFit){	// use this node's properties to prune //upperbound = my_ncmax and lowerbound = max_numofoccupants 
				if(lower_bound > tmp.my_NCmaxNumOfOccupants){
					//					System.out.println("\t\tPruned^^");
					continue;
				}else{
					//					System.out.println("\t\tTo Output^^");
					addAllContainedCells(tmp,outputToSweepLine);
				}
			}
			else if(tmp.level > levelAtFit){	//user the pruning list to find bounds

				/*
				 * pruning list is arranged in decreasing order of level starting from one level below tmp.level
				 * e.g level of fit is 1 and tmp level is 5
				 * then access 3rd entry in pruning list i.e. tmp.pruninglist.get(2);
				 */

				if(lower_bound > tmp.hiBound){
					//					System.out.println("\t\tPruned^^: ");
					continue;
				}else{
					if(levelAtFit == tmp.level - 1){
						for(TNode tn: tmp.children){
							//							System.out.println("\tInserting--: "+ tn);
							tn.setUB(tn.my_NCmaxNumOfOccupants);
							//							System.out.println("\t\t UpperBound: "+tn.my_NCmax);
							heap.add(tn);
						}
					}
					else{
						for(TNode tn: tmp.children){
							//							System.out.println("\tInserting--: "+ tn);
							LevelSummary ls = tn.pruningList.get((tn.level-levelAtFit)-1);
							tn.setUB(ls.max_NCmax);
							//							System.out.println("\t\t UpperBound: "+ls.max_NCmax+" Level Summary at Fit: "+ ls);
							heap.add(tn);
						}
					}
				}
			}
		}
		System.out.println("Number of Nodes visited: "+numElementsPopped);
		return outputToSweepLine;
	}



	private static void addTfIdfWeighting(ArrayList<Point> datasetAsPoints, TextQuery textQuery) {
		//navigate the posting list and assign weight to all points involved

		//reset weights of points
		for (Entry<Long, Point> entry : grid.locations.entrySet()) {
			Point p = entry.getValue();
			FriendList fl = grid.hashTable.get(entry.getKey());
			p.setWeight( 1 + (fl.size/(double)100) );
		}

		//give weight to points
		for(String word: textQuery.terms){
			if(grid.GLOBAL_IDX.containsKey(word)){
				List<TF_pair> pl = grid.GLOBAL_IDX.get(word);
				for(TF_pair tf_user: pl){
					Long u_id = tf_user.getId();
					double tf_w = tf_user.getValue();
					Point p = grid.locations.get(u_id);
					p.setWeight(p.getWeight() +  ((tf_w / grid.GLOBAL_DLEN.get(u_id)) * textQuery.queryImpacts.get(word)));
				}
			}
		}

	}


	private static void writeToFile(ArrayList<Point> pointsInNC, String fileName) {
		int count = 0;
		try {
			FileWriter foutstream3 = new FileWriter(fileName);
			BufferedWriter outGeographic = new BufferedWriter(foutstream3);
			for(Point p: pointsInNC){
				outGeographic.write("1 "+count+" "+p.getX()+" "+p.getY()+" "+p.getX()+" "+p.getY());
				outGeographic.newLine();
				count++;
			}
			outGeographic.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Printed array to file: "+count);
	}
}
