package tree;


//import utilities.Utilities;

public class Point {

    private final long p_id;
    private final double p_x, p_y;    
    double p_weight;

	public Point (double x, double y, long id){
	    p_x = x;
	    p_y = y;
	    p_id = id;
	    
//	    Random randomGenerator = new Random();
//		int randomInt = randomGenerator.nextInt(30);
//		double weight =  1 + (0.1 * randomInt);
//	    p_weight = weight;
	    
	    p_weight = 1;
	}

	public double getX(){return p_x;} 

	public double getY(){return p_y;}

	long getID(){return p_id;}          

	public double getWeight(){ return p_weight;};
	
	public void setWeight(double d){ p_weight = d;}
	
	public String toString() {
	    return "ID: " + p_id +  " -> (" + p_x + ", " + p_y + ")";
	}           

	/*
	// Euclidean distance										            
	double computeMinDist(double x, double y){
	    p_minDist = sqrt((x - p_x)*(x - p_x) + (y - p_y)*(y - p_y));
	    return p_minDist;
	} 

	
	// Haversine distance
	double Point::computeMinDist(double x, double y){

	    // pi/180 = 0.0174532925199433 (precise to double precision)

	    double dLong=(y-p_y)*0.0174532925199433;
	    double dLat=(x-p_x)*0.0174532925199433;

	    double aHarv = (sin(dLat/2.0)*sin(dLat/2.0))+(cos(x*0.01745329251994333)*cos(p_x*0.01745329251994333)*sin(dLong/2)*sin(dLong/2));
	    double cHarv = 2*atan2(sqrt(aHarv),sqrt(1.0-aHarv));

	    p_minDist = 6378.137*cHarv;

	    return p_minDist;
	}
	*/
}
