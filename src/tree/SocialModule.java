package tree;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class SocialModule {
	    HashMap<Long, FriendList> hashTable = new HashMap<Long, FriendList>();

		/**
		 * reads the socialgraph from file
		 * @param fileName
		 * @return 1 if process ran without any errors
		 * @throws IOException 
		 * @throws NumberFormatException 
		 */
	    HashMap<Long, FriendList> load(String file) throws NumberFormatException, IOException{
			
		    System.out.println("Loading the Social Graph from " + file);
			FileInputStream fstream1 = null;
			try {
				fstream1 = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			DataInputStream in1 = new DataInputStream(fstream1);
			BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));

			int maxSocial = 0;
			
			String strLine1 = "";
		    long id;
		    int size;
		    int totalFriends = 0;
		    FriendList entry;

		    while ((strLine1 = br1.readLine()) != null){
					String[] f = strLine1.split(" ");
					id = Integer.parseInt(f[0]);
					size = Integer.parseInt(f[1]);
			        entry = new FriendList(size, id);
			        int[] list = new int[size];
			        totalFriends+= 4*size;  //???为什么乘以4 TODO
			        for(int i = 0; i<size; i++){
			            list[i] = Integer.parseInt(f[i+2]);  //f[0]= id, f[1]= size, list[i] 为f除开第一二项的其他内容
			        }
			        entry.setList(list, size);
	
			        if(size > maxSocial)
			        	maxSocial = size;
			        hashTable.put(id, entry);
			}
		    br1.close();
		    System.out.println("MaxSocial = "+ maxSocial);
		    
		    System.out.println("Users loaded from Social Graph: " + hashTable.size());
		    System.out.println("totalFriends = " + (totalFriends/(1024)) + "KB");
		    return hashTable;
		}

		 int[] getFriends(int id){
		    FriendList v = hashTable.get(id);
		    
		    if(v!=null){
		        return v.getList();
		    }
		    else
		    	return null;
		}

		int getUserDegree(int id){
			FriendList v = hashTable.get(id);;
			
			if(v!=null){
				return v.getListSize();
			}else{
				return 0;
			}
		}
		
}
