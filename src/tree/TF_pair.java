package tree;

public class TF_pair {

	long id;
	double value;

	TF_pair(long _id, double _value) {
		this.id = _id;
		this.value = _value;
	}

	long getId() {
		return id;
	}

	double getValue() {
		return value;
	}

	void setValue(double _value) {
		this.value = _value;
	}
}
