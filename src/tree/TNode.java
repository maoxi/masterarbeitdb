package tree;

import java.util.ArrayList;
import java.util.HashMap;

public class TNode implements Comparable<TNode> {
	
	private int i,j;  //在Grid中x方向上的第i個，

	//Node properties i.e. fixed parameters
	private double x1;  //TNode为Grid中的一矩形范围
	private double y1;
	private double x2;
	private double y2;
	public final int level;
	public final int length;
	public TNode parent;
	private int nodeType;
	public ArrayList<TNode> children;    
	public ArrayList<Cell> cellMembers;  // a node stores several cells


	//Dynamic parameters and pruning mechanism
	public double max_NumOfOccupants;    // 下一层的point weight最大值, upperBound
	public double my_NCmaxNumOfOccupants; //NC=NeighbourCount 这个TNode下几个Tnode对应的区域里NextNeighbour(含occupants)最高的的那个区域的Occupants值
	public double numOfOccupants;   //这个Node下所有point的weight之和

	//HashMap<String, Double> idxImpacts;
	public HashMap<String, Double> max_Impact;  // 遇到的total_impact的总和，见笔记。
	public HashMap<String, Double> totalImpact;  //所有childNode的impact合
	public HashMap<String, Double> max_NCimpact;

	ArrayList<LevelSummary> pruningList;  //存储了当前层以下所有层的levelSummary

	public double[] neighbourCount;  //4个角落方向上的neighbour中的occupants只和，my_NCmaxNumOfOccupants是其中的最大值
	double hiBound;  //TODO ? whats this used for: descending upper bound
	
	boolean hasText = false;

	
	//---------------------------------------------
	//-------------Comparator----------------
	//---------------------------------------------
	
	/**
	 * descending upper bound  比较TNode的 hiBound, 将TNode根据Hibound进行降序排列
	 */
	public int compareTo(TNode that) {
		if      (Double.compare(this.hiBound, that.hiBound) > 0)  return -1;
		else if (Double.compare(this.hiBound, that.hiBound) < 0)  return +1;
		else                            return  0;
	}
	
	
	//---------------------------------------------
	//-----------------Constructors----------------
	//---------------------------------------------
	

	TNode(double _x1, double _y1, double _x2, double _y2, int _level, int _length, TNode _parent)
	{	
		parent = _parent;
		level =_level;
		length =_length;

		x1 = _x1; 
		x2 = _x2;                 
		y1 = _y1;       
		y2 = _y2;
		
		hiBound=0;
		my_NCmaxNumOfOccupants = 0;
		max_NumOfOccupants=0;
		numOfOccupants=0;
		neighbourCount = new double[4];
		for(int g = 0; g < 4;g++){  // 为什么初始为4个邻居
			neighbourCount[g] = 0;
		}
	}
	
	TNode(int _level, int _length)
	{	
		level =_level;
		length =_length;
		
		hiBound=0;
		my_NCmaxNumOfOccupants = 0;
		max_NumOfOccupants=0;
		numOfOccupants=0;
		neighbourCount = new double[4];
		for(int g = 0; g < 4;g++){
			neighbourCount[g] = 0;
		}
	}
	
	//---------------------------------------------
	//-------------Helper functions----------------
	//---------------------------------------------
	
	
	void setDimensions(double _x1, double _y1, double _x2, double _y2){
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
	}

	public double getNumOfOccupants(){
		return numOfOccupants;
	}
	
	public void setType(int type){
		nodeType = type;
	}

	int getType(){
		return nodeType;
	}
	
	void setIndex(int c_i, int c_j){
		i = c_i;
		j = c_j;
	}

	int getIndexI(){ return i;}

	int getIndexJ(){ return j;}

	double getX1(){ return x1;}

	double getY1(){ return y1;} 

	double getX2(){ return x2;}

	double getY2(){ return y2;}

	
	@Override
	public String toString() {
			return "Node is at level: "+level+" length: "+length+" index: ["+i+", "+j + "] |  x1 "+x1+" y1 "+y1+" x2 "+x2+" y2 "+y2+" range is: ["+ ((x1 - Grid.MIN_X)/Grid.DELTA_X)+ ", " + ((y1 - Grid.MIN_Y)/Grid.DELTA_Y) + "] to [" + ((x2 - Grid.MIN_X)/Grid.DELTA_X) + ", " + ((y2 - Grid.MIN_Y)/Grid.DELTA_Y) + "]";
	}

	public double calculateLB(double height, double width) {
		// TODO Auto-generated method stub
		//based on pruning list get lower bound
		return 0;
	}

	public void setUB(double d) {
		hiBound = d;
	}


}
