package tree;

import java.util.HashMap;
import java.util.Map;

// TODO idf_w 为何如此计算，无法理解??? 
//现在的理解是: 至少需要多大的 Dataset Size, 才能保证这个Term出现一次， 越大的dataset对应越大的query,越详细的query
public class TextQuery {

	public String[] terms;
	public HashMap<String, Double> queryImpacts = new HashMap<String, Double>();  // for each term, save its impact on this query
	public Double queryLength = 0.0;  // 用于Normalisieren
	
	TextQuery( String[] _terms, Grid grid){
		this.terms = _terms.clone();
		this.queryImpacts = new HashMap<String, Double>();
		// calculate query_length_squared;
		for(String word : terms){
			int idf = grid.IDF.get(word); //越大出现频率越高
			//IDF(t) = log_e(Total number of documents / Number of documents with term t in it). 
			double idf_w = 1 + Math.log10(Grid.DATASET_SIZE/(double)(idf));//在dataset中出现越多越不关键,idf_weight
			this.queryImpacts.put(word, idf_w); //word及其权重的键值对
			this.queryLength += idf_w*idf_w;   //所有term的权值相加
			// cout<<" idf: "<<idf<<endl;
			// cout<<"idfweight for term: "<<*itt<<" is "<<idf_w<<endl;
		}
		this.queryLength = Math.sqrt(this.queryLength);
		
		for (Map.Entry<String, Double> entry : queryImpacts.entrySet()) {
			//分子为该term的权值，分母为整个query句子的权值，相除为该term在query中的权值比重, Normalisieren durch queryLength
		    queryImpacts.put(entry.getKey(), entry.getValue() / (double) this.queryLength); 
		}
		
	}
	
}
