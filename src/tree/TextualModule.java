package tree;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class TextualModule {

	/**
	 * reads the inverted index from file
	 * @param fileName
	 * @return 1 if process ran without any errors
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	static void loadTextFile(String keywordfile, Grid grid) throws NumberFormatException, IOException {
		System.out.println("Loading keywords in GPOs from file " + keywordfile);

		FileInputStream fstream1 = null;
		try {
			fstream1 = new FileInputStream(keywordfile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		DataInputStream in1 = new DataInputStream(fstream1);
		BufferedReader br1 = new BufferedReader(new InputStreamReader(in1));
		
		String strLine1 = null;
		long id;
		String word = null;
		int occurrences;
		int times = 0;

		while ((strLine1 = br1.readLine()) != null){
			int i = 0; times++;
			String[] f = strLine1.split("\\|");
			if(f.length > 0 ) {
				id = Integer.parseInt(f[i++]);  // 一行的内容: id word Nr. word Nr. etc.
				while(i < f.length){
					if(i % 2 != 0){
						word = f[i];
					}
					else{
						occurrences = Integer.parseInt(f[i]);
						
						if(i == 2) {
							grid.addToIDX(word, occurrences, id, true);   //TODO keyword file 是什么样的，为什么分奇偶
						}
						else{
							grid.addToIDX(word, occurrences, id, false);
						}
					}
					i++;
				}
			}
		}
		br1.close();
		System.out.println("Parsed lines/users in keyword file: " + times);
	}

}
