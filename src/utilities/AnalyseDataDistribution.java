package utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import myGrid.MyPoint;
import myGrid.TwitterDataLoader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * create .csv file for output of (keyword, frequency) pairs
 * create .json file for further grapical analyse using D3.js
 * */
public class AnalyseDataDistribution {
	public static HashSet<MyPoint> datasetAsPoints = new HashSet<MyPoint>();
	static LinkedHashMap<String, Integer> topHashTagsMap = new LinkedHashMap<>();
	static LinkedHashMap<String, Integer> topHashTagsMapNot1 = new LinkedHashMap<>();
	static LinkedHashMap<String, HashMap<String, Integer>> k_k_links = new LinkedHashMap<>();
	static LinkedHashMap<String, HashMap<String, Integer>> k_k_linksNot1 = new LinkedHashMap<>();

	public static void main(String[] args) throws NumberFormatException, IOException {
		List<String> files = Arrays.asList("20140211"); // "20140207", "20140208", "20140209", "20140210", "20140211"

		for (String string : files) {
			String path = "D:\\Workspace GWT\\relevant paper\\csvSources\\";
			File filteredFile = new File(path + string + "filtered.txt");
			if (!filteredFile.exists() || filteredFile.isDirectory()) {
				new Exception("filtered file does not exist");
			} else {
				TwitterDataLoader.getInstance().loadFilteredFile(filteredFile);
			}
			datasetAsPoints.addAll(TwitterDataLoader.getInstance().getDatasetAsPoints());
		}
		writeSortedKeywordsToCSV();
		writeMaps4JSONToTXT();
//		writeNodeToJson(topHashTagsMap, k_k_links);
		
		//write Json files where the frequency != 1
		Iterator<Map.Entry<String,HashMap<String,Integer>>> itMap = k_k_linksNot1.entrySet().iterator();
		while(itMap.hasNext()){
			Entry<String, HashMap<String, Integer>>  item = itMap.next();
			topHashTagsMapNot1.put(item.getKey(), topHashTagsMap.get(item.getKey()));
			 Iterator<Map.Entry<String,Integer>> itMap2 = item.getValue().entrySet().iterator();
			while(itMap2.hasNext()){
				Map.Entry<String,Integer> item2 = itMap2.next();
				topHashTagsMapNot1.put(item2.getKey(), topHashTagsMap.get(item2.getKey()));
			}
		}
		
		writeNodeToJson(topHashTagsMapNot1, k_k_linksNot1);
	}

	private static void sortKeywordsUsingDatasetPoints() {
		if (datasetAsPoints != null) {
			for (MyPoint myPoint : datasetAsPoints) {
				LinkedHashSet<String> keywords = myPoint.getKeywords();
				for (String keyword : keywords) {
					if (topHashTagsMap.containsKey(keyword)) {
						topHashTagsMap.put(keyword, topHashTagsMap.get(keyword) + 1);
					} else {
						topHashTagsMap.put(keyword, 1);
					}
				}
			}
			topHashTagsMap = TwitterDataLoader.sortByValue(topHashTagsMap);
		}
	}

	public static void writeSortedKeywordsToCSV() {
		sortKeywordsUsingDatasetPoints();
		try {
			FileWriter writer = new FileWriter("D:\\Workspace GWT\\relevant paper\\csvSources\\keywordFrequency.csv");
			for (Map.Entry<String, Integer> keywordFrequency : topHashTagsMap.entrySet()) {
				writer.append(keywordFrequency.getKey());
				writer.append(";");
				writer.append(keywordFrequency.getValue().toString());
				writer.append("\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void writeNodeToJson( LinkedHashMap<String, Integer> nodesFrequencyMap,LinkedHashMap<String, HashMap<String, Integer>> linkMap) {
		// ---add all nodes
		JSONArray nodes = new JSONArray();
		for (Map.Entry<String, Integer> keywordFrequency : nodesFrequencyMap.entrySet()) {
			JSONObject node = new JSONObject();
			node.put("name", keywordFrequency.getKey());
			node.put("frequency", keywordFrequency.getValue());
			nodes.add(node);
		}

		// ---add all links
		JSONArray links = new JSONArray();
		for (Map.Entry<String, HashMap<String, Integer>> linkEntry : linkMap.entrySet()) {
			JSONObject link = new JSONObject();
			for (Map.Entry<String, Integer> targetEntry : linkEntry.getValue().entrySet()) {
				link.put("source", linkEntry.getKey());
				link.put("target", targetEntry.getKey());
				link.put("value", targetEntry.getValue());
				links.add(link);
			}
		}

		JSONObject obj = new JSONObject();
		obj.put("nodes", nodes);
		obj.put("links", links);

		try {
			FileWriter file = new FileWriter("D:\\Workspace GWT\\relevant paper\\csvSources\\analyse.json");
			file.write(obj.toJSONString());
			file.flush();
			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeMaps4JSONToTXT() {
		// if two keywords appear in the same dataNode, draw one link
		for (MyPoint myPoint : datasetAsPoints) {
			LinkedHashSet<String> keywords = myPoint.getKeywords();
			LinkedHashSet<String> restKeywords = new LinkedHashSet<>();
			restKeywords.addAll(keywords);
			for (String sourceK : keywords) {
				restKeywords.remove(sourceK);
				for (String targetK : restKeywords) {
					if (k_k_links.containsKey(sourceK)) {
						HashMap<String, Integer> target_value = k_k_links.get(sourceK);
						if (target_value.containsKey(targetK)) {
							target_value.put(targetK, target_value.get(targetK) + 1);
							k_k_links.put(sourceK, target_value);
						} else {
							target_value.put(targetK, 1);
							k_k_links.put(sourceK, target_value);
						}
					} else {
						HashMap<String, Integer> target_value = new HashMap<String, Integer>();
						target_value.put(targetK, 1);
						k_k_links.put(sourceK, target_value);
					}
				}
			}
		}

		// remove those frequency==1 from .txt file
		k_k_linksNot1.putAll(k_k_links);
		Iterator<Entry<String, HashMap<String, Integer>>> it1 = k_k_linksNot1.entrySet().iterator();
		while (it1.hasNext()) {
			Entry<String, HashMap<String, Integer>> item = it1.next();
			String keyword = item.getKey();
			if (topHashTagsMap.get(keyword) == 1) {
				it1.remove();
				continue;
			}
			HashMap<String, Integer> targetKeywordFs = item.getValue();
			Iterator<Entry<String, Integer>> it2 = targetKeywordFs.entrySet().iterator();
			while (it2.hasNext()) {
				int targetKeywordF = it2.next().getValue();
				if (targetKeywordF == 1)
					it2.remove();
				if (targetKeywordFs.size() == 0)
					it1.remove();
			}
		}
		try {
			FileWriter writer1 = new FileWriter("D:\\Workspace GWT\\relevant paper\\csvSources\\analyse.txt");
			writer1.append(k_k_links.toString());
			writer1.flush();
			writer1.close();
			
			FileWriter writer2 = new FileWriter("D:\\Workspace GWT\\relevant paper\\csvSources\\analyseNot1.txt");
			writer2.append(k_k_linksNot1.toString());
			writer2.flush();
			writer2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
