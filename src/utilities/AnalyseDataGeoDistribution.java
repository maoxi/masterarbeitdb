package utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * write only (lantitude, longitude) pairs to file so that its 
 * distribution could be visualized on website: 
 * http://www.darrinward.com/lat-long/?id=2099350
 */
public class AnalyseDataGeoDistribution {
	
	static String filePath = "D:\\Workspace GWT\\relevant paper\\csvSources\\20140211.tsv";
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		loadFile();
	}
	public static void loadFile() throws NumberFormatException, IOException {
		FileInputStream fstream1 = null;
		try {
			fstream1 = new FileInputStream(filePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		DataInputStream in1 = new DataInputStream(fstream1);
		BufferedReader br = new BufferedReader(new InputStreamReader(in1));
		String strLine1 = null;

		// output
		File fout = new File(filePath.split("\\.")[0] + "GeoDistribution.txt");
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		while ((strLine1 = br.readLine()) != null) {
			String[] f = strLine1.split("\t");
			double Longitude = Double.parseDouble(f[5]);
			double Latitude = Double.parseDouble(f[6]);

			bw.write(Latitude + " ");
			bw.write(",");
			bw.write(Longitude + " ");
			bw.newLine();
		}
		bw.close();
		br.close();
	}

}
