package utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateFilteredDataFile {
	static String path = "D:\\Workspace GWT\\relevant paper\\csvSources\\";

	public static void main(String[] args) throws NumberFormatException, IOException {
		List<String> files = Arrays.asList("20140211Small","20140207", "20140208", "20140209", "20140210", "20140211"); 
		for (String string : files) {
			outputFilteredData(string);
		}
	}
	
	public static void outputFilteredData(String fileName) throws NumberFormatException, IOException {
		String wholePath = path + fileName + ".tsv";

		System.out.println("Loading original twitter file " + wholePath);
		FileInputStream fstream1 = null;
		try {
			fstream1 = new FileInputStream(wholePath);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		DataInputStream in1 = new DataInputStream(fstream1);
		BufferedReader br = new BufferedReader(new InputStreamReader(in1));
		String strLine1 = null;

		// output
		File fout = new File(path + fileName+ "filtered.txt");
		FileOutputStream fos = new FileOutputStream(fout);
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

		while ((strLine1 = br.readLine()) != null) {
			String[] f = strLine1.split("\t");
			Long TweetID = Long.parseLong(f[1]);
			double Longitude = Double.parseDouble(f[5]);
			double Latitude = Double.parseDouble(f[6]);
			String stemWord = f[7];

			// extract the keywords from stemWord using regular Expression
			Pattern pattern = Pattern.compile("#([A-Za-z0-9]+)");
			Matcher matcher = pattern.matcher(stemWord);
			List<String> allMatches = new ArrayList<String>();
			while (matcher.find()) {
				allMatches.add(matcher.group().replace("#", ""));
			}
			if (!allMatches.isEmpty()) {
				double	convertedLongitude = ((Longitude / 180) + 1) / 2;
				double	convertedLatitude = 1-((Latitude / 90) + 1) / 2; // [-1,1] => [0,2] => [0,1]

				bw.write(TweetID + " ");
				bw.write(convertedLongitude + " ");
				bw.write(convertedLatitude + " ");
				for (String keyword : allMatches) {
					if(keyword.endsWith("s"))
						keyword.substring(0, keyword.length()-1);
					bw.write(keyword + " ");
				}
				bw.newLine();
			}
		}
		bw.close();
		br.close();
	}
}
