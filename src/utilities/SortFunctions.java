package utilities;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import myGrid.Main;
import myGrid.MinMaxPair;
import myGrid.Node;
import myGrid.TNode;
import myGrid.TwitterDataLoader;

/**
 * All Sort function used in sorting keyword and tree nodes 
 */
public class SortFunctions {

	public static LinkedHashSet<Node> sortSetByNodeMin(LinkedHashSet<Node> set, String keyword){
		if(!Main.useSort)
			return set;
		
		List<Node> list =new LinkedList<Node>(set);
		Collections.sort(list, new Comparator<Node>(){
			public int compare(Node firstNode, Node secondNode){
				if (firstNode.equals(secondNode))
					return 0;
				
				MinMaxPair first = null;
				MinMaxPair second = null;
				if(Main.useCandidates){
					first = ((TNode) firstNode).getNodeMinMaxNew().get(keyword);
					second = ((TNode) secondNode).getNodeMinMaxNew().get(keyword);
				}else{
					if(((TNode) firstNode).getNodeMinMaxOld().get(keyword)!=null)
						first = ((TNode) firstNode).getNodeMinMaxOld().get(keyword).get(firstNode.getLevel());
					if(((TNode) secondNode).getNodeMinMaxOld().get(keyword)!=null)
						second = ((TNode) secondNode).getNodeMinMaxOld().get(keyword).get(secondNode.getLevel()); 
				}

				if (first == null || second == null)
					return 1;

				if (first.compareTo(second) == -1)
					return 1;
				else
					return -1;
			}
		});
		LinkedHashSet<Node> result = new LinkedHashSet<Node>();
		result.addAll(list);
		return result;
	}
	
	public static LinkedHashSet<Node> sortSetByNodeMinMaxSum(LinkedHashSet<Node> set){  //TODO minSum?
		if(!Main.useSort)
			return set;
		
		List<Node> list =new LinkedList<Node>(set);
		Collections.sort(list, new Comparator<Node>(){
			public int compare(Node firstNode, Node secondNode){
				if (firstNode.equals(secondNode))
					return 0;
				
				int sumMaxFirst = 0;
				int sumMaxSecond = 0;
				Set<String> keys = Main.myQuery.getKeywords();
				if(Main.useCandidates){
					for (String keyword : keys) {
						if(((TNode) firstNode).getNodeMinMaxNew().get(keyword)!=null)
							sumMaxFirst += ((TNode) firstNode).getNodeMinMaxNew().get(keyword).getMax();
						if(((TNode) secondNode).getNodeMinMaxNew().get(keyword)!=null)
							sumMaxSecond += ((TNode) secondNode).getNodeMinMaxNew().get(keyword).getMax();
					}
				}else{
					for (String keyword: keys){
						if(((TNode) firstNode).getNodeMinMaxOld().get(keyword)!=null)
							sumMaxFirst += ((TNode) firstNode).getNodeMinMaxOld().get(keyword).get(firstNode.getLevel()).getMax();
						if(((TNode) secondNode).getNodeMinMaxOld().get(keyword)!=null)
							sumMaxSecond += ((TNode) secondNode).getNodeMinMaxOld().get(keyword).get(secondNode.getLevel()).getMax();
					}
				}
				if(sumMaxFirst > sumMaxSecond)
					return -1;
				else if(sumMaxFirst < sumMaxSecond)
					return 1;
				else
					return 1;
			}
		});
		LinkedHashSet<Node> result = new LinkedHashSet<Node>();
		result.addAll(list);
		return result;
	}
	
	public static LinkedHashSet<String> sortSetByKeywordFrequency(LinkedHashSet<String> set){
		if(!Main.useSort)
			return set;
		List<String> list =new LinkedList<String>(set);
		Collections.sort(list, new Comparator<String>(){
			@Override
			public int compare(String o1, String o2) {
				return compareFrequency(o1, o2);
			}
		});

		LinkedHashSet<String> result = new LinkedHashSet<String>();
		result.addAll(list);
		return result;
	}

	public static LinkedHashMap<String, MinMaxPair> sortMapByKeywordFrequency(LinkedHashMap<String, MinMaxPair> map){
		if(!Main.useSort)
			return map;
		List<Map.Entry<String, MinMaxPair>> list =new LinkedList <Map.Entry<String, MinMaxPair>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<String, MinMaxPair>>(){
			@Override
			public int compare(Entry<String, MinMaxPair> o1, Entry<String, MinMaxPair> o2) {
				return compareFrequency(o1.getKey(), o2.getKey());
			}
		});
		LinkedHashMap<String, MinMaxPair> result = new LinkedHashMap<String, MinMaxPair>();
		for (Map.Entry<String, MinMaxPair> entry : list){
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
	
	
	public static int compareFrequency(String o1, String o2) {
		if(o1.equals(o2))
			return 0;
		else{
			//if no such keyword is fund, it is a rare term
			if(TwitterDataLoader.getInstance().getTopHashTagsMap().get(o1)==null
					&& TwitterDataLoader.getInstance().getTopHashTagsMap().get(o2)==null)
				return 1;  
			else if(TwitterDataLoader.getInstance().getTopHashTagsMap().get(o1)==null
					&& TwitterDataLoader.getInstance().getTopHashTagsMap().get(o2)!=null)
				return 1;
			else if(TwitterDataLoader.getInstance().getTopHashTagsMap().get(o1)!=null
					&& TwitterDataLoader.getInstance().getTopHashTagsMap().get(o2)==null)
				return -1;
			else{
				int countO1 = TwitterDataLoader.getInstance().getTopHashTagsMap().get(o1);
				int countO2 = TwitterDataLoader.getInstance().getTopHashTagsMap().get(o2);
				if(countO1 > countO2)
					return -1;
				else
					return 1;
			}
		}
	}
	
	
}
