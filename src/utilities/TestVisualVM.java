package utilities;

import java.util.HashSet;

import objectexplorer.MemoryMeasurer;
import objectexplorer.ObjectGraphMeasurer;


public class TestVisualVM {

	public static void main(String[] args) throws InterruptedException {
		/*System.out.println("hallo world");
		TimeUnit.MILLISECONDS.sleep(50000); */
	
		System.out.println("grid containing: "+ ObjectGraphMeasurer.measure(new HashSet()));
		System.out.println("grid measured size: "+ MemoryMeasurer.measureBytes(new HashSet()));
	}
}
