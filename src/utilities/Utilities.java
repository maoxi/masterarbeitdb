package utilities;

import java.math.BigDecimal;

public class Utilities {
	
	//在我的javaExercise里实验过，取最高位置的1，返回值得含义为该数最高位是第几位
	public static int binlog( int bits ) // returns 0 for bits=0
	{
	    int log = 0;
	    //The unsigned right shift operator ">>>" shifts a zero into the leftmost posit
	    if( ( bits & 0xffff0000 ) != 0 ) { bits >>>= 16; log = 16; }   //右移16个bits，相当于只剩下ffff, log=16意思为 2^16
	    if( bits >= 256 ) { bits >>>= 8; log += 8; }
	    if( bits >= 16  ) { bits >>>= 4; log += 4; }
	    if( bits >= 4   ) { bits >>>= 2; log += 2; }
	    return log + ( bits >>> 1 );
	}

	//convert to a Decimal, which is float value => [int, number on (-10) to be mutipled]
	public static BigDecimal truncateDecimal(double x,int numberofDecimals)
	{
	    if ( x > 0) {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
	    } else {
	        return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
	    }
	}
	
/*	public static double mean(double[] m) {
	    double sum = 0;
	    for (int i = 0; i < m.length; i++) {
	        sum += m[i];
	    }
	    return sum / m.length;
	}
	*/
}
